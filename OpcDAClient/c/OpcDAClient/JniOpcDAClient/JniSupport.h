/**
   Implements some native support functions for Java builtin types
*/
#if !defined(_JNI_SUPPORT)
#define _JNI_SUPPORT

#include <jni.h>


// Needed includes 
#ifdef __cplusplus
extern "C" {
#endif

extern JavaVM      *cachedJVM;                     

// Types
// java.lang.Boolean
extern jclass      javaBooleanClass;
extern jmethodID   javaBoolean_booleanInit_MID;     //"<init>"        "(Z)V"
extern jmethodID   javaBoolean_booleanValue_MID;    //"booleanValue"  "()Z"

// java.lang.Byte
extern jclass      javaByteClass;
extern jmethodID   javaByte_byteInit_MID;           //"<init>"        "(Ljava/lang/String;)V" (radix10)
extern jmethodID   javaByte_byteValue_MID;          //"byteValue"     "()B"

// java.lang.Character
extern jclass      javaCharacterClass;              
extern jmethodID   javaCharacter_charInit_MID;      //"<init>"        "(C)V"
extern jmethodID   javaCharacter_charValue_MID;     //"charValue"     "()C"

// java.lang.Short
extern jclass      javaShortClass;
extern jmethodID   javaShort_shortInit_MID;         //"<init>"        "(Ljava/lang/String;)V" (radix10)
extern jmethodID   javaShort_shortValue_MID;        //"shortValue"    "()S"

// java.lang.Integer
extern jclass      javaIntegerClass;                
extern jmethodID   javaInteger_intInit_MID;         //"<init>"        "(I)V"
extern jmethodID   javaInteger_intValue_MID;        //"intValue"      "()I"

// java.lang.Long
extern jclass      javaLongClass;
extern jmethodID   javaLong_longInit_MID;           //"<init>"        "(J)V"
extern jmethodID   javaLong_longValue_MID;          //"longValue"     "()J"

// java.lang.Float
extern jclass      javaFloatClass;
extern jmethodID   javaFloat_floatInit_MID;         //"<init>"        "(F)V"
extern jmethodID   javaFloat_floatValue_MID;        //"floatValue"    "()F"

// java.lang.Double
extern jclass      javaDoubleClass;
extern jmethodID   javaDouble_doubleInit_MID;       //"<init>"        "(D)V"
extern jmethodID   javaDouble_doubleValue_MID;      //"doubleValue"   "()D"

// java.lang.String
extern jclass      javaStringClass;
extern jmethodID   javaString_getBytes_MID;         //"getBytes"        "()[B"
extern jmethodID   javaString_getChars_MID;         //"getChars"        "([CI)V"
extern jmethodID   javaString_byteInit_MID;         //"<init>"          "([B)V"
extern jmethodID   javaString_charInit_MID;         //"<init>"          "([C)V"

// java.util.Date
extern jclass      javaDateClass;
extern jmethodID   javaDate_Init_MID;				//"<init>"        "(J)V"
extern jmethodID   javaDate_getTime_MID;			//"getTime"       "()J"


// java.lang.Object
extern jclass      javaObjectClass;

// Array classes
extern jclass      javaBooleanArrayClass;
extern jclass      javaByteArrayClass;
extern jclass      javaCharacterArrayClass;
extern jclass      javaShortArrayClass;
extern jclass      javaIntegerArrayClass;
extern jclass      javaLongArrayClass;
extern jclass      javaFloatArrayClass;
extern jclass      javaDoubleArrayClass;
extern jclass      javaObjectArrayClass;

// java.lang.OutOfMemoryError
extern jclass      javaOutOfMemoryErrorExceptionClass;
extern jmethodID   javaOutOfMemoryErrorException_stringInit_MID;  // "<init>", "(Ljava/lang/String;)V"

// java.lang.NullPointerExceptionClass
extern jclass      javaNullPointerExceptionClass;
extern jmethodID   javaNullPointerException_stringInit_MID;       // "<init>", "(Ljava/lang/String;)V"

// java.lang.RuntimeException
extern jclass      javaRunTimeExceptionClass;
extern jmethodID   javaRunTimeException_stringInit_MID;           // "<init>", "(Ljava/lang/String;)V"

// java.io.IOException
extern jclass      javaIOExceptionClass;
extern jmethodID   javaIOException_stringInit_MID;                // "<init>", "(Ljava/lang/String;)V"

// java.lang.TypeNotPresentException
extern jclass      javaTypeNotPresentExceptionClass;
extern jmethodID   javaTypeNotPresentException_stringInit_MID;    // "<init>", "(Ljava/lang/String;)V"

/**
*  Creates and throws a new exception instance from the given exception class and
*  formatted message
*/
int throwException(JNIEnv *env, jclass clazz, char *f, ...);

/**
*  Constructs a new initialized java Boolean object
*/
jobject newBoolean(JNIEnv *env, jboolean b);

/**
*  Gets the primitive boolean from a Boolean object
*/
jboolean getBooleanValue(JNIEnv *env, jobject bObject);

/**
*  Constructs a new initialized java Byte object
*/
jobject newByte(JNIEnv *env, jbyte b);

/**
*  Gets the primitive boolean from a Byte object
*/
jbyte getByteValue(JNIEnv *env, jobject bObject);


/**
*  Constructs a new initialized java Short object
*/
jobject newShort(JNIEnv *env, jshort s);

/**
*  Gets the primitive short from a Short object
*/
jshort getShortValue(JNIEnv *env, jobject sObject);

/**
*  Constructs a new initialized java Integer object
*/
jobject newInteger(JNIEnv *env, jint i);

/**
*  Gets the primitive integer from a Integer object
*/
jint getIntegerValue(JNIEnv *env, jobject iObject);

/**
*  Constructs a new initialized java Long object
*/
jobject newLong(JNIEnv *env, jlong l);

/**
*  Gets the primitive long from a Long object
*/
jlong getLongValue(JNIEnv *env, jobject lObject);

/**
*  Constructs a new initialized java Float object
*/
jobject newFloat(JNIEnv *env, jfloat f);

/**
*  Gets the primitive float from a Float object
*/
jfloat getFloatValue(JNIEnv *env, jobject fObject);

/**
*  Constructs a new initialized java Double object
*/
jobject newDouble (JNIEnv *env, jdouble d);

/**
*  Gets the primitive double from a Double object
*/
jdouble getDoubleValue(JNIEnv *env, jobject dObject);


/**
*  Extract a byte character string from a java String. The current locale will be used in 
*  the translation.
*  
*  It is the responsibility of the caller to release the returned  memory when finnished using it 
*/
jbyte* getBytesFromJavaString(JNIEnv *env, jstring jstr);

/**
*  Extract the UNICODE character string from a java String. 
*  
*  It is the responsibility of the caller to release the returned  memory when finnished using it 
*/
jchar * getCharsFromJavaString(JNIEnv *env, jstring jstr);

/**
*  Construct a java string from a byte character string
*/
jstring  newJavaStringFromBytes(JNIEnv *env, jbyte *str);

/**
*  Construct a java string from a UNICODE character string
*/
jstring  newJavaStringFromCharacters(JNIEnv *env, jchar *str);


/**
*  Construct a java Date object
*/
jobject newDate(JNIEnv *env, jlong i);

/**
*  Get the corresponding long value from a java Date object
*/
jlong getDateValue(JNIEnv *env, jobject iObject);


/**
*  Used at load time to do caching of all support classes
*/
int OnJvmAttach(JNIEnv *env);

/**
*  Used at unload time to do uncaching of all loaded support classes
*/
void OnJvmDetach(JNIEnv *env);

#ifdef __cplusplus
}
#endif

#endif   // #if !defined(_JNI_SUPPORT)
