#if !defined(_JNI_ITEMLISTENER )
#define _JNI_ITEMLISTENER 

#include <jni.h>

// Needed includes 
#ifdef __cplusplus
extern "C" {
#endif

/**
*  Uninitializes the Listener
*/
void uninitializeItemListener(JNIEnv *env);

/**
*  Prepares and caches the callback methods for the DebugListener
*/
int initializeItemListener(JNIEnv *env, jobject listener);

/**
*  The native side callback that in turn calls the java one
*/
int onItemNotification(POPC_ITEM item, FILETIME *ft, DWORD quality, VARIANT  *value);

#ifdef __cplusplus
}
#endif

#endif   // #if !defined(_JNI_ITEMLISTENER)
