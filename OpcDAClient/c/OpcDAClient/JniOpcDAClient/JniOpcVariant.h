#if !defined(_JNI_VARIANT )
#define _JNI_VARIANT  

#include <jni.h>

// Needed includes 
#ifdef __cplusplus
extern "C" {
#endif


/**
*  Creates an empty Variant
*/
VARIANT *createEmptyVariant();

/**
*  Destroys a VARIANT
*/
void destroyVariant(VARIANT **v);


/**
*  Transform a VARIANT to a Java Type
*/
jobject variantToJava(JNIEnv *env, VARIANT *v);

/**
*  Transform a Java Type to a VARIANT
*/
VARIANT *javaToVariant(JNIEnv *env, VARIANT *vartmpl, jobject javaValue);

/**
*  Return VARTYPE as a char *
*/
char * variantTypeToString(VARTYPE vt);


/**
*  Return java object type as a char *
*/
char * javaTypeToString(JNIEnv *env, jobject javaValue);

#ifdef __cplusplus
}
#endif

#endif   // #if !defined(_JNI_VARIANT )



