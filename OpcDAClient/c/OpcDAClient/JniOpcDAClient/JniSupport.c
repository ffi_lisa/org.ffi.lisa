/**
   Implements some native support functions for Java builtin types

   AUTHOR hpette11@volvocars.com

   CHANGELOG

   February 2007     Created
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <wchar.h>

#include <jni.h>

#define MIN_EXCEPTION_MSG_BUF_SIZE   1024  // Minimum space we allocate for message we pass to the java side 
#define EXCEPTION_MSG_OUTPUT_ERROR   "Error generating exception description"


// Global references
JavaVM      *cachedJVM=NULL;

// Types
// java.lang.Boolean
jclass      javaBooleanClass;
jmethodID   javaBoolean_booleanInit_MID;     //"<init>"        "(Z)V"
jmethodID   javaBoolean_booleanValue_MID;    //"booleanValue"  "()Z"

jclass      javaBooleanArrayClass;

// java.lang.Byte
jclass      javaByteClass;
jmethodID   javaByte_byteInit_MID;           //"<init>"        "(B)V"
jmethodID   javaByte_byteValue_MID;          //"byteValue"     "()B"

jclass      javaByteArrayClass;

// java.lang.Character
jclass      javaCharacterClass;              
jmethodID   javaCharacter_charInit_MID;      //"<init>"        "(C)V"
jmethodID   javaCharacter_charValue_MID;     //"charValue"     "()C"

jclass      javaCharacterArrayClass;              

// java.lang.Short
jclass      javaShortClass;
jmethodID   javaShort_shortInit_MID;         //"<init>"        "(S)V"
jmethodID   javaShort_shortValue_MID;        //"shortValue"    "()S"

jclass      javaShortArrayClass;


// java.lang.Integer
jclass      javaIntegerClass;                
jmethodID   javaInteger_intInit_MID;         //"<init>"        "(I)V"
jmethodID   javaInteger_intValue_MID;        //"intValue"      "()I"

jclass      javaIntegerArrayClass;                

// java.lang.Long
jclass      javaLongClass;
jmethodID   javaLong_longInit_MID;           //"<init>"        "(J)V"
jmethodID   javaLong_longValue_MID;          //"longValue"     "()J"

jclass      javaLongArrayClass;

// java.lang.Float
jclass      javaFloatClass;
jmethodID   javaFloat_floatInit_MID;         //"<init>"        "(F)V"
jmethodID   javaFloat_floatValue_MID;        //"floatValue"    "()F"

jclass      javaFloatArrayClass;

// java.lang.Double
jclass      javaDoubleClass;
jmethodID   javaDouble_doubleInit_MID;       //"<init>"        "(D)V"
jmethodID   javaDouble_doubleValue_MID;      //"doubleValue"   "()D"

jclass      javaDoubleArrayClass;

// java.lang.String
jclass      javaStringClass;
jmethodID   javaString_getBytes_MID;
jmethodID   javaString_getChars_MID;
jmethodID   javaString_byteInit_MID;
jmethodID   javaString_charInit_MID;

// java.util.Date
jclass      javaDateClass;
jmethodID   javaDate_Init_MID;
jmethodID   javaDate_getTime_MID;


// java.lang.Object
jclass      javaObjectClass;
jclass      javaObjectArrayClass;

// java.lang.OutOfMemoryError
jclass      javaOutOfMemoryErrorExceptionClass;
jmethodID   javaOutOfMemoryErrorException_stringInit_MID;

// java.lang.NullPointerExceptionClass
jclass      javaNullPointerExceptionClass;
jmethodID   javaNullPointerException_stringInit_MID;

// java.lang.RuntimeException
jclass      javaRunTimeExceptionClass;
jmethodID   javaRunTimeException_stringInit_MID;

// java.io.IOException
jclass      javaIOExceptionClass;
jmethodID   javaIOException_stringInit_MID;

// java.lang.TypeNotPresentException
jclass      javaTypeNotPresentExceptionClass;
jmethodID   javaTypeNotPresentException_stringInit_MID;


static int supportLoaded = 0;

int throwException(JNIEnv *env, jclass clazz, char *f, ...)
{
   char  *buffer;
   char  *outbuf;

   int  rc;

   size_t   count;

   va_list  v;

   // Position argument vector
   va_start(v, f); 
   // Get a big enough buffer
   count = strlen(f)*10 + MIN_EXCEPTION_MSG_BUF_SIZE;
   buffer = calloc(1, count+1);
   // ... and place the message there
   if(buffer!=NULL) {
#if defined(_WIN32)
      if(0>_vsnprintf(buffer, count, f, v))
#elif defined(VMS)
      if(0>vsnprintf(buffer, count, f, v))
#else 
      if(0>vsprintf(buffer, f, v))
#endif
      {
         strcpy(buffer, EXCEPTION_MSG_OUTPUT_ERROR );
      }
      outbuf = buffer;
   }
   else {
      outbuf = EXCEPTION_MSG_OUTPUT_ERROR ;
   }

   // Throw the requested exception
   rc=(*env)->ThrowNew(env, clazz, outbuf);

   // Clean up 
   free(buffer);
   va_end(v);

   return rc;
}


// Boolean
jobject newBoolean(JNIEnv *env, jboolean b)
{
   return (*env)->NewObject(env, javaBooleanClass, javaBoolean_booleanInit_MID, b);
}

jboolean getBooleanValue(JNIEnv *env, jobject bObject)
{
   return (*env)->CallBooleanMethod( env, bObject, javaBoolean_booleanValue_MID);
}

// Byte
jobject newByte(JNIEnv *env, jbyte b)
{
   return (*env)->NewObject(env, javaByteClass, javaByte_byteInit_MID, b);
}

jbyte getByteValue(JNIEnv *env, jobject bObject)
{
   return (*env)->CallByteMethod( env, bObject, javaByte_byteValue_MID);
}


// Short
jobject newShort(JNIEnv *env, jshort s)
{
   return (*env)->NewObject(env, javaShortClass, javaShort_shortInit_MID, s);
}

jshort getShortValue(JNIEnv *env, jobject sObject)
{
   return (*env)->CallShortMethod( env, sObject, javaShort_shortValue_MID);
}


// Integer
jobject newInteger(JNIEnv *env, int i)
{
   return (*env)->NewObject(env, javaIntegerClass, javaInteger_intInit_MID, i);
}

int getIntegerValue(JNIEnv *env, jobject iObject)
{
   return (*env)->CallIntMethod( env, iObject, javaInteger_intValue_MID);
}

// Long
jobject newLong(JNIEnv *env, jlong i)
{
   return (*env)->NewObject(env, javaLongClass, javaLong_longInit_MID, i);
}

jlong getLongValue(JNIEnv *env, jobject iObject)
{
   return (*env)->CallLongMethod( env, iObject, javaLong_longValue_MID);
}


// Float
jobject newFloat(JNIEnv *env, float f)
{
   return (*env)->NewObject(env, javaFloatClass, javaFloat_floatInit_MID, (jfloat) f);
}

float getFloatValue(JNIEnv *env, jobject fObject)
{
   return (*env)->CallFloatMethod( env, fObject, javaFloat_floatInit_MID);
}

// Double
jobject newDouble(JNIEnv *env, double f)
{
   return (*env)->NewObject(env, javaDoubleClass, javaDouble_doubleInit_MID, (jdouble) f);
}

jdouble getDoubleValue(JNIEnv *env, jobject fObject)
{
   return (*env)->CallDoubleMethod( env, fObject, javaDouble_doubleInit_MID);
}


/**
   Extract a byte character string from a java String. The current locale will be used in 
   the translation.

   It is the responsibility of the caller to release the returned byte array.

   Returns byte array
   Returning NULL means that an error occured and an exception should be thrown by caller
*/

jbyte *getBytesFromJavaString(JNIEnv *env, jstring jstr)
{
   jbyteArray byteArr = 0;
   jbyte *result = 0;
   jint len; 

   while(1) {
      byteArr = (jbyteArray) (*env)->CallObjectMethod(env, jstr, javaString_getBytes_MID);
      if((*env)->ExceptionCheck(env)) {(*env)->ExceptionClear(env); break; }

      len = (*env)->GetArrayLength(env, byteArr);
      if((*env)->ExceptionCheck(env)) {(*env)->ExceptionClear(env); break; }

      result = (jbyte *) malloc((len + 1)*sizeof(jbyte));
	   if (result == 0) break;

	   (*env)->GetByteArrayRegion(env, byteArr, 0, len, result);
      if((*env)->ExceptionCheck(env)) {
         (*env)->ExceptionClear(env);
         free(result);
         result=0;
         break;
      }

	   result[len] = 0; /* NULL-terminate */
      //Done
      break;
   }

   // Always clean up local references
   (*env)->DeleteLocalRef(env, byteArr);

   return result;
}

/**
   Extract the UNICODE character string from a java String. 

   Returning NULL means that an error occured and an exception should be thrown by caller
*/
jchar * getCharsFromJavaString(JNIEnv *env, jstring jstr)
{
   jcharArray charArr= 0;
   jchar *result = 0;
   jint len;

   while(1) {
      charArr = (jcharArray) (*env)->CallObjectMethod(env, jstr, javaString_getChars_MID);
      if((*env)->ExceptionCheck(env)) {(*env)->ExceptionClear(env); break; }

      len = (*env)->GetArrayLength(env, charArr);
      if((*env)->ExceptionCheck(env)) {(*env)->ExceptionClear(env); break; }

      result = (jchar *) malloc((len + 1)*sizeof(jchar));
	   if (result == 0) break;

	   (*env)->GetCharArrayRegion(env, charArr, 0, len, (jchar *)result);
      if((*env)->ExceptionCheck(env)) {
         (*env)->ExceptionClear(env);
         free(result);
         result=0;
         break;
      }

	   result[len] = 0; /* NULL-terminate */
      //Done
      break;
   }

   // Always clean up local references
   (*env)->DeleteLocalRef(env, charArr);

    return result;
}

/**
   Construct a java string from a byte character string

   Returning NULL means an error occured
*/
jstring  newJavaStringFromBytes(JNIEnv *env, jbyte *str)
{
   jstring     result = 0;
   jbyteArray  byteArr= 0;
   jsize       len;

   len = (jsize) strlen((char *)str);

   while(1) {
      byteArr=(*env)->NewByteArray(env, len);
      if((*env)->ExceptionCheck(env)) {(*env)->ExceptionClear(env); break; }

      (*env)->SetByteArrayRegion(env, byteArr, 0, len, str);
      if((*env)->ExceptionCheck(env)) {(*env)->ExceptionClear(env); break; }

      result=(*env)->NewObject(env, javaStringClass, javaString_byteInit_MID, byteArr);
      if((*env)->ExceptionCheck(env)) {(*env)->ExceptionClear(env); break; }

      // Done
      break;
   }

   // Always clean up local references
   (*env)->DeleteLocalRef(env, byteArr);

   return result;
}

/**
   Construct a java string from a UNICODE character string

   Note. Need to check if this is compatible across platforms (jchar/wchar_t)
   Returning NULL means an error occured

*/
jstring  newJavaStringFromCharacters(JNIEnv *env, jchar *str)
{
   jstring     result = 0;
   jcharArray  charArr=0;
   int         len;

   len = (int) wcslen((wchar_t *) str);

   while(1) {
      charArr=(*env)->NewCharArray(env, len);
      if((*env)->ExceptionCheck(env)) {(*env)->ExceptionClear(env); break; }

      (*env)->SetCharArrayRegion(env, charArr, 0, len, str);
      if((*env)->ExceptionCheck(env)) {(*env)->ExceptionClear(env); break; }

      result = (*env)->NewObject(env, javaStringClass, javaString_charInit_MID, charArr);
      if((*env)->ExceptionCheck(env)) {(*env)->ExceptionClear(env); break; }

      // Done
      break;
   }

   // Always clean up local references
   (*env)->DeleteLocalRef(env, charArr);
   return result;

}


// Date
jobject newDate(JNIEnv *env, jlong i)
{
   return (*env)->NewObject(env, javaDateClass, javaDate_Init_MID, i);
}

jlong getDateValue(JNIEnv *env, jobject iObject)
{
	return (*env)->CallLongMethod( env, iObject, javaDate_getTime_MID);
}


/*
   Preloads common Java classes.

   Due to platform compatibility (JNI_OnLoad not always supported ?) , initialization is not called via the JNI_OnLoad function
   but instead called after the library is completly loaded by th JVM
*/
int OnJvmAttach(JNIEnv *env)
{
   jint   rc=!JNI_ERR;

   if(supportLoaded) return rc;

   while(1) {

      /////////////////////////////
      //    Value type classes
      /////////////////////////////
      // java.lang.Boolean
      if( (javaBooleanClass=(*env)->FindClass(env, "java/lang/Boolean"))!=NULL) {
         if( (javaBooleanClass=(*env)->NewGlobalRef(env, javaBooleanClass))==NULL) { rc=JNI_ERR; break;}
         if( (javaBoolean_booleanInit_MID  = (*env)->GetMethodID(env, javaBooleanClass, "<init>", "(Z)V"))==NULL) { rc=JNI_ERR; break;}
         if( (javaBoolean_booleanValue_MID = (*env)->GetMethodID(env, javaBooleanClass, "booleanValue", "()Z"))==NULL) { rc=JNI_ERR; break;}
      } 
      else { rc=JNI_ERR; break;}
      // boolean[]
      {
         jobject arrayBoolean;
         if((arrayBoolean=(*env)->NewBooleanArray(env, 1))!=NULL) {
            if( (javaBooleanArrayClass = (*env)->GetObjectClass(env, arrayBoolean))==NULL) { rc=JNI_ERR; break;}
            if( (javaBooleanArrayClass=(*env)->NewGlobalRef(env, javaBooleanArrayClass))==NULL) { rc=JNI_ERR; break;}
            (*env)->DeleteLocalRef(env, arrayBoolean);
         }
         else { rc=JNI_ERR; break;}
      }

      // java.lang.Byte
      if( (javaByteClass=(*env)->FindClass(env, "java/lang/Byte"))!=NULL) {
         if( (javaByteClass=(*env)->NewGlobalRef(env, javaByteClass))==NULL) { rc=JNI_ERR; break;}
         if( (javaByte_byteInit_MID = (*env)->GetMethodID(env, javaByteClass, "<init>", "(B)V"))==NULL) { rc=JNI_ERR; break;}
         if( (javaByte_byteValue_MID = (*env)->GetMethodID(env, javaByteClass, "byteValue", "()B"))==NULL) { rc=JNI_ERR; break;}
      } 
      else { rc=JNI_ERR; break;}
      // byte[]
      {
         jobject arrayByte;
         if((arrayByte=(*env)->NewByteArray(env, 1))!=NULL) {
            if( (javaByteArrayClass = (*env)->GetObjectClass(env, arrayByte))==NULL) { rc=JNI_ERR; break;}
            if( (javaByteArrayClass=(*env)->NewGlobalRef(env, javaByteArrayClass))==NULL) { rc=JNI_ERR; break;}
            (*env)->DeleteLocalRef(env, arrayByte);
         }
         else { rc=JNI_ERR; break;}
      }


      // java.lang.Character
      if( (javaCharacterClass=(*env)->FindClass(env, "java/lang/Character"))!=NULL) {
         if( (javaCharacterClass=(*env)->NewGlobalRef(env, javaCharacterClass))==NULL) { rc=JNI_ERR; break;}
         if( (javaCharacter_charInit_MID= (*env)->GetMethodID(env, javaCharacterClass, "<init>", "(C)V"))==NULL) { rc=JNI_ERR; break;}
         if( (javaCharacter_charValue_MID = (*env)->GetMethodID(env, javaCharacterClass, "charValue", "()C"))==NULL) { rc=JNI_ERR; break;}
      } 
      else { rc=JNI_ERR; break;}
      // char[]
      {
         jobject arrayCharacter;
         if((arrayCharacter=(*env)->NewCharArray(env, 1))!=NULL) {
            if( (javaCharacterArrayClass = (*env)->GetObjectClass(env, arrayCharacter))==NULL) { rc=JNI_ERR; break;}
            if( (javaCharacterArrayClass=(*env)->NewGlobalRef(env, javaCharacterArrayClass))==NULL) { rc=JNI_ERR; break;}
            (*env)->DeleteLocalRef(env, arrayCharacter);
         }
         else { rc=JNI_ERR; break;}
      }


      // java.lang.Short
      if( (javaShortClass=(*env)->FindClass(env, "java/lang/Short"))!=NULL) {
         if( (javaShortClass=(*env)->NewGlobalRef(env, javaShortClass))==NULL) { rc=JNI_ERR; break;}
         if( (javaShort_shortInit_MID = (*env)->GetMethodID(env, javaShortClass, "<init>", "(S)V"))==NULL) { rc=JNI_ERR; break;}
         if( (javaShort_shortValue_MID = (*env)->GetMethodID(env, javaShortClass, "shortValue", "()S"))==NULL) { rc=JNI_ERR; break;}
      } 
      else { rc=JNI_ERR; break;}
      // short[]
      {
         jobject arrayShort;
         if((arrayShort=(*env)->NewShortArray(env, 1))!=NULL) {
            if( (javaShortArrayClass = (*env)->GetObjectClass(env, arrayShort))==NULL) { rc=JNI_ERR; break;}
            if( (javaShortArrayClass=(*env)->NewGlobalRef(env, javaShortArrayClass))==NULL) { rc=JNI_ERR; break;}
            (*env)->DeleteLocalRef(env, arrayShort);
         }
         else { rc=JNI_ERR; break;}
      }

      // java.lang.Integer
      if( (javaIntegerClass=(*env)->FindClass(env, "java/lang/Integer"))!=NULL) {
         if( (javaIntegerClass=(*env)->NewGlobalRef(env, javaIntegerClass))==NULL) { rc=JNI_ERR; break;}
         if( (javaInteger_intInit_MID  = (*env)->GetMethodID(env, javaIntegerClass, "<init>", "(I)V"))==NULL) { rc=JNI_ERR; break;}
         if( (javaInteger_intValue_MID = (*env)->GetMethodID(env, javaIntegerClass, "intValue", "()I"))==NULL) { rc=JNI_ERR; break;}
      } 
      else { rc=JNI_ERR; break;}
      // int[]
      {
         jobject arrayInteger;
         if((arrayInteger=(*env)->NewIntArray(env, 1))!=NULL) {
            if( (javaIntegerArrayClass = (*env)->GetObjectClass(env, arrayInteger))==NULL) { rc=JNI_ERR; break;}
            if( (javaIntegerArrayClass=(*env)->NewGlobalRef(env, javaIntegerArrayClass))==NULL) { rc=JNI_ERR; break;}
            (*env)->DeleteLocalRef(env, arrayInteger);
         }
         else { rc=JNI_ERR; break;}
      }

      // java.lang.Long
      if( (javaLongClass=(*env)->FindClass(env, "java/lang/Long"))!=NULL) {
         if( (javaLongClass=(*env)->NewGlobalRef(env, javaLongClass))==NULL) { rc=JNI_ERR; break;}
         if( (javaLong_longInit_MID  = (*env)->GetMethodID(env, javaLongClass, "<init>", "(J)V"))==NULL) { rc=JNI_ERR; break;}
         if( (javaLong_longValue_MID = (*env)->GetMethodID(env, javaLongClass, "longValue", "()J"))==NULL) { rc=JNI_ERR; break;}
      } 
      else { rc=JNI_ERR; break;}
      // long[]
      {
         jobject arrayLong;
         if((arrayLong=(*env)->NewLongArray(env, 1))!=NULL) {
            if( (javaLongArrayClass = (*env)->GetObjectClass(env, arrayLong))==NULL) { rc=JNI_ERR; break;}
            if( (javaLongArrayClass=(*env)->NewGlobalRef(env, javaLongArrayClass))==NULL) { rc=JNI_ERR; break;}
            (*env)->DeleteLocalRef(env, arrayLong);
         }
         else { rc=JNI_ERR; break;}
      }

      // java.lang.Float
      if( (javaFloatClass =(*env)->FindClass(env, "java/lang/Float"))!=NULL) {
         if( (javaFloatClass=(*env)->NewGlobalRef(env, javaFloatClass))==NULL) { rc=JNI_ERR; break;}
         if( (javaFloat_floatInit_MID  = (*env)->GetMethodID(env, javaFloatClass, "<init>", "(F)V"))==NULL) { rc=JNI_ERR; break;}
         if( (javaFloat_floatValue_MID = (*env)->GetMethodID(env, javaFloatClass, "floatValue", "()F"))==NULL) { rc=JNI_ERR; break;}
      } 
      else { rc=JNI_ERR; break;}
      // float[]
      {
         jobject arrayFloat;
         if((arrayFloat=(*env)->NewFloatArray(env, 1))!=NULL) {
            if( (javaFloatArrayClass = (*env)->GetObjectClass(env, arrayFloat))==NULL) { rc=JNI_ERR; break;}
            if( (javaFloatArrayClass=(*env)->NewGlobalRef(env, javaFloatArrayClass))==NULL) { rc=JNI_ERR; break;}
            (*env)->DeleteLocalRef(env, arrayFloat);
         }
         else { rc=JNI_ERR; break;}
      }


      // java.lang.Double
      if( (javaDoubleClass=(*env)->FindClass(env, "java/lang/Double"))!=NULL) {
         if( (javaDoubleClass=(*env)->NewGlobalRef(env, javaDoubleClass))==NULL) { rc=JNI_ERR; break;}
         if( (javaDouble_doubleInit_MID = (*env)->GetMethodID(env, javaDoubleClass, "<init>", "(D)V"))==NULL) { rc=JNI_ERR; break;}
         if( (javaDouble_doubleValue_MID = (*env)->GetMethodID(env, javaDoubleClass, "doubleValue", "()D"))==NULL) { rc=JNI_ERR; break;}
      } 
      else { rc=JNI_ERR; break;}
      // double[]
      {
         jobject arrayDouble;
         if((arrayDouble=(*env)->NewDoubleArray(env, 1))!=NULL) {
            if( (javaDoubleArrayClass = (*env)->GetObjectClass(env, arrayDouble))==NULL) { rc=JNI_ERR; break;}
            if( (javaDoubleArrayClass=(*env)->NewGlobalRef(env, javaDoubleArrayClass))==NULL) { rc=JNI_ERR; break;}
            (*env)->DeleteLocalRef(env, arrayDouble);
         }
         else { rc=JNI_ERR; break;}
      }

      // java.lang.Object
      if( (javaObjectClass=(*env)->FindClass(env, "java/lang/Object"))!=NULL) {
         if( (javaObjectClass=(*env)->NewGlobalRef(env, javaObjectClass))==NULL) { rc=JNI_ERR; break;}
      } 
      else { rc=JNI_ERR; break;}
      // Object[]
      {
         jobject arrayObject;
         if((arrayObject=(*env)->NewObjectArray(env, 1, javaObjectClass, NULL))!=NULL) {
            if( (javaObjectArrayClass = (*env)->GetObjectClass(env, arrayObject))==NULL) { rc=JNI_ERR; break;}
            if( (javaObjectArrayClass=(*env)->NewGlobalRef(env, javaObjectArrayClass))==NULL) { rc=JNI_ERR; break;}
            (*env)->DeleteLocalRef(env, arrayObject);
         }
         else { rc=JNI_ERR; break;}
      }

      // java.lang.String
      if( (javaStringClass=(*env)->FindClass(env, "java/lang/String"))!=NULL) {
         if( (javaStringClass=(*env)->NewGlobalRef(env, javaStringClass))==NULL) { rc=JNI_ERR; break;}
         if( (javaString_byteInit_MID = (*env)->GetMethodID(env, javaStringClass, "<init>", "([B)V"))==NULL) { rc=JNI_ERR; break;}
         if( (javaString_charInit_MID = (*env)->GetMethodID(env, javaStringClass, "<init>", "([C)V"))==NULL) { rc=JNI_ERR; break;}
         if( (javaString_getBytes_MID = (*env)->GetMethodID(env, javaStringClass, "getBytes", "()[B"))==NULL) { rc=JNI_ERR; break;}
         if( (javaString_getChars_MID = (*env)->GetMethodID(env, javaStringClass, "toCharArray", "()[C"))==NULL) { rc=JNI_ERR; break;}
      } 
      else { rc=JNI_ERR; break;}

      // java.util.Date
      if( (javaDateClass=(*env)->FindClass(env, "java/util/Date"))!=NULL) {
         if( (javaDateClass=(*env)->NewGlobalRef(env, javaDateClass))==NULL) { rc=JNI_ERR; break;}
         if( (javaDate_Init_MID = (*env)->GetMethodID(env, javaDateClass, "<init>", "(J)V"))==NULL) { rc=JNI_ERR; break;}
         if( (javaDate_getTime_MID = (*env)->GetMethodID(env, javaDateClass, "getTime", "()J"))==NULL) { rc=JNI_ERR; break;}
      } 
      else { rc=JNI_ERR; break;}


      //////////////////////////
      //   Exceptions classes
      //////////////////////////
      if( (javaOutOfMemoryErrorExceptionClass=(*env)->FindClass(env, "java/lang/OutOfMemoryError"))!=NULL) {
         if( (javaOutOfMemoryErrorExceptionClass=(*env)->NewGlobalRef(env, javaOutOfMemoryErrorExceptionClass))==NULL) { rc=JNI_ERR; break;}
         if( (javaOutOfMemoryErrorException_stringInit_MID = (*env)->GetMethodID(env, javaOutOfMemoryErrorExceptionClass, "<init>", "(Ljava/lang/String;)V"))==NULL) { rc=JNI_ERR; break;}
      }
      else { rc=JNI_ERR; break;}

      // java.lang.RuntimeException
      if( (javaRunTimeExceptionClass=(*env)->FindClass(env, "java/lang/RuntimeException"))!=NULL) {
         if( (javaRunTimeExceptionClass=(*env)->NewGlobalRef(env, javaRunTimeExceptionClass))==NULL) { rc=JNI_ERR; break;}
         if( (javaRunTimeException_stringInit_MID= (*env)->GetMethodID(env, javaRunTimeExceptionClass, "<init>", "(Ljava/lang/String;)V"))==NULL) { rc=JNI_ERR; break;}
      }
      else { rc=JNI_ERR; break;}

      // java.lang.NullpointerException
      if( (javaNullPointerExceptionClass=(*env)->FindClass(env, "java/lang/NullPointerException"))!=NULL) {
         if( (javaNullPointerExceptionClass=(*env)->NewGlobalRef(env, javaNullPointerExceptionClass))==NULL) { rc=JNI_ERR; break;}
         if( (javaNullPointerException_stringInit_MID= (*env)->GetMethodID(env, javaNullPointerExceptionClass, "<init>", "(Ljava/lang/String;)V"))==NULL) { rc=JNI_ERR; break;}
      } 
      else { rc=JNI_ERR; break;}

      // java.io.IOException
      if( (javaIOExceptionClass=(*env)->FindClass(env, "java/io/IOException"))!=NULL) {
         if( (javaIOExceptionClass=(*env)->NewGlobalRef(env, javaIOExceptionClass))==NULL) { rc=JNI_ERR; break;}
         if( (javaIOException_stringInit_MID= (*env)->GetMethodID(env, javaIOExceptionClass, "<init>", "(Ljava/lang/String;)V"))==NULL) { rc=JNI_ERR; break;}
      } 
      else { rc=JNI_ERR; break;}
/*
      // java.lang.TypeNotPresentException
      if( (javaTypeNotPresentExceptionClass=(*env)->FindClass(env, "java/lang/TypeNotPresentException"))!=NULL) {
         if( (javaTypeNotPresentExceptionClass=(*env)->NewGlobalRef(env, javaTypeNotPresentExceptionClass))==NULL) { rc=JNI_ERR; break;}
         if( (javaTypeNotPresentException_stringInit_MID= (*env)->GetMethodID(env, javaTypeNotPresentExceptionClass, "<init>", "(Ljava/lang/String;)V"))==NULL) { rc=JNI_ERR; break;}
      } 
      else { rc=JNI_ERR; break;}
*/
      // Done
      supportLoaded = 1;
      break;
   }

   return rc;
}

void OnJvmDetach(JNIEnv *env)
{
   while(1) {

      // Release the reserved resources (cashed classes, methods etc)
      (*env)->DeleteGlobalRef(env, javaBooleanClass);
      (*env)->DeleteGlobalRef(env, javaByteClass);
      (*env)->DeleteGlobalRef(env, javaCharacterClass);
      (*env)->DeleteGlobalRef(env, javaShortClass);
      (*env)->DeleteGlobalRef(env, javaIntegerClass);
      (*env)->DeleteGlobalRef(env, javaLongClass);
      (*env)->DeleteGlobalRef(env, javaFloatClass);
      (*env)->DeleteGlobalRef(env, javaDoubleClass);
      (*env)->DeleteGlobalRef(env, javaObjectClass);

      (*env)->DeleteGlobalRef(env, javaBooleanArrayClass);
      (*env)->DeleteGlobalRef(env, javaByteArrayClass);
      (*env)->DeleteGlobalRef(env, javaCharacterArrayClass);
      (*env)->DeleteGlobalRef(env, javaShortArrayClass);
      (*env)->DeleteGlobalRef(env, javaIntegerArrayClass);
      (*env)->DeleteGlobalRef(env, javaLongArrayClass);
      (*env)->DeleteGlobalRef(env, javaFloatArrayClass);
      (*env)->DeleteGlobalRef(env, javaDoubleArrayClass);
      (*env)->DeleteGlobalRef(env, javaObjectArrayClass);

      (*env)->DeleteGlobalRef(env, javaStringClass);

      (*env)->DeleteGlobalRef(env, javaOutOfMemoryErrorExceptionClass);
      (*env)->DeleteGlobalRef(env, javaNullPointerExceptionClass);
      (*env)->DeleteGlobalRef(env, javaRunTimeExceptionClass);
      (*env)->DeleteGlobalRef(env, javaIOExceptionClass);

      // Done
      break;
   }

   return;
}
