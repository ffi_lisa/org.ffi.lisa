/**
   Implements the native side callback for opc disconnects

   FFI LISA build

   Revsion 1.0 

   Date     february 2013

   Author: H�kan Pettersson, LISA-project

*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <jni.h>

// The opc client library include
#include "opcdaclient.h"

// The JNI support
#include "jnisupport.h"
#include "jnidebuglistener.h"
#include "jniopcenvironment.h"

// The disconnect... class,instance and method is used internally by this module
static jclass      disconnectListenerClass = NULL;
static jobject     disconnectListenerInstance = NULL;
static jmethodID   disconnectListener_onDisconnect_MID ;

#define JAVA_ONDISCONNECT_CALLBACK_NAME             "onDisconnect"
#define JAVA_ONDISCONNECT_CALLBACK_SIGNATURE        "(J)V"

#define DEFAULT_JNI_VERSION      JNI_VERSION_1_4

/**
* Uninitializes the disconnectListener
*/
void uninitializeDisconnectListener(JNIEnv *env)
{
   // Debug turned off so now it's save to deregister
   (*env)->DeleteGlobalRef(env, disconnectListenerClass); disconnectListenerClass=NULL;
   (*env)->DeleteGlobalRef(env, disconnectListenerInstance); disconnectListenerInstance=NULL;

   DBG_LOG("uninitializeDisconnectListener: DisconnectListener uninitialized");

}

/**
*  Prepares and cashes the callback method etc.
*/
int initializeDisconnectListener(JNIEnv *env, jobject listener)
{
   jclass clazz;
   int rc=0;

   // We only handle one listener on the native side
   if(disconnectListenerClass!=NULL) {
      uninitializeDisconnectListener(env);
   }


   // Prepare the callback method 
   if ((*env)->EnsureLocalCapacity(env, 2) == JNI_OK) {
      while(1) {
         // Disconnect notification
         if((clazz=(*env)->GetObjectClass(env, listener))!=NULL) {
            if((disconnectListenerClass=(*env)->NewGlobalRef(env, clazz))==NULL) { break; }
            if((disconnectListenerInstance = (*env)->NewGlobalRef(env, listener))==NULL) { break; }
            if((disconnectListener_onDisconnect_MID = 
               (*env)->GetMethodID( env, 
                                    disconnectListenerClass, 
                                    JAVA_ONDISCONNECT_CALLBACK_NAME, 
                                    JAVA_ONDISCONNECT_CALLBACK_SIGNATURE))==NULL) { break; }
         }

         // Done 
         rc=1;
         break;
      }
   }

   // Always delete local ref.
   (*env)->DeleteLocalRef(env, clazz);

   // Check if there were exceptions. 
   if((*env)->ExceptionCheck(env)) {(*env)->ExceptionClear(env);}

   if(rc) {
      DBG_LOG("initializeDisconnectListener: DisconnectListener initialized");
   }
   else {
      DBG_LOG("initializeDisconnectListener: Failed initializing the DisconnectListener");
   }

   return rc;
}

/**
*  Callback used to report when disconnecting from an OPC server

*/
int onDisconnect(POPC_CONNECTION connection)
{
   JNIEnv            *env = NULL;
   JavaVMAttachArgs  args = { DEFAULT_JNI_VERSION, NULL, NULL };
   jint              attachStatus;

   int      rc=1; // Assume success

   // Make shure the JVM is there
   if(cachedJVM==NULL) return 0;

   // Make shure the java callback is there
   if(disconnectListenerInstance==NULL || disconnectListener_onDisconnect_MID==NULL) return 0;

   while(1){
      // Use the cashed JVM to attach this thread to the JVM if not allready attached
      attachStatus = (*cachedJVM)->GetEnv(cachedJVM, (void **) &env, DEFAULT_JNI_VERSION);
      if(attachStatus==JNI_EDETACHED) {
         if((*cachedJVM)->AttachCurrentThread(cachedJVM, (void **)&env, &args)!=0) {
            rc=0;
            break;
         }
      }
      else if(attachStatus==JNI_EVERSION) {
         rc=0;
         break;
      }

      // Callback into JAVA side
      (*env)->CallVoidMethod( env, disconnectListenerInstance , disconnectListener_onDisconnect_MID, 
                              (jlong) connection);

      // Detach  current thread from the JVM
      if(attachStatus==JNI_EDETACHED) {
         if((*cachedJVM)->DetachCurrentThread(cachedJVM)!=0){
            break;
         }
      }

      // Done 
      break;
   }

   if(!rc) DBG_LOG("onDisconnect: Failed forwarding disconnect notification from native side server: %s", connection->server);

   return rc;
}
