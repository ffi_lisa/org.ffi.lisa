/**
   Implements the support methods for the VARIANT class

   FFI LISA build

   Revsion 1.0 

   Date     february 2013

   Author: H�kan Pettersson, LISA-project






BSTR bstr = V_BSTR(pValue);
int a = SysStringLen(bstr)+1;
char *ansistr = new char[a];
result = ::WideCharToMultiBYte(CP_ACP, 0, bstr, -1, ansistr, a, NULL,
NULL);




*/

#include <jni.h>

#include "jnisupport.h"
#include "jnidebuglistener.h"
#include "jniopcreference.h"
#include "jniopcenvironment.h"

// By javah generated include 
#include "generated/com_chalmers_lisa_jni_OpcVariant.h"

// Macro defined for the class (If classname changes and methods stay, only this macro needs modification)
#define NATIVE(func) Java_com_chalmers_lisa_jni_OpcVariant_##func
#define JCONSTANT(con)  com_chalmers_lisa_jni_OpcVariant_##con

/*
void *getValueReference(VARIANT *v)
{
   switch(v->vt) {
      case VT_I1:     result = v->cVal;        break;
      case VT_UI2:    result = v->uiVal;       break;
      case VT_UI4:    result = v->ulVal;       break;      
      case VT_UI8:    result = v->ullVal;      break;   
      case VT_INT:    result = v->intVal;      break;   
      case VT_UINT:   result = v->uintVal;     break;   
      case VT_I8:     result = v->llval;       break;   
      case VT_I4:     result = v->lVal;        break;   
      case VT_UI1:    result = v->bVal;        break;   
      case VT_I2:     result = v->iVal;        break;   
      case VT_R4:     result = v->fltVal;      break;   
      case VT_R8:     result = v->dblVal;      break;   
      case VT_BOOL:   result = v->boolVal;     break;   
      case VT_ERROR:  result = v->scode;       break;   
      case VT_CY:     result = v->cyVal;       break;   
      case VT_DATE:   result = v->date;        break;   
      case VT_BSTR:   result = v->bstrVal;     break;   
      case VT_ARRAY:  result = v->parray;      break;   

      case punkVal         // VT_UNKNOWN:
      case pdispVal        // VT_DISPATCH:
      case pbVal           // VT_BYREF|VT_UI1.
      case piVal           // VT_BYREF|VT_I2.
      case plVal           // VT_BYREF|VT_I4.
      case pllVal          // VT_BYREF|VT_I8.
      case pfltVal         // VT_BYREF|VT_R4.
      case pdblVal         // VT_BYREF|VT_R8.
      case pboolVal        // VT_BYREF|VT_BOOL.
      case pscode          // VT_BYREF|VT_ERROR.
      case pcyVal          // VT_BYREF|VT_CY.
      case pdate;           // VT_BYREF|VT_DATE.
      case pbstrVal;        // VT_BYREF|VT_BSTR.
      case ppunkVal;       // VT_BYREF|VT_UNKNOWN.
      case ppdispVal;      // VT_BYREF|VT_DISPATCH.
      case pparray;        // VT_ARRAY|*.
      case pvarVal;         // VT_BYREF|VT_VARIANT.
      case byref;           // Generic ByRef.
      case pdecVal          // VT_BYREF|VT_DECIMAL.
      case pcVal;           // VT_BYREF|VT_I1.
      case puiVal;          // VT_BYREF|VT_UI2.
      case pulVal;          // VT_BYREF|VT_UI4.
      case pullVal;         // VT_BYREF|VT_UI8.
      case pintVal;         // VT_BYREF|VT_INT.
      case puintVal;        // VT_BYREF|VT_UINT.
      default:
}
*/

JNIEXPORT void JNICALL NATIVE(create)(JNIEnv *env, jobject self)
{
   VARIANT     *v=0;

   // Make a new empty VARIANT object
   if((v = (VARIANT *) calloc(1, sizeof(VARIANT)))!=NULL) {
      VariantInit(v);
   }
   // Save the native ref of the VARIANT in the corresponding java object
   (*env)->SetLongField(env, self, javaOpcReferenceFieldID, (jlong) v );
}

JNIEXPORT void JNICALL NATIVE(release)(JNIEnv *env, jobject self)
{
   VARIANT *v = (VARIANT *) (*env)->GetLongField(env, self, javaOpcReferenceFieldID);

   if(v==NULL) {
      throwException(env, javaIOExceptionClass, "VARIANT reference illegal");
      return;
   }

   VariantClear(v);

   if((v->vt & VT_ARRAY)==VT_ARRAY) { 
      #pragma message("TODO: Must also indicate to corresponding OpcArray on the java side") 
   }

   free(v);

   // Save the native ref in the corresponding java object
   (*env)->SetLongField(env, self, javaOpcReferenceFieldID, (jlong) 0);
}


JNIEXPORT jint JNICALL NATIVE(getType)(JNIEnv *env, jobject self)
{
   VARIANT *v = (VARIANT *) (*env)->GetLongField(env, self, javaOpcReferenceFieldID);
   jint rc = JCONSTANT(VT_ERROR);

   if(v==NULL) {
      throwException(env, javaIOExceptionClass, "VARIANT reference illegal");
      return rc;
   }

   // Map to java side constants for the types we support
   if ((v->vt & VT_ARRAY)==0) {
      switch(v->vt) {
         case VT_EMPTY        : rc = JCONSTANT(VT_EMPTY)        ; break;
         case VT_NULL         : rc = JCONSTANT(VT_NULL)         ; break;
         case VT_UI1          : rc = JCONSTANT(VT_UI1)          ; break;
         case VT_UI2          : rc = JCONSTANT(VT_UI2)          ; break;
         case VT_UI4          : rc = JCONSTANT(VT_UI4)          ; break;
         case VT_UINT         : rc = JCONSTANT(VT_UINT)         ; break;
         case VT_I1           : rc = JCONSTANT(VT_I1)           ; break;
         case VT_I2           : rc = JCONSTANT(VT_I2)           ; break;
         case VT_I4           : rc = JCONSTANT(VT_I4)           ; break;
         case VT_INT          : rc = JCONSTANT(VT_INT)          ; break;
         case VT_R4           : rc = JCONSTANT(VT_R4)           ; break;
         case VT_R8           : rc = JCONSTANT(VT_R8)           ; break;
         case VT_BOOL         : rc = JCONSTANT(VT_BOOL)         ; break;
         case VT_DATE         : rc = JCONSTANT(VT_DATE)         ; break;
         case VT_BSTR         : rc = JCONSTANT(VT_BSTR)         ; break;
         case VT_CY           : rc = JCONSTANT(VT_CY)           ; break;
         case VT_DECIMAL      : rc = JCONSTANT(VT_DECIMAL)      ; break;
         case VT_DISPATCH     : rc = JCONSTANT(VT_DISPATCH)     ; break;
         case VT_VARIANT      : rc = JCONSTANT(VT_VARIANT)      ; break;
         case VT_RECORD       : rc = JCONSTANT(VT_RECORD)       ; break;
         case VT_UNKNOWN      : rc = JCONSTANT(VT_UNKNOWN)      ; break;
         case VT_ARRAY        : rc = JCONSTANT(VT_ARRAY)        ; break;
         case VT_BYREF        : rc = JCONSTANT(VT_BYREF)        ; break;
         case VT_ERROR        : rc = JCONSTANT(VT_ERROR)        ; break;
         default              : rc = JCONSTANT(VT_ERROR)        ; break;
      }
   }
   else if ((v->vt  & VT_ARRAY)==VT_ARRAY) {
      rc = JCONSTANT(VT_ARRAY);
   }

   return rc;
}

JNIEXPORT jboolean JNICALL NATIVE(getNativeBoolean)(JNIEnv *env, jobject self)
{
   VARIANT *v = (VARIANT *) (*env)->GetLongField(env, self, javaOpcReferenceFieldID);
   jboolean result=0;

   if(v==NULL) {
      #pragma message("TODO: Trow runtime exception meaning invalid reference") 
      return 0;
   }
   switch(v->vt) {
      case VT_BOOL:   result = (jboolean) v->boolVal; break;
      default:
         #pragma message("TODO: Trow runtime exception meaning invalid type") 
         break;
   }
   return result;
}


JNIEXPORT jbyte JNICALL NATIVE(getNativeByte)(JNIEnv *env, jobject self)
{
   VARIANT *v = (VARIANT *) (*env)->GetLongField(env, self, javaOpcReferenceFieldID);
   jbyte result=0;

   if(v==NULL) {
#pragma message("TODO: Trow runtime exception meaning invalid reference") 
      return 0;
   }
   switch(v->vt) {
      case VT_I1:     result = v->cVal;     break;
      case VT_UI1:    result = v->bVal;     break;
      default:
         #pragma message("TODO: Trow runtime exception meaning invalid type") 
         break;
   }
   return result;
}

JNIEXPORT jshort JNICALL NATIVE(getNativeShort)(JNIEnv *env, jobject self)
{
   VARIANT *v = (VARIANT *) (*env)->GetLongField(env, self, javaOpcReferenceFieldID);
   jshort result=0;

   if(v==NULL) {
      #pragma message("TODO: Trow runtime exception meaning invalid reference") 
      return 0;
   }
   switch(v->vt) {
      case VT_I1:     result = v->cVal;        break;
      case VT_I2:     result = v->iVal;        break;   
      case VT_UI1:    result = v->bVal;        break;   
      case VT_UI2:    result = v->uiVal;       break;
      default:
         #pragma message("TODO: Trow runtime exception meaning invalid type") 
         break;
   }
   return result;
}

JNIEXPORT jint JNICALL NATIVE(getNativeInt)(JNIEnv *env, jobject self)
{
   VARIANT *v = (VARIANT *) (*env)->GetLongField(env, self, javaOpcReferenceFieldID);
   jint result=0;

   if(v==NULL) {
      #pragma message("TODO: Trow runtime exception meaning invalid reference") 
      return 0;
   }
   switch(v->vt) {
      case VT_I1:     result = v->cVal;        break;
      case VT_I2:     result = v->iVal;        break;   
      case VT_I4:     result = v->lVal;        break;   
      case VT_UI1:    result = v->bVal;        break;   
      case VT_UI2:    result = v->uiVal;       break;
      case VT_UI4:    result = v->ulVal;       break;      
      case VT_INT:    result = v->intVal;      break;   
      case VT_UINT:   result = v->uintVal;     break;   
      default:
         #pragma message("TODO: Trow runtime exception meaning invalid type") 
         break;
   }
   return result;
}

JNIEXPORT jlong JNICALL NATIVE(getNativeLong)(JNIEnv *env, jobject self)
{
   VARIANT *v = (VARIANT *) (*env)->GetLongField(env, self, javaOpcReferenceFieldID);
   jlong result=0;

   if(v==NULL) {
      #pragma message("TODO: Trow runtime exception meaning invalid reference") 
      return 0;
   }
   switch(v->vt) {
      case VT_I1:     result = v->cVal;         break;
      case VT_I2:     result = v->iVal;        break;   
      case VT_I4:     result = v->lVal;        break;   
      case VT_UI1:    result = v->bVal;        break;   
      case VT_UI2:    result = v->uiVal;       break;
      case VT_UI4:    result = v->ulVal;       break;      
      case VT_INT:    result = v->intVal;      break;   
      case VT_UINT:   result = v->uintVal;     break;   
      case VT_I8:     result = v->llVal;       break;   
      case VT_UI8:    result = v->ullVal;      break;   
      default:
         #pragma message("TODO: Trow runtime exception meaning invalid type") 
         break;
   }
   return result;
}

JNIEXPORT jfloat JNICALL NATIVE(getNativeFloat)(JNIEnv *env, jobject self)
{
   VARIANT *v = (VARIANT *) (*env)->GetLongField(env, self, javaOpcReferenceFieldID);
   jfloat result=0;

   if(v==NULL) {
      #pragma message("TODO: Trow runtime exception meaning invalid reference") 
      return 0;
   }
   switch(v->vt) {
      case VT_R4:     result = v->fltVal;      break;   
      default:
         #pragma message("TODO: Trow runtime exception meaning invalid type") 
         break;
   }
   return result;
}

JNIEXPORT jdouble JNICALL NATIVE(getNativeDouble)(JNIEnv *env, jobject self)
{
   VARIANT *v = (VARIANT *) (*env)->GetLongField(env, self, javaOpcReferenceFieldID);
   jdouble result=0;

   if(v==NULL) {
      #pragma message("TODO: Trow runtime exception meaning invalid reference") 
      return 0;
   }
   switch(v->vt) {
      case VT_R4:     result = v->fltVal;      break;   
      case VT_R8:     result = v->dblVal;      break;   
      default:
         #pragma message("TODO: Trow runtime exception meaning invalid type") 
         break;
   }
   return result;
}


// public native String getNativeString();

JNIEXPORT jstring JNICALL NATIVE(getNativeString)(JNIEnv *env, jobject self)
{
   VARIANT *v = (VARIANT *) (*env)->GetLongField(env, self, javaOpcReferenceFieldID);

   jstring  result=0; 
   BSTR     ptr;

   if(v==NULL) {
      #pragma message("TODO: Trow runtime exception meaning invalid reference") 
      return 0;
   }

   if(v->vt==VT_BSTR) {
      ptr = v->bstrVal;
      if((result = newJavaStringFromCharacters(env, ptr))==NULL) {
         #pragma message("TODO: Trow runtime exception meaning invalid type") 
      }
   }
   else {
      #pragma message("TODO: Trow runtime exception meaning invalid type") 
   }

   return result;
}

JNIEXPORT jlong JNICALL NATIVE(getNativeDate)(JNIEnv *env, jobject self)
{
   VARIANT *v = (VARIANT *) (*env)->GetLongField(env, self, javaOpcReferenceFieldID);
   jlong result=0;

   SYSTEMTIME  st;
   FILETIME    ft;

   if(v==NULL) {
      #pragma message("TODO: Trow runtime exception meaning invalid reference") 
      return 0;
   }
   switch(v->vt) {
      case VT_DATE:
         VariantTimeToSystemTime(v->date, &st);  
         SystemTimeToFileTime(&st, &ft);
         result = ((*(__int64 *)(&ft)) - (*(__int64 *) &utcBaseTime))/10000;
         break;
      default:
         #pragma message("TODO: Trow runtime exception meaning invalid type") 
         break;
   }
   return result;
}
