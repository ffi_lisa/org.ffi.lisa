#if !defined(_OPC_REFERENCE)
#define _OPC_REFERENCE

extern jclass      javaOpcReferenceClass;
extern jfieldID    javaOpcReferenceFieldID;


#include <jni.h>
#include <wtypes.h>

// Needed includes 
#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif   // #if !defined(_OPC_REFERENCE)
