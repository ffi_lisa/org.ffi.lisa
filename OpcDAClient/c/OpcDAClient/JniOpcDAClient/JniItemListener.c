/**
   Implements the native side callback for OpcItemNotifications

   FFI LISA build

   Revsion 1.0 

   Date     february 2013

   Author: H�kan Pettersson, LISA-project

*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <jni.h>

// The opc client library include
#include "opcdaclient.h"

// The JNI support
#include "jnisupport.h"
#include "jnidebuglistener.h"
#include "jniopcenvironment.h"
#include "jniopcvariant.h"

// The item... class,instance and method is used internally by this module
static jclass      itemListenerClass = NULL;
static jobject     itemListenerInstance = NULL;
static jmethodID   itemListener_onItemNotification_MID ;

#define JAVA_ONITEMNOTIFICATION_CALLBACK_NAME             "onItemNotification"
//#define JAVA_ONITEMNOTIFICATION_CALLBACK_SIGNATURE        "(JJJJ)V"
#define JAVA_ONITEMNOTIFICATION_CALLBACK_SIGNATURE		  "(JLjava/util/Date;JJ)V"

#define DEFAULT_JNI_VERSION      JNI_VERSION_1_4


/**
* Uninitializes the itemListener
*/
void uninitializeItemListener(JNIEnv *env)
{
   // Debug turned off so now it's save to deregister
   (*env)->DeleteGlobalRef(env, itemListenerClass); itemListenerClass=NULL;
   (*env)->DeleteGlobalRef(env, itemListenerInstance); itemListenerInstance=NULL;

   DBG_LOG("uninitializeItemListener: ItemListener uninitialized");

}

/**
*  Prepares and cashes the callback method etc.
*/
int initializeItemListener(JNIEnv *env, jobject listener)
{
   jclass clazz;
   int rc=0;

   // We only handle one item listener on the native side
   if(itemListenerClass!=NULL) {
      uninitializeItemListener(env);
   }


   // Prepare the callback method 
   if ((*env)->EnsureLocalCapacity(env, 2) == JNI_OK) {
      while(1) {
         // Item notification
         if((clazz=(*env)->GetObjectClass(env, listener))!=NULL) {
            if((itemListenerClass=(*env)->NewGlobalRef(env, clazz))==NULL) { break; }
            if((itemListenerInstance = (*env)->NewGlobalRef(env, listener))==NULL) { break; }
            if((itemListener_onItemNotification_MID = 
               (*env)->GetMethodID( env, 
                                    itemListenerClass, 
                                    JAVA_ONITEMNOTIFICATION_CALLBACK_NAME, 
                                    JAVA_ONITEMNOTIFICATION_CALLBACK_SIGNATURE))==NULL) { break; }
         }

         // Done 
         rc=1;
         break;
      }
   }

   // Always delete local ref.
   (*env)->DeleteLocalRef(env, clazz);

   // Check if there were exceptions. 
   if((*env)->ExceptionCheck(env)) {(*env)->ExceptionClear(env);}

   if(rc) {
      DBG_LOG("initializeItemListener: ItemListener initialized");
   }
   else {
      DBG_LOG("initializeItemListener: Failed initializing ItemListener ");
   }

   return rc;
}

/**
*  Callback used to report any change to any item that has a registered subcribtion

*/
int onItemNotification(POPC_ITEM item, FILETIME *ft, DWORD quality, VARIANT  *value)
{
   JNIEnv            *env = NULL;
   JavaVMAttachArgs  args = { DEFAULT_JNI_VERSION, NULL, NULL };
   jint              attachStatus;

   int      rc=1; // Assume success

   if(item!=NULL) DBG_LOG("onItemNotification: Native side ItemNotification for VARIANT:%s of VARTYPE:%s", item->name, variantTypeToString(value->vt));

   // Make shure the JVM is there
   if(cachedJVM==NULL) return 0;

   // Make shure the java callback is there
   if(itemListenerInstance==NULL || itemListener_onItemNotification_MID==NULL) return 0;

   while(1){
      // Use the cashed JVM to attach this thread to the JVM if not allready attached
      attachStatus = (*cachedJVM)->GetEnv(cachedJVM, (void **) &env, DEFAULT_JNI_VERSION);
      if(attachStatus==JNI_EDETACHED) {
         if((*cachedJVM)->AttachCurrentThread(cachedJVM, (void **)&env, &args)!=0) {
            rc=0;
            break;
         }
      }
      else if(attachStatus==JNI_EVERSION) {
         rc=0;
         break;
      }

      // Callback into JAVA side
      (*env)->CallVoidMethod( env, itemListenerInstance , itemListener_onItemNotification_MID, 
                              (jlong) item,
							  newDate(env, (jlong) (((*(__int64 *)(ft)) - (*(__int64 *) &utcBaseTime))/10000)),  
                              (jlong) quality,
                              (jlong) value);

      // Detach  current thread from the JVM
      if(attachStatus==JNI_EDETACHED) {
         if((*cachedJVM)->DetachCurrentThread(cachedJVM)!=0){
            break;
         }
      }

      // Done 
      break;
   }

   if(!rc) DBG_LOG("onItemNotification: Failed forwarding item notification from native side: %s", item->name);

   return rc;
}
