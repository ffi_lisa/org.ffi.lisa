#if !defined(_OPC_ENVIRONMENT)
#define _OPC_ENVIRONMENT

extern FILETIME   utcBaseTime;

#include <jni.h>
#include <wtypes.h>

// Needed includes 
#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif   // #if !defined(_OPC_ENVIRONMENT)
