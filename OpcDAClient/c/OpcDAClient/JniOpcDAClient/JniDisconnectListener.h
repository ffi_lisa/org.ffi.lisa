#if !defined(_JNI_DISCONNECTLISTENER )
#define _JNI_DISCONNECTLISTENER 

#include <jni.h>

// Needed includes 
#ifdef __cplusplus
extern "C" {
#endif

/**
*  Uninitializes the Listener
*/
void uninitializeDisconnectListener(JNIEnv *env);

/**
*  Prepares and caches the callback methods for the DebugListener
*/
int initializeDisconnectListener(JNIEnv *env, jobject listener);

/**
*  The native side callback that in turn calls the java one
*/
int onDisconnect(POPC_CONNECTION connection);

#ifdef __cplusplus
}
#endif

#endif   // #if !defined(_JNI_DISCONNECTLISTENER)
