/**
   Implements the support function for the native VARIANT class

   FFI LISA build

   Revsion 1.0 

   Date     march 2013

   Author: H�kan Pettersson, LISA-project
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include <jni.h>

#include "opcdaclient.h"
#include "jnisupport.h"
#include "jnidebuglistener.h"
#include "jniopcenvironment.h"

/*
	Creates a new uinitialized VARIANT
*/
VARIANT *createEmptyVariant()
{
   VARIANT     *v=NULL;

   // Create a new empty VARIANT object
   if((v = (VARIANT *) calloc(1, sizeof(VARIANT)))!=NULL) {
      VariantInit(v);
   }
   return v;
}


/*
	Destroys a VARIANT
*/
void destroyVariant(VARIANT **v)
{
	if(*v!=NULL) {
		VariantClear(*v);
		free(*v);
		*v=NULL;
	}
}


char * variantTypeToString(VARTYPE vt)
{
   if ((vt & VT_ARRAY)==0) {
      switch(vt) {
	     case VT_BOOL:			return("VT_BOOL");
         case VT_UI1:  			return("VT_UI1");
         case VT_I1:   			return("VT_I1");
         case VT_I2:   			return("VT_I2");
         case VT_UI2:  			return("VT_UI2");
         case VT_I4:   			return("VT_I4");
         case VT_UI4:  			return("VT_UI4");
         case VT_INT:  			return("VT_INT");
         case VT_UINT: 			return("VT_UINT");
         case VT_I8:   			return("VT_I8");   
         case VT_UI8:  			return("VT_UI8");   
         case VT_R4:   			return("VT_R4");
         case VT_R8:   			return("VT_R8");
         case VT_BSTR: 			return("VT_BSTR");
		 case VT_DATE:			return("VT_DATE");
         case VT_EMPTY:			return("VT_EMPTY");
         case VT_NULL:			return("VT_NULL");
		 case VT_CY:			return("VT_CY");
		 case VT_DECIMAL:		return("VT_DECIMAL");
		 case VT_DISPATCH:		return("VT_DISPATCH");
		 case VT_VARIANT:		return("VT_VARIANT");
		 case VT_RECORD:		return("VT_RECORD");
		 case VT_UNKNOWN:		return("VT_UNKNOWN");
		 case VT_ARRAY:			return("VT_ARRAY");
		 case VT_BYREF:			return("VT_BYREF");
		 case VT_ERROR:			return("VT_ERROR");
		 default:	            return("Unknown");
      }
   }
   else if ((vt & VT_ARRAY)==VT_ARRAY) {
      switch(vt ^ VT_ARRAY) {
	     case VT_BOOL:			return("VT_BOOL[]");
         case VT_UI1:  			return("VT_UI1[]");
         case VT_I1:   			return("VT_I1[]");
         case VT_I2:   			return("VT_I2[]");
         case VT_UI2:  			return("VT_UI2[]");
         case VT_I4:   			return("VT_I4[]");
         case VT_UI4:  			return("VT_UI4[]");
         case VT_INT:  			return("VT_INT[]");
         case VT_UINT: 			return("VT_UINT[]");
         case VT_I8:   			return("VT_I8[]");   
         case VT_UI8:  			return("VT_UI8[]");   
         case VT_R4:   			return("VT_R4[]");
         case VT_R8:   			return("VT_R8[]");
		 default:	            return("Unsupported[]");
	  }
   }
   else return("Unknown");

}


char * javaTypeToString(JNIEnv *env, jobject javaValue)
{
	// Boolean
	if      ( (*env)->IsInstanceOf(env, javaValue, javaBooleanClass))		{ return("Boolean"); }
	else if ( (*env)->IsInstanceOf(env, javaValue, javaByteClass))			{ return("Byte"); }
	else if ( (*env)->IsInstanceOf(env, javaValue, javaCharacterClass))		{ return("Character"); }
	else if ( (*env)->IsInstanceOf(env, javaValue, javaShortClass))			{ return("Short"); }
	else if ( (*env)->IsInstanceOf(env, javaValue, javaIntegerClass))		{ return("Integer"); }
	else if ( (*env)->IsInstanceOf(env, javaValue, javaLongClass))			{ return("Long"); }
	else if ( (*env)->IsInstanceOf(env, javaValue, javaFloatClass))			{ return("Float"); }
	else if ( (*env)->IsInstanceOf(env, javaValue, javaDoubleClass))		{ return("Double"); }
	else if ( (*env)->IsInstanceOf(env, javaValue, javaStringClass))	    { return("String"); }
	else if ( (*env)->IsInstanceOf(env, javaValue, javaDateClass))	        { return("Date"); }
	else if ( (*env)->IsInstanceOf(env, javaValue, javaBooleanArrayClass))	{ return("Boolean[]"); }
	else if ( (*env)->IsInstanceOf(env, javaValue, javaByteArrayClass))		{ return("Byte[]"); }
	else if ( (*env)->IsInstanceOf(env, javaValue, javaCharacterArrayClass)){ return("Character[]"); }
	else if ( (*env)->IsInstanceOf(env, javaValue, javaShortArrayClass))	{ return("Short[]"); }
	else if ( (*env)->IsInstanceOf(env, javaValue, javaIntegerArrayClass))	{ return("Integer[]"); }
	else if ( (*env)->IsInstanceOf(env, javaValue, javaLongArrayClass))		{ return("Long"); }
	else if ( (*env)->IsInstanceOf(env, javaValue, javaFloatArrayClass))	{ return("Float[]"); }
	else if ( (*env)->IsInstanceOf(env, javaValue, javaDoubleArrayClass))	{ return("Double[]"); }

	else return("Unsupported");
}


/**
	Converts a VARIANT type to a Java type
*/
jobject variantToJava(JNIEnv *env, VARIANT *v)
{

   BSTR stringPtr;
   jboolean booleanValue;
   jbyte byteValue;  
   jshort shortValue;  
   jint intValue;  
   jlong longValue;
   jfloat floatValue;
   jdouble doubleValue;

   jobject result=NULL;

   if ((v->vt & VT_ARRAY)==0) {
      switch(v->vt) {
	     // Boolean
	     case VT_BOOL: booleanValue = (jboolean) v->boolVal;  result = (*env)->NewObject(env, javaBooleanClass, javaBoolean_booleanInit_MID, booleanValue);  break;
	     // Byte
         case VT_UI1:  byteValue = v->bVal;       result = (*env)->NewObject(env, javaByteClass, javaByte_byteInit_MID, byteValue);  break;   
         case VT_I1:   byteValue = v->cVal;       result = (*env)->NewObject(env, javaByteClass, javaByte_byteInit_MID, byteValue);  break;   
		 // Short
         case VT_I2:   shortValue = v->iVal;      result = (*env)->NewObject(env, javaShortClass, javaShort_shortInit_MID, shortValue);  break;   
         case VT_UI2:  shortValue = v->uiVal;     result = (*env)->NewObject(env, javaShortClass, javaShort_shortInit_MID, shortValue);  break;
		 // Integer
         case VT_I4:   intValue = v->lVal;        result = (*env)->NewObject(env, javaIntegerClass, javaInteger_intInit_MID, intValue);  break;   
         case VT_UI4:  intValue = v->ulVal;       result = (*env)->NewObject(env, javaIntegerClass, javaInteger_intInit_MID, intValue);  break;      
         case VT_INT:  intValue = v->intVal;      result = (*env)->NewObject(env, javaIntegerClass, javaInteger_intInit_MID, intValue);  break;   
         case VT_UINT: intValue = v->uintVal;     result = (*env)->NewObject(env, javaIntegerClass, javaInteger_intInit_MID, intValue);  break;   
		 // Long
         case VT_I8:   longValue = v->llVal;      result = (*env)->NewObject(env, javaLongClass, javaLong_longInit_MID, longValue);  break;   
         case VT_UI8:  longValue = v->ullVal;     result = (*env)->NewObject(env, javaLongClass, javaLong_longInit_MID, longValue);  break;   
		 // Float
         case VT_R4:   floatValue = v->fltVal;    result = (*env)->NewObject(env, javaFloatClass, javaFloat_floatInit_MID, floatValue);  break;
		 // Double
         case VT_R8:   doubleValue = v->dblVal;   result = (*env)->NewObject(env, javaDoubleClass, javaDouble_doubleInit_MID, doubleValue);  break;
		 // String
         case VT_BSTR: stringPtr = v->bstrVal;    result = newJavaStringFromCharacters(env, (jchar *) stringPtr);  break;
		 case VT_DATE:
			{
				jlong       timeStamp;
				SYSTEMTIME  st;
				FILETIME    ft;
				VariantTimeToSystemTime(v->date, &st);  
				SystemTimeToFileTime(&st, &ft);
				timeStamp = ((*(__int64 *)(&ft)) - (*(__int64 *) &utcBaseTime))/10000;
				result = newDate(env, timeStamp );
			}
			break;
		 // unsupported/not yet implemented
		 /*
         case VT_EMPTY:
         case VT_NULL:

		 case VT_CY:
		 case VT_DECIMAL:
		 case VT_DISPATCH:
		 case VT_VARIANT:
		 case VT_RECORD:
		 case VT_UNKNOWN:
		 case VT_ARRAY:
		 case VT_BYREF:
		 case VT_ERROR:
		 */
		 default:
			 break;
      }
   }
   else if ((v->vt  & VT_ARRAY)==VT_ARRAY) {
	   int i, len;

	   int nDim;
	   long minIndex;
	   long maxIndex;
	   void *pdata;
	   jobject  valueArray;
	   jobject  valueItem;

	   VARTYPE type=v->vt ^ VT_ARRAY;
	   nDim = SafeArrayGetDim(v->parray);

	   // Accept one dimensional arays
	   if(nDim==1) {
 	       SafeArrayGetLBound(v->parray,1,&minIndex);
 	       SafeArrayGetUBound(v->parray,1,&maxIndex);
		   len=maxIndex-minIndex+1;
		   SafeArrayAccessData(v->parray, &pdata); 

		   switch(type) {
				// Boolean
				case VT_BOOL: 
					// jboolean and VARIANT_BOOL are of diffent size. Need to take them one by one
					valueArray = (*env)->NewBooleanArray(env, len);
					for(i=minIndex=0;i<maxIndex;i++) {
						valueItem = newBoolean(env, (jboolean) *(((VARIANT_BOOL *)pdata)+i) );
						(*env)->SetObjectArrayElement(env, valueArray, i, valueItem);
						if((*env)->ExceptionCheck(env)) break;
					}
					result=valueArray;
					break;
				// Byte
				case VT_UI1:
				case VT_I1:   
					valueArray = (*env)->NewByteArray(env, len); 
					(*env)->SetByteArrayRegion(env, valueArray, 0, len, (BYTE *) pdata);
					if((*env)->ExceptionCheck(env)) break;
					result=valueArray;
					break;
				// Short
				case VT_I2:
				case VT_UI2:
					valueArray = (*env)->NewShortArray(env, len); 
					(*env)->SetShortArrayRegion(env, valueArray, 0, len, (SHORT *) pdata);
					if((*env)->ExceptionCheck(env)) break;
					result=valueArray;
					break;
				// Integer
				case VT_INT:
				case VT_UINT:
				case VT_I4:
				case VT_UI4:
					valueArray = (*env)->NewIntArray(env, len); 
					(*env)->SetIntArrayRegion(env, valueArray, 0, len, (INT *) pdata);
					if((*env)->ExceptionCheck(env)) break;
					result=valueArray;
					break;
				// Long
				case VT_I8:
				case VT_UI8:
					valueArray = (*env)->NewLongArray(env, len); 
					(*env)->SetLongArrayRegion(env, valueArray, 0, len, (LONGLONG *) pdata);
					if((*env)->ExceptionCheck(env)) break;
					result=valueArray;
					break;
				// Float
				case VT_R4:
					valueArray = (*env)->NewFloatArray(env, len); 
					(*env)->SetFloatArrayRegion(env, valueArray, 0, len, (FLOAT *) pdata);
					if((*env)->ExceptionCheck(env)) break;
					result=valueArray;
					break;
				// Double
				case VT_R8:
					valueArray = (*env)->NewDoubleArray(env, len); 
					(*env)->SetDoubleArrayRegion(env, valueArray, 0, len, (DOUBLE *) pdata);
					if((*env)->ExceptionCheck(env)) break;
					result=valueArray;
					break;
		   }
		   SafeArrayUnaccessData(v->parray); 
	   }
   }

   // Return what we got
   return result;
}


/**
	Converts a java simple type to the corresponding VARIANT
*/
VARIANT *javaToSimpleVariant(JNIEnv *env, VARTYPE type, jobject javaValue)
{
	VARIANT *v;

	if((v=createEmptyVariant())==NULL) return NULL;

	if((*env)->IsInstanceOf(env, javaValue, javaBooleanClass) && type==VT_BOOL) {
		v->boolVal = getBooleanValue(env, javaValue);
		v->vt = VT_BOOL;
	}
	else if ( (*env)->IsInstanceOf(env, javaValue, javaByteClass)  && (type==VT_I1) ) {
		v->cVal = getByteValue(env, javaValue);
		v->vt = VT_I1;
	}
	else if ( (*env)->IsInstanceOf(env, javaValue, javaByteClass)  && (type==VT_UI1) ) {
		v->bVal = getByteValue(env, javaValue);
		v->vt = VT_UI1;
	}
	else if ( (*env)->IsInstanceOf(env, javaValue, javaShortClass)  && (type==VT_I2) ) {
		v->iVal = getShortValue(env, javaValue);
		v->vt = VT_I2;
	}
	else if ( (*env)->IsInstanceOf(env, javaValue, javaShortClass)  && (type==VT_UI2) ) {
		v->uiVal = getShortValue(env, javaValue);
		v->vt = VT_UI2;
	}
	else if ( (*env)->IsInstanceOf(env, javaValue, javaIntegerClass)  && (type==VT_I4) ) {
		v->lVal = getIntegerValue(env, javaValue);
		v->vt = VT_I4;
	}
	else if ( (*env)->IsInstanceOf(env, javaValue, javaIntegerClass)  && (type==VT_UI4) ) {
		v->ulVal  = getIntegerValue(env, javaValue);
		v->vt = VT_UI4;
	}
	else if ( (*env)->IsInstanceOf(env, javaValue, javaIntegerClass)  && (type==VT_INT) ) {
		v->intVal = getIntegerValue(env, javaValue);
		v->vt = VT_INT;
	}
	else if ( (*env)->IsInstanceOf(env, javaValue, javaIntegerClass)  && (type==VT_UINT ) ) {
		v->uintVal = getIntegerValue(env, javaValue);
		v->vt = VT_UINT ;
	}
	else if ( (*env)->IsInstanceOf(env, javaValue, javaLongClass)  && (type==VT_I8) ) {
		v->llVal = getLongValue(env, javaValue);
		v->vt = VT_I8;
	}
	else if ( (*env)->IsInstanceOf(env, javaValue, javaLongClass)  && (type==VT_UI8) ) {
		v->ullVal = getLongValue(env, javaValue);
		v->vt = VT_UI8;
	}
	else if ( (*env)->IsInstanceOf(env, javaValue, javaFloatClass)  && (type==VT_R4) ) {
		v->fltVal = getFloatValue(env, javaValue);
		v->vt = VT_R4;
	}
	else if ( (*env)->IsInstanceOf(env, javaValue, javaDoubleClass)  && (type==VT_R8) ) {
		v->dblVal = getDoubleValue(env, javaValue);;
		v->vt = VT_R8;
	}
	else if ( (*env)->IsInstanceOf(env, javaValue, javaStringClass)  && (type==VT_BSTR) ) {
		v->bstrVal = SysAllocString((BSTR) getCharsFromJavaString(env, (jstring) javaValue));
		v->vt = VT_BSTR;
	}
	else if ( (*env)->IsInstanceOf(env, javaValue, javaDateClass)  && (type==VT_DATE) ) {
		SYSTEMTIME  st;
		FILETIME    ft;
		jlong timeStamp=getDateValue(env, javaValue)*10000+(*(__int64 *) &utcBaseTime);
		*((__int64 *)(&ft))=timeStamp;
		FileTimeToSystemTime(&ft, &st);
		SystemTimeToVariantTime(&st, &v->date);  
		v->vt = VT_DATE;
	}

	// Make sure that we actually did something
	if(v->vt==VT_EMPTY) destroyVariant(&v);

	return v;
}

/**
	Converts a java array type to the corresponding VARIANT
*/
VARIANT *javaToArrayVariant(JNIEnv *env, VARTYPE varType, int varSize, jobject javaValue)
{
	VARIANT *v;
	SAFEARRAY *arr;
	void *arrData;
	int length;

	if((v=createEmptyVariant())==NULL) return NULL;
	if((arr=SafeArrayCreateVector(varType, 0, varSize))==NULL)   {
		destroyVariant(&v);
		return NULL;
	}

	length = (*env)->GetArrayLength(env, javaValue);
	SafeArrayAccessData(arr, &arrData); 

	if ( (*env)->IsInstanceOf(env, javaValue, javaBooleanArrayClass) && varType== VT_BOOL && varSize>=length) {
		// jboolean and VARIANT_BOOL are of diffent size. Need to take them one by one
		VARIANT_BOOL *bvect= (VARIANT_BOOL *) arrData;
		jboolean *barr;
		jboolean isCopy;
		int i;

		barr=(*env)->GetBooleanArrayElements(env, javaValue, &isCopy);
		for(i=0;i<varSize;i++) bvect[i] = barr[i];
		if(isCopy) (*env)->ReleaseBooleanArrayElements(env, javaValue, barr, JNI_ABORT);
		v->parray=arr; v->vt=VT_ARRAY|varType;
	}
	else if ( (*env)->IsInstanceOf(env, javaValue, javaByteArrayClass) && (varType==VT_I1||varType==VT_UI1) && varSize>=length) { 
		(*env)->GetByteArrayRegion(env, javaValue, 0, length , arrData);
		v->parray=arr; v->vt=VT_ARRAY|varType;
	}
	else if ( (*env)->IsInstanceOf(env, javaValue, javaShortArrayClass) && (varType==VT_I2||varType==VT_UI2) && varSize>=length) { 
		(*env)->GetShortArrayRegion(env, javaValue, 0, length , arrData);
		v->parray=arr; v->vt=VT_ARRAY|varType;
	}
	else if ( (*env)->IsInstanceOf(env, javaValue, javaIntegerArrayClass) && (varType==VT_I4||varType==VT_UI4||varType==VT_INT||varType==VT_UINT) && varSize>=length) { 
		(*env)->GetIntArrayRegion(env, javaValue, 0, length , arrData);
		v->parray=arr; v->vt=VT_ARRAY|varType;
	}
	else if ( (*env)->IsInstanceOf(env, javaValue, javaLongArrayClass) &&  (varType==VT_I8||varType==VT_UI8) && varSize>=length) { 
		(*env)->GetLongArrayRegion(env, javaValue, 0, length , arrData);
		v->parray=arr; v->vt=VT_ARRAY|varType;
	}
	else if ( (*env)->IsInstanceOf(env, javaValue, javaFloatArrayClass) && varType== VT_R4 && varSize>=length) { 
		(*env)->GetFloatArrayRegion(env, javaValue, 0, length , arrData);
		v->parray=arr; v->vt=VT_ARRAY|varType;
	}
	else if ( (*env)->IsInstanceOf(env, javaValue, javaDoubleArrayClass) && varType== VT_R8 && varSize>=length) { 
		(*env)->GetDoubleArrayRegion(env, javaValue, 0, length , arrData);
		v->parray=arr; v->vt=VT_ARRAY|varType;
	}

	SafeArrayUnaccessData(arr); 

	// Make sure that we actually did something
	if(v->vt==VT_EMPTY) {
		destroyVariant(&v);
		SafeArrayDestroy(arr);
	}		

	return v;
}


VARIANT *javaToVariant(JNIEnv *env, VARIANT *vartmpl, jobject javaValue)
{

	VARIANT *result=NULL;

	if((vartmpl->vt & VT_ARRAY)==VT_ARRAY) {
	   long minIndex;
	   long maxIndex;
	   // Accept one dimensional arays
	   if(SafeArrayGetDim(vartmpl->parray)==1) {
 	       SafeArrayGetLBound(vartmpl->parray,1,&minIndex);
 	       SafeArrayGetUBound(vartmpl->parray,1,&maxIndex);
		   result = javaToArrayVariant(env, vartmpl->vt^VT_ARRAY , maxIndex-minIndex+1, javaValue);
	   }
	}
	else {
		result = javaToSimpleVariant(env, vartmpl->vt, javaValue);
	}
	return result;
}




