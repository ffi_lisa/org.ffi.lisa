#if !defined(_JNI_DEBUGLISTENER )
#define _JNI_DEBUGLISTENER 

#include <jni.h>

// Needed includes 
#ifdef __cplusplus
extern "C" {
#endif

extern int debugEnabled;

#define DBG_LOG   !debugEnabled ? (int)0 : onDebugMessage

/**
*  Uninitializes the DebugListener
*/
void uninitializeDebugListener(JNIEnv *env);

/**
*  Prepares and caches the callback methods for the DebugListener
*/
int initializeDebugListener(JNIEnv *env, jobject listener, jboolean enabled);

/**
*  The native side callback that in turn calls the java one
*/
int onDebugMessage(char *f, ...);

#ifdef __cplusplus
}
#endif

#endif   // #if !defined(_JNI_DEBUGLISTENER)



