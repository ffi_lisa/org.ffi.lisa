/**
   Implements the native methods for the OpcReference class

   FFI LISA build

   Revsion 1.0 

   Date     february 2013

   Author: H�kan Pettersson, LISA-project

*/
#include <jni.h>
#include "jnisupport.h"

// By javah generated include 
#include "generated/org_ffi_lisa_opc_jni_OpcReference.h"

// Macro defined for the class (If classname changes and methods stay, only this macro needs modification)
#define NATIVE(func) Java_org_ffi_lisa_opc_jni_OpcReference_##func
#define CONSTANT(con)  org_ffi_lisa_opc_jni_OpcReference_##con

// OPCReference
#define JAVA_REFERENCE_FIELD_NAME         "reference"
#define JAVA_REFERENCE_FIELD_SIGNATURE    "J"

// Globals
jclass      javaOpcReferenceClass;
jfieldID    javaOpcReferenceFieldID;

/**
   The initReference method from the OPCReference class. We make shure we save the reference 
   fieldID so that we can reach it from  all classes that extends the OPCReference class
   Only make this once
*/
static jboolean isInitialized=0;

JNIEXPORT void JNICALL NATIVE(initReference)(JNIEnv *env, jobject self)
{
   if(isInitialized) return;

   while(1) {
      // Cache the Java side environment class
      javaOpcReferenceClass = (*env)->GetObjectClass(env, self);
      if (javaOpcReferenceClass == NULL) break;

      // Get field ID of the reference storage
      javaOpcReferenceFieldID = (*env)->GetFieldID(env, javaOpcReferenceClass , JAVA_REFERENCE_FIELD_NAME, JAVA_REFERENCE_FIELD_SIGNATURE);
      if (javaOpcReferenceFieldID == NULL) break;

      // Done
      isInitialized=1;
      break;
   }

   if(!isInitialized) {
      throwException(env, javaIOExceptionClass, "Failed storing the reference field ID from the OpcReference class");
   }
}
