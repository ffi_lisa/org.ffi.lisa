/**
   @file OpcDAClient.h 

   Include file for the OPC-DA client implementation in pure C
*/

#if !defined(_OPCCLIENT_INCLUDED)
#define _OPCCLIENT_INCLUDED

// Needed includes 
#include <wtypes.h>     // Window types etc

#ifdef __cplusplus
extern "C" {
#endif

#define  OPC_SUCCESS       0     
#define  OPC_ERROR         -1

#define  OPC_MAX_SERVERS            100
#define  OPC_MAX_PROPERTYNAME_SIZE  1000
#define  OPC_MAX_ITEMNAME_SIZE      1000
#define  OPC_MAX_GROUPNAME_SIZE     1000
#define  OPC_MAX_SERVERNAME_SIZE    1000
#define  OPC_MAX_VENDOR_SIZE        1000


typedef int(*PFN_FORWARD_VALUECHANGE)(struct _OPC_ITEM *item, 
                                      FILETIME *filetime,
                                      DWORD    quality,
                                      VARIANT  *value);
typedef int(*PFN_FORWARD_ERROR)(DWORD code, LPTSTR message);
typedef int(*PFN_FORWARD_DCOMMSG)(LPTSTR message);
typedef int(*PFN_FORWARD_DISCONNECT)(struct _OPC_CONNECTION *);


typedef  struct _OPC_CONNECTION {
   char                       *server;       // Server name
   char                       *host;         // Host where server is started
   HANDLE                     libHandle;     // Original handle
   PFN_FORWARD_DISCONNECT     pfnDisconnect; // Callback for server disconnect
   PFN_FORWARD_VALUECHANGE    pfnForward;    // Item notification forward function
   BOOL                       isDisconnected;// Server disconnect flag
   void                       *userInfo;     // User info related to connection
} OPC_CONNECTION, *POPC_CONNECTION;
// typedef  struct _OPC_CONNECTION  OPC_CONNECTION, *POPC_CONNECTION;



typedef  struct _OPC_GROUP {
   POPC_CONNECTION            connection;    // Connection group belongs to
   HANDLE                     libHandle;     // Original handle
   PFN_FORWARD_VALUECHANGE    pfnForward;    // Item notification forward function
   void                       *userInfo;     // User info related to group
   char                       name[0];       // Group name
} OPC_GROUP, *POPC_GROUP;


typedef  struct _OPC_PROPERTY {
   DWORD    id;                     // Property ID
   VARIANT  value;                  // Value
   char     description[0];         // Description
} OPC_PROPERTY, *POPC_PROPERTY;

typedef  struct _OPC_ITEM {
   POPC_GROUP                 group;         // Group item belongs to
   HANDLE                     libHandle;     // Original handle
   PFN_FORWARD_VALUECHANGE    pfnForward;    // Item notification forward function

   VARTYPE                    varType;       // Item type
   DWORD                      accessRights;  // Item access right
//   int                        numberOfProperties;
//   POPC_PROPERTY              *properties;
   BOOL                       isActivated;
   void                       *userInfo;     // User info related to group
   char                       name[0];       // Item name
} OPC_ITEM, *POPC_ITEM;

typedef struct _OPC_CONNECTION_LOOKUP_ENTRY {
   HANDLE            hWTConn;
   POPC_CONNECTION   connection;
} OPC_CONNECTION_LOOKUP_ENTRY, *POPC_CONNECTION_LOOKUP_ENTRY;

typedef  struct _OPC_ENVIRONMENT {
   PFN_FORWARD_ERROR             pfnError;
   PFN_FORWARD_DCOMMSG           pfnDcomMsg;
   OPC_CONNECTION_LOOKUP_ENTRY   connectionMap[OPC_MAX_SERVERS];
   void                          *userInfo;
} OPC_ENVIRONMENT, *POPC_ENVIRONMENT;


////////////////////////////////////////
//    Prototypes
////////////////////////////////////////

/**
*  Uninitializes a previously initialized OPC environment
*/
void uninitializeOPC(POPC_ENVIRONMENT *environment);

/**
*  Initializes the OPC environment
*/
POPC_ENVIRONMENT initializeOPC(void); 

/**
*  Disconnect a previously connected OPC server
*/
void disconnectOpcDA(POPC_CONNECTION *connection);

/**
* Release a previously retrieved server list
*/
void releaseOpcServerList(char **list[]);

/**
* Retrieve available OPC servers
*/
int getOpcServerList(char *host, char **list[]);

/**
*  Set the Error notification callback
*/
void setErrorCallback(POPC_ENVIRONMENT environment, PFN_FORWARD_ERROR func);

/**
*  Set the DCOM message notification callback
*/
void setDcomMessageCallback(POPC_ENVIRONMENT environment, PFN_FORWARD_DCOMMSG func);

/**
*  Connect to a specified OPC server
*/
POPC_CONNECTION connectOpcDA(char *host, char *server);

/**
*  Set the disconnect callback
*/
void setDisconnectCallback(POPC_CONNECTION connection, PFN_FORWARD_DISCONNECT func);

/**
*  Set Notification callback for connection related items
*/
void setConnectionNotificationCallback(POPC_CONNECTION connection, PFN_FORWARD_VALUECHANGE func);

/**
* Release a previously retrieved item list
*/
void releaseOpcItemNameList(char **list[]);

/**
* Retrieve available Items from a connected server
*/
int getOpcItemNameList(POPC_CONNECTION connection, char **list[]);


/**
* Removes a group
*/
void removeOpcGroup(POPC_GROUP *group);

/**
* Adds a group to a connection
*/
POPC_GROUP addOpcGroup(POPC_CONNECTION con, char *groupName);

/**
*  Set Notification callback for group related items
*/
void setGroupNotificationCallback(POPC_GROUP group, PFN_FORWARD_VALUECHANGE func); 

/**
* Release a previously retrieved propertylist
*/
void releaseItemProperties(POPC_PROPERTY **properties);

/**
* Retrieve the properties for an item
*/
int getItemProperties(POPC_ITEM item, POPC_PROPERTY **propertyList);

/**
* Removes and releases an item 
*/
void removeOpcItem(POPC_ITEM *item);

/**
* Add an item to a group
*/
POPC_ITEM addOpcItem(POPC_GROUP group, char *tagName);

/**
*  Set Notification callback for a specific item
*/
void setItemNotificationCallback(POPC_ITEM item, PFN_FORWARD_VALUECHANGE func); 

/**
* Read an item synchronously
*/
int readOpcDeviceItem(POPC_ITEM item, VARIANT *v, FILETIME *ft, DWORD *q);

/**
* Write an item synchronously
*/
int writeOpcDeviceItemSync(POPC_ITEM item, VARIANT *v);

/**
* Write an item asynchronously
*/
int writeOpcDeviceItemAsync(POPC_ITEM item, VARIANT *v);

BOOL isQualityGood(DWORD quality);

#ifdef __cplusplus
}
#endif

#endif   // #if !defined(_OPCCLIENT_INCLUDED)
