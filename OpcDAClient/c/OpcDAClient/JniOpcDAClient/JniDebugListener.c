/**
   Implements the native side callback for the DebugListener interface 

   FFI LISA build

   Revsion 1.0 

   Date     february 2013

   Author: H�kan Pettersson, LISA-project

*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <jni.h>
#include "jnisupport.h"

int debugEnabled = 0;

// The debug class,instance and method is used internally by this module
static jclass      debugListenerClass = NULL;
static jobject     debugListenerInstance = NULL;
static jmethodID   debugListener_onDebugMessage_MID ;

#define JAVA_ONDBGMSG_CALLBACK_NAME             "onDebugMessage"
#define JAVA_ONDBGMSG_CALLBACK_SIGNATURE        "(Ljava/lang/String;)V"

#define MIN_DEBUG_MSG_BUF_SIZE   1024  // Minimum space we allocate for message we pass to the java side 
#define DEBUG_MSG_OUTPUT_ERROR   "Error generating a requested debug message"

#define DEFAULT_JNI_VERSION      JNI_VERSION_1_4

void uninitializeDebugListener(JNIEnv *env)
{
   debugEnabled = 0;
   // Debug turned off so now it's save to deregister
   (*env)->DeleteGlobalRef(env, debugListenerClass); debugListenerClass=NULL;
   (*env)->DeleteGlobalRef(env, debugListenerInstance); debugListenerInstance = NULL;
}

/**
*  Prepares and cashes the callback method etc.
*/
int initializeDebugListener(JNIEnv *env, jobject listener, jboolean enabled)
{
   jclass clazz;

   int rc;

   // We only handle one debug listener
   if(debugListenerClass!=NULL) {
      uninitializeDebugListener(env);
   }

   // Prepare the callback method 
   if ((*env)->EnsureLocalCapacity(env, 2) == JNI_OK) {
      while(1) {
         // Item notification
         if((clazz=(*env)->GetObjectClass(env, listener))!=NULL) {
            if((debugListenerClass=(*env)->NewGlobalRef(env, clazz))==NULL) { break; }
            if((debugListenerInstance = (*env)->NewGlobalRef(env, listener))==NULL) { break; }
            if((debugListener_onDebugMessage_MID = (*env)->GetMethodID(env, debugListenerClass, JAVA_ONDBGMSG_CALLBACK_NAME, JAVA_ONDBGMSG_CALLBACK_SIGNATURE))==NULL) { break; }
         }
         // Done 
         break;
      }
   }

   // Always delete local ref.
   (*env)->DeleteLocalRef(env, clazz);

   // Check if there were exceptions. Not much we can do, except clearing it  
   if((*env)->ExceptionCheck(env)) {(*env)->ExceptionClear(env);}

   // Set the return status
   rc = (debugListenerClass!=NULL && debugListenerInstance!=NULL);

   debugEnabled = rc && enabled;

   return rc;
}

/**
*  The native side callback that in turn calls the java one
*/
int onDebugMessage(char *f, ...) 
{
   JNIEnv            *env = NULL;
   JavaVMAttachArgs  args = { DEFAULT_JNI_VERSION, NULL, NULL };

   jstring  message;
   char     *buffer;
   char     *outbuf;

   jint     attachStatus;

   size_t   count;
   int      rc=1; // Assume success

   va_list v;

   // Make shure the JVM is there
   if(cachedJVM==NULL) return 0;

   // Make shure the java callback is there
   if(debugListenerInstance==NULL || debugListener_onDebugMessage_MID==NULL) return 0;

   while(1){
      // Use the cashed JVM to attach this thread to the JVM if not allready attached
      attachStatus = (*cachedJVM)->GetEnv(cachedJVM, (void **) &env, DEFAULT_JNI_VERSION);
      if(attachStatus==JNI_EDETACHED) {
         if((*cachedJVM)->AttachCurrentThread(cachedJVM, (void **)&env, &args)!=0) {
            break;
         }
      }
      else if(attachStatus==JNI_EVERSION) {
         rc=0;
         break;
      }

      // Position argument vector
      va_start(v, f); 

      // Get a big enough printing buffer ...
      count = strlen(f)*10 + MIN_DEBUG_MSG_BUF_SIZE;
      buffer = calloc(1, count+1);
      if(buffer!=NULL) {
         // ... and place the message there
#if defined(_WIN32)
         if(0>_vsnprintf(buffer, count, f, v))
#elif defined(VMS)
         if(0>vsnprintf(buffer, count, f, v))
#else 
         if(0>vsprintf(buffer, f, v))
#endif
         {
            strcpy(buffer, DEBUG_MSG_OUTPUT_ERROR);
         }
         outbuf = buffer;
      }
      else {
         outbuf = DEBUG_MSG_OUTPUT_ERROR;
      }

      // Convert to java string
      if((message=newJavaStringFromBytes(env, (jbyte *)outbuf))==NULL) {
         // If we fail here there is not much more we can do ...
         rc=0;
         free(buffer);
         break;
      }
      else {
         // Callback into JAVA side to report the debug message
         (*env)->CallVoidMethod( env, debugListenerInstance , debugListener_onDebugMessage_MID, message);
      }

      // Clean up 
      (*env)->DeleteLocalRef(env, message);
      va_end(v);
      free(buffer);

      // Detach  current thread from the JVM
      if(attachStatus==JNI_EDETACHED) {
         if((*cachedJVM)->DetachCurrentThread(cachedJVM)!=0){
            break;
         }
      }

      // Done 
      break;
   }

   return rc;
}
