/**
   Implements the native methods for the OpcEnvironment class


   Debug settings in Visual Studio:
   --------------------------------
   Command : java.exe
   Command Arguments: -Djava.library.path=. -cp ../../../Java/OpcDAClient/bin;  "<replace with path to jar files>\log4j-1.2.9.jar";  org.ffi.lisa.opc.test.OpcClientTest

   Dependencies to  the following jars:
   ------------------------------------
   log4j-1.2.9.jar

   Java header file generation for prebuild step:
   ----------------------------------------------
   javah -verbose -d ./generated -jni -classpath  ../../../java/OpcDAClient/bin  org.ffi.lisa.opc.jni.OpcEnvironment   org.ffi.lisa.opc.jni.OpcReference  org.ffi.lisa.opc.jni.OpcVariant org.ffi.lisa.opc.jni.OpcItemListener org.ffi.lisa.opc.jni.OpcDebugListener


   
   FFI LISA build

   Revsion 1.0 

   Date     february 2013

   Author: H�kan Pettersson, LISA-project

*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include <jni.h>

// The opc client library include
#include "opcdaclient.h"

#include "jnisupport.h"
#include "jniopcreference.h"
#include "jnidebuglistener.h"
#include "jniitemlistener.h"
#include "jnidisconnectlistener.h"
#include "jniopcvariant.h"


// By javah generated include 
#include "generated/org_ffi_lisa_opc_jni_OpcEnvironment.h"

// Macro defined for the class (If classname changes and methods stay, only this macro needs modification)
#define NATIVE(func) Java_org_ffi_lisa_opc_jni_OpcEnvironment_##func
#define CONSTANT(con)  org_ffi_lisa_opc_jni_OpcEnvironment_##con

// Any changes in the interface between this native implementation and the java side code
// must be reflected in the version number on both sides.
#define  VERSION  0x0100
#define  MAJOR_VERSION(v)  (((v)&0xff00)>>8)
#define  MINOR_VERSION(v)  ((v)&0xff)

static SYSTEMTIME utcOffset = { 1970, 1, 1, 1, 0, 0, 0, 0 };

FILETIME   utcBaseTime;

JNIEXPORT jint JNICALL NATIVE(getNativeVersion)(JNIEnv *env, jobject self)
{
   return   VERSION;
}

/**
*  Initializes the OpcDAClient library
*
*/
JNIEXPORT void JNICALL NATIVE(initialize)(JNIEnv *env, jobject self)
{
   JavaVM *jvm;

   jstring        result=NULL;
   jint           rc=0;

   POPC_ENVIRONMENT  opcEnv=NULL;

   while(1) {
      if((rc=(*env)->GetJavaVM(env,&jvm))!=0) break;
      // Cache the JVM here...
      cachedJVM = jvm;
      // ... and the support classes here.
      if((rc=OnJvmAttach(env))==JNI_ERR) break;

      // Don't bother to raise an exception if we failed since we just use a listener if it exists
      if(initializeDebugListener(env, self, 1)) {
         DBG_LOG("initialize: DebugListener callback interface successfully initialized"); 
      }
      DBG_LOG("initialize: Initializing native driver. Version = %d.%d", MAJOR_VERSION(VERSION), MINOR_VERSION(VERSION)); 

      // Get the timestamp offset used when setting java side timestamp 
      // (long value will be used for timestamps from native side)
      SystemTimeToFileTime(&utcOffset, &utcBaseTime);

      // Do the  one time initialisation
      if((opcEnv=initializeOPC())== NULL) { rc=JNI_ERR; break;}
      DBG_LOG("initialize: Native OpcDAClient library initialized."); 

      // Save the native environment (OpcReference extension of this class) 
      (*env)->SetLongField(env, self, javaOpcReferenceFieldID, (jlong) opcEnv);

      // Done
      break;
   }


   // Take care of things that occured above
   if(rc!=JNI_ERR) {
      // Everything looks fine
      DBG_LOG("initialize: OpcDAClient initializing complete"); 
   }
   else {
      if(env==NULL) {
         DBG_LOG("initialize: Failure loading local OPC environment"); 
         throwException(env, javaIOExceptionClass, "Failure loading local OPC environment");
      }
      else {
         DBG_LOG("initialize: OPC initialization failure"); 
         throwException(env, javaIOExceptionClass, "OpenMMS initialization failure");
      }
   }
}

/**
   Uninitialize the OpcEnvironment
*/
JNIEXPORT void JNICALL NATIVE(uninitialize)(JNIEnv *env, jobject self)
{
   POPC_ENVIRONMENT opcEnv = (POPC_ENVIRONMENT)(*env)->GetLongField(env, self, javaOpcReferenceFieldID );

   uninitializeOPC(&opcEnv);

   (*env)->SetLongField(env, self, javaOpcReferenceFieldID, (jlong) 0L);

}


/**
*  Assocciates a JAVA side listener that receives debug messages from the native side
*  and indicates if enabled from start or not.
*/
JNIEXPORT void JNICALL NATIVE(setDebugListener)(JNIEnv *env, jobject self, jobject listener, jboolean enabled)
{
   if(initializeDebugListener(env, listener, enabled)) {
      DBG_LOG("setDebugListener: Callback interface successfully initialized"); 
   }
   else {
      DBG_LOG("setDebugListener: Failed initializing."); 
      throwException(env, javaIOExceptionClass, "Failed initializing debugListener");
   }
}

/**
*  Enables debug message forwarding to JAVA side 
*/
JNIEXPORT void JNICALL NATIVE(enableDebugListener)(JNIEnv *env, jobject self)
{
   debugEnabled = 1; 
   DBG_LOG("enableDebugListener: Debugging on");
}

/**
*  Disables debug message forwarding to JAVA side 
*/
JNIEXPORT void JNICALL NATIVE(disableDebugListener)(JNIEnv *env, jobject self)
{
   if(debugEnabled) {
      // No mening to log if log is not enabled
      DBG_LOG("enableDebugListener: Debugging off");
   }
   debugEnabled = 0;
}

/**
* Return status for debuglistener
*/
JNIEXPORT jboolean JNICALL NATIVE(isDebugListenerEnabled)(JNIEnv *env, jobject self)
{
   return debugEnabled!=0;
}


// DCOM callback that forward to the installed debug callback
int onDcomMessage(LPTSTR message)
{
   int rc=0;
   // Log if debug logging is enabled 
   if(debugEnabled) {
      rc = DBG_LOG("DCOM: %s", message);
   }
   return rc;
}

/**
*  Enables DCOM debug message forwarding to JAVA side 
*/
JNIEXPORT void JNICALL NATIVE(enableDcomLogging)(JNIEnv *env, jobject self)
{
   POPC_ENVIRONMENT opcEnv = (POPC_ENVIRONMENT)(*env)->GetLongField(env, self, javaOpcReferenceFieldID );
   DBG_LOG("enableDcomLogging: OpEnv =  %d",opcEnv);
   setDcomMessageCallback(opcEnv, onDcomMessage);

   if(debugEnabled) {
      DBG_LOG("enableDcomLogging: DCOM debug logging turned ON");
   }
}

/**
*  Disables DCOM debug message forwarding to JAVA side 
*/
JNIEXPORT void JNICALL NATIVE(disableDcomLogging)(JNIEnv *env, jobject self)
{
   POPC_ENVIRONMENT opcEnv = (POPC_ENVIRONMENT)(*env)->GetLongField(env, self, javaOpcReferenceFieldID );

   if(debugEnabled) {
      // No mening to log if log is not enabled
      DBG_LOG("disableDcomLogging: DCOM debug logging turned OFF");
   }
   setDcomMessageCallback(opcEnv, NULL);
}

/**
*/
int onErrorMessage(DWORD code, LPTSTR message)
{
   int rc=0;
   // Log if debug logging is enabled 
   if(debugEnabled) {
      rc = DBG_LOG("ERROR: %ld - %s", code, message);
   }
   return  rc;
}

/**
*  Enables Error message forwarding to JAVA side.
*/
JNIEXPORT void JNICALL NATIVE(enableErrorLogging)(JNIEnv *env, jobject self)
{
   POPC_ENVIRONMENT opcEnv = (POPC_ENVIRONMENT)(*env)->GetLongField(env, self, javaOpcReferenceFieldID );
   DBG_LOG("enableErrorLogging: OpEnv =  %d",opcEnv);
   setErrorCallback(opcEnv, onErrorMessage);
   if(debugEnabled) {
      DBG_LOG("enableErrorLogging: Error logging turned ON");
   }

}

/**
*  Disables Error message forwarding to JAVA side 
*/
JNIEXPORT void JNICALL NATIVE(disableErrorLogging)(JNIEnv *env, jobject self)
{
   POPC_ENVIRONMENT opcEnv = (POPC_ENVIRONMENT)(*env)->GetLongField(env, self, javaOpcReferenceFieldID );
   setDcomMessageCallback(opcEnv, NULL);
   if(debugEnabled) {
      DBG_LOG("disableErrorLogging: Error logging turned OFF");
   }
}
/**
* Get the available servers on local host or specified remote host
*
*  "localhost", "local", "127.0.0.1", "." are all valid names  for a local host query.
*
*/
JNIEXPORT jobjectArray JNICALL NATIVE(getServerList)(JNIEnv *env, jobject self, jstring hostname)
{
   char           *host;

   int            itemCount;
   char           **serverList;

   jobjectArray   stringArray;
   jstring        serverString;

   int   i;

   // Get OPC server host
   if((host = getBytesFromJavaString(env, hostname))==NULL) {
      DBG_LOG("getServerList: Error in passed parameter: hostname");
      return (jlong) 0;
   }

   DBG_LOG("getServerList: Retriving available servers on %s", host);

   // Get the server list
   itemCount = getOpcServerList(host, &serverList);
   DBG_LOG("getServerList: %d servers found", itemCount);

   // Create result
   if(itemCount>0) {
      stringArray = (*env)->NewObjectArray(env, itemCount, javaStringClass, NULL);
      if(stringArray==NULL) return NULL;
   }
   for(i=0;i<itemCount;i++) {
      DBG_LOG("getServerList: OPC server (%d) - %s", i, serverList[i]);
      serverString = newJavaStringFromBytes(env, serverList[i]); 
      (*env)->SetObjectArrayElement(env, stringArray, i, serverString);
   }
   releaseOpcServerList(&serverList);

   return stringArray;
}


/**
* Establish a connection with an OPC server
*/
JNIEXPORT jlong JNICALL NATIVE(openConnection)(JNIEnv *env, jobject self, jstring servername, jstring hostname)
{
   POPC_CONNECTION   connection=0;

   char *server;
   char *host;

   // Get OPC server string
   if((server=getBytesFromJavaString(env, servername))==NULL) {
      DBG_LOG("open: Failed retriving server name parameter");
      throwException(env, javaIOExceptionClass, "Failed retriving server name parameter");
      return (jlong) 0;
   }

   // Get OPC server host
   if((host = getBytesFromJavaString(env, hostname))==NULL) {
      DBG_LOG("open: Failed retriving host name parameter");
      free(server);
      throwException(env, javaIOExceptionClass, "Failed retriving host name parameter");
      return (jlong) 0;
   }

   // Open connection to the specified OPC DA server
   if((connection = connectOpcDA(host, server))==NULL) {
      DBG_LOG("open: Failed establishing connection to server %s on host %s",server, host);
      free(server);
      free(host);
      throwException(env, javaIOExceptionClass, "Failed establishing connection to server %s on host %s",server, host);
      return (jlong) 0;
   }


   free(server);
   free(host);

   return (jlong) connection;
}

/**
* Close an OPC server connection 
*/
JNIEXPORT void JNICALL NATIVE(closeConnection)(JNIEnv *env, jobject self, jlong connectionRef)
{
   POPC_CONNECTION connection = (POPC_CONNECTION) connectionRef;
   disconnectOpcDA(&connection);
}


/**
* Get the available items for a given server connection 
*
*  "localhost", "local", "127.0.0.1", "." are all valid names  for a local host query.
*
*/
JNIEXPORT jobjectArray JNICALL NATIVE(getItemList)(JNIEnv *env, jobject self, jlong connectionRef)
{
   POPC_CONNECTION connection = (POPC_CONNECTION) connectionRef;
   
   int            itemCount;
   char           **itemNameList;
   jobjectArray   stringArray;
   jstring        itemString;

   int   i;

   // Get the list of items on the server
   itemCount = getOpcItemNameList(connection, &itemNameList);
   DBG_LOG("getItemList: %d items found", itemCount);

   // Create result
   if(itemCount>0) {
      stringArray = (*env)->NewObjectArray(env, itemCount, javaStringClass, NULL);
      if(stringArray==NULL) return NULL;
   }
   for(i=0;i<itemCount;i++) {
      DBG_LOG("getItemList: OPC item (%d) - %s", i, itemNameList[i]);
      itemString = newJavaStringFromBytes(env, itemNameList[i]); 
      (*env)->SetObjectArrayElement(env, stringArray, i, itemString);
   }
   releaseOpcItemNameList(&itemNameList);

   return stringArray;
}

/**
* Adds a group for holding items items
*/
JNIEXPORT jlong JNICALL NATIVE(addGroup)(JNIEnv *env, jobject self, jlong connectionRef, jstring groupString)
{
   POPC_CONNECTION connection = (POPC_CONNECTION) connectionRef;
   POPC_GROUP group=0;

   char *groupName;

   // Get OPC group name
   if((groupName=getBytesFromJavaString(env, groupString))==NULL) {
      DBG_LOG("addGroup: Failed retriving group name parameter");
      throwException(env, javaIOExceptionClass, "Failed retriving group name parameter");
      return (jlong) 0;
   }
   if((group = addOpcGroup(connection , groupName))==NULL) {
      DBG_LOG("addGroup: Failed adding group %s" , groupName);
      free(groupName);
      throwException(env, javaIOExceptionClass, "Failed adding group");
      return (jlong) 0;
   }

   DBG_LOG("addGroup: %s group was added", groupName);
   free(groupName);
   return (jlong) group;
}

/**
* Remove a group
*/
JNIEXPORT void JNICALL NATIVE(removeGroup)(JNIEnv *env, jobject self, jlong groupRef)
{
   POPC_GROUP group = (POPC_GROUP) groupRef;

   DBG_LOG("removeGroup: %s group removed", group->name);
   removeOpcGroup(&group);
}

/**
* Adds an �tem to a group
*/
JNIEXPORT jlong JNICALL NATIVE(addItem)(JNIEnv *env, jobject self, jlong groupRef, jstring itemString)
{
   POPC_GROUP group = (POPC_GROUP) groupRef;
   POPC_ITEM item=0;

   char *itemName;

   // Get OPC item name
   if((itemName=getBytesFromJavaString(env, itemString))==NULL) {
      DBG_LOG("addItem: Failed retriving item name parameter");
      throwException(env, javaIOExceptionClass, "Failed retriving item name parameter");
      return (jlong) 0;
   }
   if((item = addOpcItem(group, itemName))==NULL) {
      DBG_LOG("addItem: Failed adding item %s" , itemName);
      free(itemName);
      throwException(env, javaIOExceptionClass, "Failed adding item");
      return (jlong) 0;
   }

   DBG_LOG("addItem: %s item was added", itemName);
   free(itemName);
   return (jlong) item;
}

/**
* Remove a group
*/
JNIEXPORT void JNICALL NATIVE(removeItem)(JNIEnv *env, jobject self, jlong itemRef)
{
   POPC_ITEM item = (POPC_ITEM) itemRef;

   DBG_LOG("removeItem: %s item removed", item->name);
   removeOpcItem(&item);
}

/**
* Registers the java callback that will receive all item change events 
* For this JNI implementation only one item listener is used. Any filtering 
* for different items, groups etc. should be implemented on the Java side.
*/
JNIEXPORT void JNICALL NATIVE(setItemListener)(JNIEnv *env, jobject self, jlong connectionRef, jobject listener)
{
   POPC_CONNECTION connection = (POPC_CONNECTION) connectionRef;

   // Make shure the callback is in place
   if(!initializeItemListener(env, listener)) {
      throwException(env, javaIOExceptionClass, "Failed initializing the item listener");
      return;
   }

   // Enable the opc client notification forwarding for all items 
   setConnectionNotificationCallback(connection, onItemNotification);

   return;
}

JNIEXPORT void JNICALL NATIVE(resetItemListener)(JNIEnv *env, jobject self, jlong connectionRef)
{
   POPC_CONNECTION connection = (POPC_CONNECTION) connectionRef;

   // Disable the opc client notification forwarding for all items 
   setConnectionNotificationCallback(connection, NULL);
   uninitializeItemListener(env);

   return;
}


/**
* Registers the java callback that will receive all disconnect notifications.
* 
* For this JNI implementation only one disconnect listener is used. Any filtering 
* for different connections should be implemented on the Java side.
*/
JNIEXPORT void JNICALL NATIVE(setDisconnectListener)(JNIEnv *env, jobject self, jlong connectionRef, jobject listener)
{
   POPC_CONNECTION connection = (POPC_CONNECTION) connectionRef;

   // Make shure the callback is in place
   if(!initializeDisconnectListener(env, listener)) {
      throwException(env, javaIOExceptionClass, "Failed initializing the disconnect listener");
      return;
   }

   // Enable the opc client notification forwarding for all items 
   setDisconnectCallback(connection, onDisconnect);

   return;
}

JNIEXPORT void JNICALL NATIVE(resetDisconnectListener)(JNIEnv *env, jobject self, jlong connectionRef)
{
   POPC_CONNECTION connection = (POPC_CONNECTION) connectionRef;

   // Disable the opc client notification forwarding for all items 
   setDisconnectCallback(connection, NULL);
   uninitializeDisconnectListener(env);

   return;
}

/**
*  Reads an item value. 
*  Note. The retrieved quality and time are not returned to the java side.
*/
JNIEXPORT jobject JNICALL NATIVE(readItem)(JNIEnv *env, jobject self, jlong itemRef)
{
   POPC_ITEM item = (POPC_ITEM) itemRef;

   VARIANT *variant;
   FILETIME filetime;
   DWORD quality;
   int rc;

   jobject result=NULL;

   // Result placed here
   variant = createEmptyVariant();

   if((rc=readOpcDeviceItem(item, variant, &filetime, &quality))!=OPC_SUCCESS) {
      DBG_LOG("readItem: Failed reading item %s", item->name);
      throwException(env, javaIOExceptionClass, "Failed reading item");
      return NULL;
   }

   if(!isQualityGood(quality)) {
      DBG_LOG("readItem: Quality of item %s not good", item->name);
      throwException(env, javaIOExceptionClass, "Bad item quality");
      return NULL;
   }

   DBG_LOG("readItem: Item read %s", item->name);

	if((result=variantToJava(env, variant))!=NULL) {
		// Check if there where exceptions
		if((*env)->ExceptionCheck(env)) {
			(*env)->ExceptionClear(env);
			throwException(env, javaIOExceptionClass, "Failure retrieving value associated with VARIANT VARTYPE:%s", variantTypeToString(variant->vt));
			destroyVariant(&variant);
			return NULL;
		}
		DBG_LOG("readItem: Successfully converted VARIANT VARTYPE:%s to java type:%s", variantTypeToString(variant->vt), javaTypeToString(env, result));
		destroyVariant(&variant);
	}
	else {
		throwException(env, javaIOExceptionClass, "Failure converting native VARIANT VARTYPE:%s to a java type", variantTypeToString(variant->vt) );
		destroyVariant(&variant);
	}

   return result;
}


/**
*  Write an item
*/
JNIEXPORT void JNICALL NATIVE(writeItem)(JNIEnv *env, jobject self, jlong itemRef, jobject value)
{
   POPC_ITEM item = (POPC_ITEM) itemRef;

   VARIANT *variant;
   FILETIME filetime;
   DWORD quality;
   int rc;

   VARIANT *result=NULL;

   // Read item first in order to get a VARIANT template to use
   variant = createEmptyVariant();
   if((rc=readOpcDeviceItem(item, variant, &filetime, &quality))!=OPC_SUCCESS) {
      DBG_LOG("writeItem: Failed examining item %s", item->name);
      throwException(env, javaIOExceptionClass, "Failed examining item ");
      return;
   }
   if(!isQualityGood(quality)) {
      DBG_LOG("writeItem: Quality of item %s not good", item->name);
      throwException(env, javaIOExceptionClass, "Bad item quality");
      return;
   }
	DBG_LOG("writeItem: Successfully created template VARIANT VARTYPE:%s", variantTypeToString(variant->vt));

   // Convert from java to VARIANT
   result=javaToVariant(env, variant, value);

   // Done with template
   destroyVariant(&variant);

	if(result!=NULL) {
		// Check if there where exceptions
		if((*env)->ExceptionCheck(env)) {
			(*env)->ExceptionClear(env);
			throwException(env, javaIOExceptionClass, "Failure converting java value of type %s to VARIANT VARTYPE:%s", javaTypeToString(env, value), variantTypeToString(result->vt));
			destroyVariant(&result);
			return;
		}
		DBG_LOG("writeItem: Successfully converted java type:%s to VARIANT VARTYPE:%s", javaTypeToString(env, value), variantTypeToString(result->vt));

		if(writeOpcDeviceItemSync(item, result)!=OPC_SUCCESS) {
			DBG_LOG("writeItemSync: Failed writing item %s", item->name);
			throwException(env, javaIOExceptionClass, "Failed writing item");
			destroyVariant(&result);
			return;
		}
	}
	else {
		throwException(env, javaIOExceptionClass, "Failure converting java type:%s to native type", javaTypeToString(env, value));
		return;
	}

   DBG_LOG("writeItem: Item written %s", item->name);

   return;
}


JNIEXPORT void JNICALL NATIVE(writeItemAsync)(JNIEnv *env, jobject self, jlong itemRef, jobject value)
{
   POPC_ITEM item = (POPC_ITEM) itemRef;
/*
   VARIANT *variant = (VARIANT *) valueRef;

   if( writeOpcDeviceItemAsync(item, variant )!=OPC_SUCCESS) {
      DBG_LOG("writeItemAsync: Failed writing item %s", item->name);
      throwException(env, javaIOExceptionClass, "Failed writing item");
      return;
   }

   DBG_LOG("writeItemAsync: Item written %s", item->name);
*/
   return;
}


JNIEXPORT jstring JNICALL NATIVE(getItemName)(JNIEnv *env, jobject self, jlong itemRef)
{
   POPC_ITEM item = (POPC_ITEM) itemRef;
   return(newJavaStringFromBytes(env, (jbyte *)(item->name))); 

}

JNIEXPORT jstring JNICALL NATIVE(getConnectionName)(JNIEnv *env, jobject self, jlong connectionRef)
{
   POPC_CONNECTION connection= (POPC_CONNECTION) connectionRef;
   return(newJavaStringFromBytes(env, (jbyte *)(connection->server))); 
}


/*
	Test if the value is of date type
*/
JNIEXPORT jboolean JNICALL NATIVE(isDateValue)(JNIEnv *env, jobject self, jlong valueRef)
{
   VARIANT *v = (VARIANT *) valueRef;
   return (v->vt & VT_DATE) == VT_DATE;
}

/*
	Returns a java standard value object converted from an OPC VARIANT
*/
JNIEXPORT jobject JNICALL NATIVE(getItemValue)(JNIEnv *env, jobject self, jlong valueRef)
{
   VARIANT *v = (VARIANT *) valueRef;
   jobject result=NULL;

   if(v!=NULL) {
	   if((result=variantToJava(env, v))!=NULL) {
		   // Check if there where exceptions
		   if((*env)->ExceptionCheck(env)) {
			  (*env)->ExceptionClear(env);
			  throwException(env, javaIOExceptionClass, "Failure retrieving value associated with VARIANT VARTYPE:%s", variantTypeToString(v->vt));
			  return NULL;
		   }
		   DBG_LOG("getItemValue: Successfully converted VARIANT VARTYPE:%s to java type:%s", variantTypeToString(v->vt), javaTypeToString(env, result));
	   }
	   else {
			throwException(env, javaIOExceptionClass, "Failure converting native VARIANT VARTYPE:%s to a java type", variantTypeToString(v->vt) );
			return NULL;
		}
	}

   return result;
}

// TESTING
JNIEXPORT void JNICALL NATIVE(putItemValue)(JNIEnv *env, jobject self, jobject javaValue)
{
	// Boolean
	if((*env)->IsInstanceOf(env, javaValue, javaBooleanClass))			    { DBG_LOG("putItemValue: Boolean"); return; }
	else if ( (*env)->IsInstanceOf(env, javaValue, javaByteClass))			{ DBG_LOG("putItemValue: Byte"); return; }
	else if ( (*env)->IsInstanceOf(env, javaValue, javaCharacterClass))		{ DBG_LOG("putItemValue: Character"); return; }
	else if ( (*env)->IsInstanceOf(env, javaValue, javaShortClass))			{ DBG_LOG("putItemValue: Short"); return; }
	else if ( (*env)->IsInstanceOf(env, javaValue, javaIntegerClass))		{ DBG_LOG("putItemValue: Integer"); return; }
	else if ( (*env)->IsInstanceOf(env, javaValue, javaLongClass))			{ DBG_LOG("putItemValue: Long"); return; }
	else if ( (*env)->IsInstanceOf(env, javaValue, javaFloatClass))			{ DBG_LOG("putItemValue: Float"); return; }
	else if ( (*env)->IsInstanceOf(env, javaValue, javaDoubleClass))		{ DBG_LOG("putItemValue: Double"); return; }
	else if ( (*env)->IsInstanceOf(env, javaValue, javaBooleanArrayClass))	{ DBG_LOG("putItemValue: Boolean[]"); return; }
	else if ( (*env)->IsInstanceOf(env, javaValue, javaByteArrayClass))		{ DBG_LOG("putItemValue: Byte[]"); return; }
	else if ( (*env)->IsInstanceOf(env, javaValue, javaCharacterArrayClass)){ DBG_LOG("putItemValue: Character[]"); return; }
	else if ( (*env)->IsInstanceOf(env, javaValue, javaShortArrayClass))	{ DBG_LOG("putItemValue: Short[]"); return; }
	else if ( (*env)->IsInstanceOf(env, javaValue, javaIntegerArrayClass))	{ DBG_LOG("putItemValue: Integer[]"); return; }
	else if ( (*env)->IsInstanceOf(env, javaValue, javaLongArrayClass))		{ DBG_LOG("putItemValue: Long"); return; }
	else if ( (*env)->IsInstanceOf(env, javaValue, javaFloatArrayClass))	{ DBG_LOG("putItemValue: Float[]"); return; }
	else if ( (*env)->IsInstanceOf(env, javaValue, javaDoubleArrayClass))	{ DBG_LOG("putItemValue: Double[]"); return; }
	return;
}
