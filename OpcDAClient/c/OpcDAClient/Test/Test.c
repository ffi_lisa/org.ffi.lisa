/*---------------------------------------------------------------------------*\
|  MODULE: Test
>*---------------------------------------------------------------------------*<
|
|  PURPOSE:    Tests the OpcDAClient library functions
|
|  FUNCTIONS:
|
|  REVISION HISTORY   
|     
\*---------------------------------------------------------------------------*/
#include <stdio.h>
#include <conio.h>
#include <time.h>
#include <process.h>
#include <wtypes.h>        // Common types like DWORD, CHAR ...

// Library include
#include "..\JniOpcDAClient\OpcDAClient.h"

char * getTypeName(VARTYPE vt) {

	static char typeName[100];
	VARTYPE type;


	if ((vt  & VT_ARRAY)==VT_ARRAY) {
		strcpy(typeName, "ARRAY of ");
		type = vt^VT_ARRAY;
	}
	else {
		type=vt;
		typeName[0]='\0';
	}

    switch(type) {
	    // Boolean
		case VT_BOOL: strcat(typeName, "VT_BOOL");  break;
        case VT_I1:   strcat(typeName, "VT_I1");  break;
        case VT_UI1:  strcat(typeName, "VT_UI1");  break;   
        case VT_I2:   strcat(typeName, "VT_I2");  break;   
        case VT_I4:   strcat(typeName, "VT_I4");  break;   
        case VT_UI2:  strcat(typeName, "VT_UI2");  break;   
        case VT_UI4:  strcat(typeName, "VT_UI4");  break;   
        case VT_INT:  strcat(typeName, "VT_INT");  break;   
        case VT_UINT: strcat(typeName, "VT_UINT");  break;   
        case VT_I8:   strcat(typeName, "VT_I8");  break;   
        case VT_UI8:  strcat(typeName, "VT_UI8");  break;   
        case VT_R4:   strcat(typeName, "VT_R4");  break;   
        case VT_R8:   strcat(typeName, "VT_R8");  break;   
        case VT_BSTR: strcat(typeName, "VT_BSTR");  break;   
        case VT_EMPTY:    strcat(typeName, "VT_EMPTY");  break;   
        case VT_NULL:     strcat(typeName, "VT_NULL");  break;   
		case VT_DATE:     strcat(typeName, "VT_DATE");  break;    
		case VT_CY:       strcat(typeName, "VT_CY");  break;   
		case VT_DECIMAL:  strcat(typeName, "VT_DECIMAL");  break;   
		case VT_DISPATCH: strcat(typeName, "VT_DISPATCH");  break;   
		case VT_VARIANT:  strcat(typeName, "VT_VARIANT");  break;   
		case VT_RECORD:   strcat(typeName, "VT_RECORD");  break;   
		case VT_UNKNOWN:  strcat(typeName, "VT_UNKNOWN");  break;   
		case VT_ARRAY:    strcat(typeName, "VT_ARRAY");  break;   
		case VT_BYREF:    strcat(typeName, "VT_BYREF");  break;   
		case VT_ERROR:    strcat(typeName, "VT_ERROR");  break;   
		default:          strcat(typeName, "UNKNOWN");  break;   
    }

   return typeName;
}

// Callbacks
int onItemValueChange(POPC_ITEM item, 
                      FILETIME *filetime,
                      DWORD    quality,
                      VARIANT  *value)
{

   return printf("Item notification: Name: %s Type: %s\n", item->name, getTypeName(value->vt));

}

int onError(DWORD code, LPTSTR message)
{
   return printf("Error: %s\n", message);
}

int onDcomMessage(LPTSTR message)
{
   return printf("DCOM: %s\n", message);
}

int onDisconnect(POPC_CONNECTION connection) 
{
   return printf("Connection:  %s@%s disconnected\n", connection->server, connection->host);
}


int main(int argc, char *argv[])
{
   POPC_ENVIRONMENT  env=NULL;
   POPC_CONNECTION   connection=NULL;
   POPC_GROUP        group=NULL;
   POPC_PROPERTY     proper=NULL;
   POPC_ITEM         item=NULL;
   POPC_ITEM         *itemList=NULL;
   POPC_PROPERTY     *propertyList;

   char **serverList;
   char **itemNameList;

   int   i,j;

   while(1) {

      if(argc<2) break;

      // Must do a one time initialisation
      if((env=initializeOPC())== NULL) break;

      // Register callbacks
      setErrorCallback(env, onError);
      setDcomMessageCallback(env, onDcomMessage);

      // Get available OPC servers
      getOpcServerList(argv[1], &serverList);
      printf("Available servers\n");
		for(i=0;serverList[i]!=NULL;i++) {
			printf("  (%d): %s\n", i, serverList[i]);
		}
      releaseOpcServerList(&serverList);

      if(argc<3) break;

      // Establish a new connection with the server
      if( (connection=connectOpcDA(argv[1], argv[2]))==NULL) break;

      // Register callbacks
      setDisconnectCallback(connection, onDisconnect);
      setConnectionNotificationCallback(connection, onItemValueChange);

      // Get the list of items on the server
      getOpcItemNameList(connection, &itemNameList);
      printf("Available items\n");
		for(i=0;itemNameList[i]!=NULL;i++) {
			printf("  (%d): %s\n", i, itemNameList[i]);
		}

      if(i>0) {
         if( (itemList = (POPC_ITEM *)calloc(i+1, sizeof(POPC_ITEM)))==NULL) break;
      }

      // Add a group for holding items we will subscribe to
      if( (group=addOpcGroup(connection, "Test"))==NULL) break;

      // Register callbacks
      setGroupNotificationCallback(group, onItemValueChange);

      // Add items to our group
		for(i=0;itemNameList[i]!=NULL;i++) {
         itemList[i] = addOpcItem(group, itemNameList[i]);
         getItemProperties(itemList[i], &propertyList);
         for(j=0;propertyList[j]!=NULL;j++) {
            printf("  (%d): %s [%ld : %s]\n", i, itemList[i]->name , propertyList[j]->id, propertyList[j]->description);
         }
         releaseItemProperties(&propertyList);
		}
      // Marker for end of list
      itemList[i]=NULL;
      releaseOpcItemNameList(&itemNameList);

      {
         VARIANT variant;
         FILETIME filetime;
         DWORD quality;
         int rc;


         VariantInit(&variant);

         // Read first item we have in our list
         rc = readOpcDeviceItem(itemList[0], &variant, &filetime, &quality);
         printf("%s: read OPC item returned %d\n", itemList[0]->name, rc);

         // Write it back synchronously
         rc = writeOpcDeviceItemSync(itemList[0], &variant);
         printf("%s: write OPC item returned %d\n", itemList[0]->name, rc);

         // Write it back asynchronously
         rc = writeOpcDeviceItemAsync(itemList[0], &variant);
         printf("%s: write OPC item returned %d\n", itemList[0]->name, rc);
      }

      // Wait for any key to be pressed
      _getch();

      // Free things
      for(i=0;itemList[i]!=NULL;i++) {
         removeOpcItem(&itemList[i]);
      }
      free(itemList);
      removeOpcGroup(&group);
      disconnectOpcDA(&connection);
      uninitializeOPC(&env);

      // Done
      break;
	}


}




/*
int   stop=0;

void waitForKey(void *p)
{
   int *stop=(int *) p;

    _getch();
   *stop=1;
}



// Start the key press thread
_beginthread(waitForKey, 0, &stop);
while(!stop) Sleep(1);

*/
