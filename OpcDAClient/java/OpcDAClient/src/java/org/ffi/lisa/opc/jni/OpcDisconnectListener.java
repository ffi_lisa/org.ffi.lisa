package org.ffi.lisa.opc.jni;

public interface OpcDisconnectListener {
	/**
	* Call back for OPC native side disconnect notifications
	*/
	public void onDisconnect(long connection_ref);
	
}
