package org.ffi.lisa.opc.jni;

import java.io.IOException;
import org.apache.log4j.Logger;

/**
 * This class contains an almost "one to one" mapping for the OPC-client functionalities in 
 * the native OpcDAClient C-library.
 * <P> 
 * The design goal was to make it simple on the native side and move type complexity etc to 
 * the Java side. Almost no user defined classes needs to be known by the native side.
 * Mostly standard java built in classes like String, Integer IOException etc. are used.
 * (JniSupport.h declares the native side java references)
 * <P>
 * The implementation is made as a singleton.
 * <P>
 * <P>
 * Written for the FFI LISA project 
 * <P>
 * Author: H�kan Pettersson,  February 2013 
 * 
 */
public class OpcEnvironment extends OpcReference implements OpcDebugListener { 
	
   // Any changes in the interface between the native implementation and the this code
   // should be reflected in the version number.
   private static final int REQUIRED_NATIVE_VERSION = 0x0100;

   private static final String ERROR_CODE_FAILED_NATIVE_CALL = "failed.native.call";
   private static final String ERROR_CODE_FAILED_VERSION_CHECK = "failed.native.version";
   private static final String ERROR_CODE_FAILED_LIBRARY_LOAD = "failed.native.library";
   
   private static final Logger logger = Logger.getLogger(OpcEnvironment.class);
   
   
   private static OpcEnvironment ref;

   private static String nativeLibraryName = "OpcDAClient";
   private static Logger nativeLogger;
   private int nativeVersion;
   
	
   /**
    * Creates the environment enabling native OPC calls
    */
   private OpcEnvironment() throws OpcException {
      nativeLogger = Logger.getLogger(OpcEnvironment.class.getPackage().getName() + "." + nativeLibraryName);
      
      if (logger.isDebugEnabled()) {
         logger.debug("Loading OpcDAClient native implementation");
      }
      
      // Load native library...
      try {
    	  System.loadLibrary(nativeLibraryName);
      }
      catch (UnsatisfiedLinkError e) {
		   throw new OpcException(	"Unable to find nativelibrary module: " + nativeLibraryName ,  
     								OpcEnvironment.class,
			          				ERROR_CODE_FAILED_LIBRARY_LOAD);
      }
      catch (NullPointerException e) {
		   throw new OpcException(	"Library module name not supplied", 
			          				OpcEnvironment.class,
			          				ERROR_CODE_FAILED_LIBRARY_LOAD);
      }
       
      nativeVersion = this.getNativeVersion();
     
      if (logger.isDebugEnabled()) {
         logger.debug("Native library loaded: " + nativeLibraryName + "  version: " + Integer.toHexString(nativeVersion));
      }
      
	  // Check version of native library
	  if(nativeVersion != REQUIRED_NATIVE_VERSION) {
		   throw new OpcException(	"Unmatched native library version, found: "+ Integer.toHexString(nativeVersion) +" required: " + Integer.toHexString(REQUIRED_NATIVE_VERSION) , 
     								OpcEnvironment.class,
     								ERROR_CODE_FAILED_VERSION_CHECK);
	  }
	  
      // Make sure the OpcReference is initialized once
      try {
    	  initReference();
      } catch (IOException e) {
  		   throw new OpcException(	"Failed initializing native reference access", 
  									OpcEnvironment.class,
  									ERROR_CODE_FAILED_NATIVE_CALL);
      }

      // Do native initialization
      try {
          if (logger.isDebugEnabled()) {
              logger.debug("Initializing native library");
          }
          this.initialize();
      } 
      catch (IOException e) {
			   throw new OpcException(	"Failed initializing native library", 
										OpcEnvironment.class,
										ERROR_CODE_FAILED_NATIVE_CALL);
      }
      
      if (logger.isDebugEnabled()) {
         logger.debug("Native library initialized");
      }
      
   }
   
	/**
	 * Gets the OcpEnvironment instance (created if not existing)   
	 */
   
   public static synchronized OpcEnvironment getOpcEnvironment(String libraryName) throws OpcException
   {
      if (ref == null) {
         nativeLibraryName = libraryName;          
         ref = new OpcEnvironment();
      }
      return ref;
   }

   
	/**
	 * Gets the OcpEnvironment instance (created if not existing)   
	 */
   public static synchronized OpcEnvironment getOpcEnvironment() throws OpcException
   {
      if (ref == null) {
          ref = new OpcEnvironment();
      }
      return ref;
   }

   /**
    * Guarantees that we are not cloned (since we are a singleton)
    */
   public Object clone() throws CloneNotSupportedException
   {
      throw new CloneNotSupportedException(); 
   }
   
   /**
    * A default implementation of the OpcDebugListener interface
    */
   public void onDebugMessage(String message) {
      if (nativeLogger.isDebugEnabled()) {
         nativeLogger.debug(message);
      }
      else {
         // Turn off logger if native still issues call backs
         // while the logger is turned off
         this.disableDebugListener();
      }
   }
   
   /******************************************
    * Native side methods
    ******************************************    
    */
   /**
    * Get the native library version
    */
   public native int getNativeVersion();
   
   /**
    * Initializes the native side of the OPC environment
    */
   public native void initialize() throws IOException;
   
   /**
    * Reset the native side of the OPC environment
    */
   public native void uninitialize();
   
   /**
    * Sets the debug message listener for the established environment. 
    * Debug messages will be reported on the call back of the listener
    * This will enable debug messages to be forwarded to the java side.
    * 
    * Note.
    * 
    * The native side debug listener is set up at native initialization of 
    * this class (see constructor above) and will as a default use the call back
    * in this class (this.onDebugMessage)
    *
    * @param listener   The instantiated interface
    * @param enable		Indicates id debug listener should turned on or off
    */
   public native void setDebugListener(OpcDebugListener listener, boolean enable) throws IOException;
   
   /**
    * Returns the logger used for native side debugging.
    *  
    * @return
    */
   public Logger getDebugListener() {
      return nativeLogger; 
   }
   
   /**
    * Enables debug messages from native side 
    */
   public native void enableDebugListener();
   
   /**
    * Disables debug messages from native side 
    */
   public native void disableDebugListener();
   
   /**
    * Checks if native side debug is turned on or of 
    */
   public native boolean isDebugListenerEnabled();
   
   /**
    * Enables DCOM log messages from native side
    *  
    * Currently the implementation forwards to the DebugListener   
    */
   public native void enableDcomLogging();
   
   /**
    * Disables DCOM log messages from native side 
    */
   public native void disableDcomLogging();
   
   /**
    * Enables Error messages from native side.
    * 
    * Currently the implementation forwards to the DebugListener   
    */
   public native void enableErrorLogging();
   
   /**
    * Disables Error messages 
    */
   public native void disableErrorLogging();
   
  
   /**
    * Retrieves a list of available servers on local or remote host
    * 
    * @param host
    * 		
    * @return An array of strings containing available OPC servers 
    */
   public native String[] getServerList(String host);
   
   
   /**
    * Sets the OPC native side disconnect notification listener 
    *  
    * @param listener
    */
   public native void setDisconnectListener(long connectionRef, OpcDisconnectListener listener) throws IOException;
   
   /**
    Clears the OPC native side disconnect listener 
    *  
    * @param listener
    */
   public native void resetDisconnectListener(long connectionRef); 

   /**
    * Open a connection to a specified OPC server
    *  
    * @param server  Name of OPC server to open
    * @param host    Name of host running the OPC server (... if DCOM is used.)
    *                (A '.' means server at local host)
    *                 
    * @return        Native reference to the open server connection
    *                A return value of -1 means failure
    */
   public native long openConnection(String server, String host) throws IOException;
   
   /**
    * Close a connection to an open OPC server
    *  
    * @param connectionRef Native reference to the open server connection
    */
   public native void closeConnection(long connectionRef);
   
   
   /**
    * Retrieve an connection name
    * 
    * @param connectionRef	Native side connection reference 
    */
   public native String getConnectionName(long connectionRef);
   
   /**
    * Get a list of existing items in an Opc server
    *    
    * @param 	connectionRef	Native reference to the open server connection
    * @return	Array of item name strings
    */
   public native String[] getItemList(long connectionRef);
   
   
   
   /**
    * Sets an item listener to a connected OPC server. OPC item notifications 
    * will be reported to the call back of the supplied listener
    * 
    * Currently only one call back for the whole environment is possible
    * even if it is setup/cleared per connection. Any filtering etc. needs
    * to be handled locally by the java implementation   
    *  
    *  
    * @param serverRef     Native reference to the open server connection
    * @param listener
    */
   public native void setItemListener(long connectionRef , OpcItemListener listener) throws IOException;
   
   /**
    * Clear  any setup listener 
    * @param connectionRef
    * @param listener
    */
   public native void resetItemListener(long connectionRef) ;
   
   
   /**
    * Add a group to a connected OPC server
    * 
    * @param connectionRef  Native reference to OPC server connection
    * @param name       	 Name of server group
    * @return           	 Native reference to the group
    */
   public native long addGroup(long connectionRef , String name) throws IOException;
   
   /**
    * Remove a group from an Opc Server
    *  
    * @param connectionRef Native reference to server group
    */
   public native void removeGroup(long groupRef );
   
   
   
   /**
    * Add an Item to a group
    * 
    * @param groupRef   	Native reference to item group
    * @param name       	Name of the item
    * 
    * @return           	Native reference to the item
    */
   public native long addItem(long groupRef, String name) throws IOException;;

   
   /**
    * Remove an item from a group
    * 
    * @param itemRef   Native reference to item group   
    */
   public native void removeItem(long itemRef);

   
   /**
    * Retrieve an item name
    * 
    * @param itemRef	Native side item reference 
    */
   public native String getItemName(long itemRef);
   

   /**
    * Reads an Opc item (synchronously)
    * 
    * @param itemRef       native reference to item

    * @return              A java value object
    */
   public native Object readItem(long itemRef) throws IOException;
   
   
   /**
    * Write an OPC item (synchronously)
    * <br><br>
    * The supplied java object must match the corresponding type on native side. see getItemValue    
    *  
    * @param itemRef    native reference to item 
    * @param value      a java value object
    * 
   */
   public native void writeItem(long itemRef, Object value) throws IOException;;
   

   /**
    * Get the value object from native side value reference
    * <br><br>
    * Default mapping for an OPC VARIANT type to a plain java built in type:
    * <br><br>
    *	<pre><table>
    *	<tr><td>VARIANT</td>			<td>   </td><td>Java</td></tr>
    *	<tr><td>-------</td>			<td>   </td><td>----</td></tr>
    *	<tr><td>VT_BOOL</td>			<td>   </td><td>Boolean</td></tr>
    *	<tr><td>VT_I1</td>  			<td>   </td><td>Byte</td></tr>
    *	<tr><td>VT_UI1</td> 			<td>   </td><td>Byte</td></tr>   
    *	<tr><td>VT_I2</td>  			<td>   </td><td>Short</td></tr>   
    *	<tr><td>VT_UI2</td> 			<td>   </td><td>Short</td></tr>
    *	<tr><td>VT_I4</td>  			<td>   </td><td>Integer</td></tr>   
    *	<tr><td>VT_UI4</td> 			<td>   </td><td>Integer</td></tr>      
    *	<tr><td>VT_INT</td> 			<td>   </td><td>Integer</td></tr>   
    *	<tr><td>VT_UINT</td>			<td>   </td><td>Integer</td></tr>   
    *	<tr><td>VT_I8</td>  			<td>   </td><td>Long</td></tr>   
    *	<tr><td>VT_UI8</td> 			<td>   </td><td>Long</td></tr>   
    *	<tr><td>VT_R4</td>  			<td>   </td><td>Float</td></tr>
    *	<tr><td>VT_R8</td>  			<td>   </td><td>Double</td></tr>
    *	<tr><td>VT_BSTR</td>			<td>   </td><td>String</td></tr>
    *	<tr><td>VT_DATE</td>			<td>   </td><td>Date</td></tr>
    *	<tr><td>VT_ARRAY|VT_BOOL</td>	<td>   </td><td>Boolean[]</td></tr>
    *	<tr><td>VT_ARRAY|VT_I1</td>		<td>   </td><td>Byte[]</td></tr>
    *	<tr><td>VT_ARRAY|VT_UI1</td>	<td>   </td><td>Byte[]</td></tr>   
    *	<tr><td>VT_ARRAY|VT_I2</td>		<td>   </td><td>Short[]</td></tr>   
    *	<tr><td>VT_ARRAY|VT_UI2</td>	<td>   </td><td>Short[]</td></tr>
    *	<tr><td>VT_ARRAY|VT_I4</td>		<td>   </td><td>Integer[]</td></tr>   
    *	<tr><td>VT_ARRAY|VT_UI4</td>	<td>   </td><td>Integer[]</td></tr>      
    *	<tr><td>VT_ARRAY|VT_INT</td>	<td>   </td><td>Integer[]</td></tr>   
    *	<tr><td>VT_ARRAY|VT_UINT</td>	<td>   </td><td>Integer[]</td></tr>   
    *	<tr><td>VT_ARRAY|VT_I8</td>		<td>   </td><td>Long[]</td></tr>   
    *	<tr><td>VT_ARRAY|VT_UI8</td>	<td>   </td><td>Long[]</td></tr>   
    *	<tr><td>VT_ARRAY|VT_R4</td>		<td>   </td><td>Float[]</td></tr>
    *	<tr><td>VT_ARRAY|VT_R8</td>		<td>   </td><td>Double[]</td></tr>
    *
    *	</table></pre>
    * 
    * @param valueRef reference to native side value
    * @return
    */
    public native Object getItemValue(long valueRef);
    

    /**
     *  Tests if a VARIANT is of VT_DATE type 
     *   
     * @param valueRef
     * @return
     */
    public native boolean isDateValue(long valueRef);
    
    
    public native void putItemValue(Object value);

}
