package org.ffi.lisa.opc.jni;

import java.io.IOException;

/**
 * An abstraction for a native pointer data type. JNI classes that needs to maintain 
 * a native side reference can extend this class 
 *	   
 */
public class OpcReference {
	
    /**
     * Pointer value of the real C pointer. Uses long to be 64-bit safe. 
     */
    protected long reference;
	
	
    /**
     * Compares this <code>OpcReference </code> to the specified object.
     *
     * @param other a <code>OpcReference </code>
     * @return      true if the class of this <code>OpcReference </code> object 
     *              and the class of <code>other</code> are exactly equal, 
     *              and the references being pointed to by these objects are
     *              also equal. Returns false otherwise.
     */
    public boolean equals(Object other) {
   	 if (other == null)
   		 return false;
   	 if (other == this)
   		 return true;
   	 if (OpcReference.class != other.getClass())
   		 return false;
   	 return reference == ((OpcReference)other).reference;
    }

    /**
     * Returns a hash code for the OpcReference represented by this object.
     *
     * @return a hash code value for the represented reference
     */
    public int hashCode() {
        return (int)((reference >>> 32) + (reference & 0xFFFFFFFF));
    }

    /**
     *  Initialize field and method IDs for native methods of this class.
     *  Only needs to be initialized once 
     */
    protected native void initReference() throws IOException;
}
