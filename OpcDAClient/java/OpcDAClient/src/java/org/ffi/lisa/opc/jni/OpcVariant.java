package org.ffi.lisa.opc.jni;

import java.sql.*;

public class OpcVariant extends OpcReference {
	
   // By native side mapped VARIANT type constants
   public static final int VT_UI1          = 1;    // VT_UI1         // 1-byte unsigned int.
   public static final int VT_UI2          = 2;    // VT_UI2         // 2-byte unsigned int. 
   public static final int VT_UI4          = 3;    // VT_UI4         // 4-byte unsigned int.
   public static final int VT_UI8   	   = 4;	   // VT_UI8         // 8-byte unsigned int.
   public static final int VT_UINT         = 5;    // VT_UINT        // Unsigned machine int.
   
   public static final int VT_I1           = 6;    // VT_I1          // 1--byte signed int.
   public static final int VT_I2           = 7;    // VT_I2          // 2-byte signed int.
   public static final int VT_I4           = 8;    // VT_I4          // 4-byte-signed int.
   public static final int VT_I8   		   = 9;	   // VT_I8          // 8-byte-signed int. 
   public static final int VT_INT          = 10;   // VT_INT         // Signed machine int.
   
   public static final int VT_R4           = 11;   // VT_R4          // 4-byte real. 
   public static final int VT_R8           = 12;   // VT_R8          // 8-byte real.
   
   public static final int VT_BOOL         = 13;   // VT_BOOL        // Boolean; True=-1, False=0.
   public static final int VT_DATE         = 14;   // VT_DATE        // Date.
   public static final int VT_BSTR         = 15;   // VT_BSTR        // Automation string.
   public static final int VT_CY           = 16;   // VT_CY          // Currency.
   public static final int VT_DECIMAL      = 17;   // VT_DECIMAL     // 16 byte fixed point.
   public static final int VT_ARRAY        = 200;  // VT_ARRAY       // SAFEARRAY*.

   
   public static final int VT_EMPTY        = 0;     // VT_EMPTY       // Not specified.
   public static final int VT_NULL         = 1000;  // VT_NULL        // SQL-style Null.
   public static final int VT_DISPATCH     = 101;   // VT_DISPATCH   // IDispatch.Far*
   public static final int VT_VARIANT      = 102;   // VT_VARIANT    // VARIANT FAR*.
   public static final int VT_RECORD       = 103;   // VT_RECORD     // User defined type
   public static final int VT_UNKNOWN      = 104;   // VT_UNKNOWN    // IUnknown FAR*.

   public static final int VT_BYREF        = 300;   // VT_BYREF       // By reference

   public static final int VT_ERROR        = -1;    // VT_ERROR       // Scodes.
   
   
//   By native side mapped OPC quality flags
//   public static final int  OPC_QUALITY_MASK            =0xC0;
//   public static final int  OPC_STATUS_MASK             =0xFC;
//   public static final int  OPC_LIMIT_MASK              =0x03;
//   public static final int  OPC_QUALITY_BAD             =0x00;
//   public static final int  OPC_QUALITY_UNCERTAIN       =0x40;
//   public static final int  OPC_QUALITY_GOOD            =0xC0;
//   public static final int  OPC_QUALITY_CONFIG_ERROR    =0x04;
//   public static final int  OPC_QUALITY_NOT_CONNECTED   =0x08;
//   public static final int  OPC_QUALITY_DEVICE_FAILURE  =0x0c;
//   public static final int  OPC_QUALITY_SENSOR_FAILURE  =0x10;
//   public static final int  OPC_QUALITY_LAST_KNOWN      =0x14;
//   public static final int  OPC_QUALITY_COMM_FAILURE    =0x18;
//   public static final int  OPC_QUALITY_OUT_OF_SERVICE  =0x1C;
//   public static final int  OPC_QUALITY_LAST_USABLE     =0x44;
//   public static final int  OPC_QUALITY_SENSOR_CAL      =0x50;
//   public static final int  OPC_QUALITY_EGU_EXCEEDED    =0x54;
//   public static final int  OPC_QUALITY_SUB_NORMAL      =0x58;
//   public static final int  OPC_QUALITY_LOCAL_OVERRIDE  =0xD8;
//   public static final int  OPC_LIMIT_OK                =0x00;
//   public static final int  OPC_LIMIT_LOW               =0x01;
//   public static final int  OPC_LIMIT_HIGH              =0x02;
//   public static final int  OPC_LIMIT_CONST             =0x03;
   
   
//   private OpcArray arrayReference;
   
   /**
    * Constructs a new empty OpcVariant reference
    */
   public OpcVariant()
   {
   	create();
   }

   /**
    * Constructs a new OpcVariant using a byte initializer
    * @param value	value to set
    */
   public OpcVariant(byte value)
	{
	   this();
	   setNativeByte(value);
	}
   
   /**
    * Constructs a new OpcVariant using a short initializer
    * @param value	value to set
    */
   public OpcVariant(short value)
	{
   		this();
		setNativeShort(value);
	}
   
	
   /**
    * Constructs a new OpcVariant using a integer initializer
    * @param value	value to set
    */
	public OpcVariant(int value)
	{
		this();
		setNativeInt(value);
	}
	
   /**
    * Constructs a new OpcVariant using a long initializer
    * @param value	value to set
    */
	public OpcVariant(long value)
	{
		this();
		setNativeLong(value);
	}
	
   /**
    * Constructs a new OpcVariant using a float initializer
    * @param value	value to set
    */
	public OpcVariant(float value)
	{
		this();
		setNativeFloat(value);
	}
	
   /**
    * Constructs a new OpcVariant using a double initializer
    * @param value	value to set
    */
	public OpcVariant(double value)
	{
		this();
		setNativeDouble(value);
	}
	
   /**
    * Constructs a new OpcVariant using a timestamp initializer
    * @param value	value to set
    */
	public OpcVariant(Timestamp value)
	{
		this();
		setNativeDate(value.getTime());
	}
	
	
   /**
    * Constructs a new OpcVariant using a String initializer
    * @param value	value to set
    */
	public OpcVariant(String value)
	{
		this();
		setNativeString(value);
	}
	
   /**
    * Constructs a new OpcVariant using an OpcArray initializer
    * @param value	value to set
    */
//	public OpcVariant(OpcArray array)
//	{
//		this();
//		setNativeArray(value);
//	}
	
	/**
	 * Creates an empty native VARIANT type. This class will hold a reference to 
	 * the native side type.
	 * All operations are done on native side  
	 *
	 */
   private native void create();
   
	/**
	 * Release the native VARIANT type resources
	 *
	 */
   public native void release();
	
   public native int getType();
   public native int setType(int type);
   
   public native boolean getNativeBoolean();
   public native void setNativeBoolean(boolean value);
   
   public native byte getNativeByte();
   public native void setNativeByte(byte value);
   
   public native short getNativeShort();
   public native void setNativeShort(short value);
   
   public native int getNativeInt();
   public native void setNativeInt(int value);
   
   public native long getNativeLong();
   public native void setNativeLong(long value);
   
   public native float getNativeFloat();
   public native void setNativeFloat(float value);
   
   public native double getNativeDouble();
   public native void setNativeDouble(double value);
   
   public native String getNativeString();
   public native void setNativeString(String value);
   
   public native long getNativeDate();
   public native void setNativeDate(long value);
   
//   public native OpcArray getNativeArray();
//   public native void setNativeArray(OpcArray value);
      
   public String toString() {
   	int vt;
   	String rc=null;
   	
   	vt = getType();
   	switch(vt){
   		case VT_UI1:
   		case VT_UI2: 
   		case VT_UI4:
   		case VT_UI8:
   		case VT_UINT:
   		case VT_I1:
   		case VT_I2:
   		case VT_I4:
   		case VT_I8: 
   		case VT_INT:
   			rc = new Long(this.getNativeLong()).toString();
   			break;
   		case VT_R4: 
   		case VT_R8:
   			rc = new Double(this.getNativeDouble()).toString();
   			break;
   		case VT_BOOL:
   			rc = new Boolean(this.getNativeBoolean()).toString(); 
   			break;
   		case VT_DATE:
   			rc = new Timestamp(this.getNativeDate()).toString();
   			break;
   		case VT_BSTR:
   			rc = this.getNativeString();
   			break;
   		case VT_ARRAY:
   			rc = new String("<array>");
   			break;
   		default:
   			rc=new String("<Unsupported type>");
   			break;
   	}
   	return rc;
   }
}
