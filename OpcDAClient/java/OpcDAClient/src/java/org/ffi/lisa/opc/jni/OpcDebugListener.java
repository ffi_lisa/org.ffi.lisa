package org.ffi.lisa.opc.jni;

public interface OpcDebugListener {
	
	/**
	* Call back for OPC debug notifications
	*/
	public void onDebugMessage(String message);

}
