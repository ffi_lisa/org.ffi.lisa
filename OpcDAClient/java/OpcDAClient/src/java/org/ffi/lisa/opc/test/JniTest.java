package org.ffi.lisa.opc.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.parsers.FactoryConfigurationError;

import org.apache.log4j.xml.DOMConfigurator;
import org.ffi.lisa.opc.jni.OpcDisconnectListener;
import org.ffi.lisa.opc.jni.OpcEnvironment;
import org.ffi.lisa.opc.jni.OpcException;
import org.ffi.lisa.opc.jni.OpcItemListener;

public class JniTest implements OpcItemListener, OpcDisconnectListener 
{
	private OpcEnvironment opcEnv;
	
	private SimpleDateFormat timestampFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");	
	
	/**
	 * Tests the native OpcEnvironment functions
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		JniTest nt = new JniTest();
		nt.doRun();
	}

	private String getTypeString(Object value) {
	   	if(value instanceof Boolean) 	return "Boolean";
	   	if(value instanceof Byte) 		return "Byte";
	   	if(value instanceof Short) 		return "Short";
	   	if(value instanceof Integer) 	return "Integer";
	   	if(value instanceof Long) 		return "Long";
	   	if(value instanceof Float) 		return "Float";
	   	if(value instanceof Double) 	return "Double";
	   	if(value instanceof String) 	return "String";
	   	if(value instanceof Date)  		return "Date";
	   	if(value instanceof boolean[]) 	return "Boolean["+ ((boolean[])value).length+"]";
	   	if(value instanceof byte[]) 	return "Byte["+ ((byte[])value).length+"]";
	   	if(value instanceof short[]) 	return "Short["+ ((short[])value).length+"]"; 
	   	if(value instanceof int[]) 		return "Integer["+ ((int[])value).length+"]";
	   	if(value instanceof long[]) 	return "Long["+ ((long[])value).length+"]";
	   	if(value instanceof float[]) 	return "Float["+ ((float[])value).length+"]";
	   	if(value instanceof double[]) 	return "Double["+ ((double[])value).length+"]";
	   	return "Unknown/Unsupported";
	}
	

	// The Item Notification call back
	public void onItemNotification(long itemRef, Date timestamp, long quality, long valueRef) {
		
	   	Object value = opcEnv.getItemValue(valueRef);
	   	
	   	System.out.println(new StringBuffer(timestampFormat.format(timestamp)).append(" : Item notification received from ").
						append(opcEnv.getItemName(itemRef)).
						append(" . Type=").
						append(getTypeString(value)).
						append(" , Value=").
						append(value));
	}

	
	// The disconnect call back
	public void onDisconnect(long connectionRef) {
	   	System.out.println(new StringBuffer("Disconnected from server: ").append(opcEnv.getConnectionName(connectionRef)));
	}

	private void doRun() {
		
		String [] serverNameList;
		String [] itemNameList;
		
		long connectionRef=0;
		long groupRef=0;
		long itemRef=0;
		long valueRef=0;
		
		// Load log4j configuration
		try {
			DOMConfigurator.configureAndWatch("./src/conf/log4j.xml"); 
		} catch (FactoryConfigurationError e) {
			System.out.println("Could not read the log4j XML configuration file " + e.toString());
			return;
		}
		
		// Get the OpcEnvironment 
		try {
			opcEnv=OpcEnvironment.getOpcEnvironment("../../c/OpcDAClient/release/JniOpcDAClient");
		} catch (OpcException e1) {
			e1.printStackTrace();
			return;
		}
		// Enable some extra logging
		
		// opcEnv.enableDcomLogging();		
		opcEnv.enableErrorLogging();
		
		
		// Get available server
		serverNameList = opcEnv.getServerList("localhost");   		
		for(int i=0;i<serverNameList.length;i++) {
			System.out.println("Available server: " + serverNameList[i]);
		}
		
		// Establish connection
		try {
			connectionRef = opcEnv.openConnection("Beijer Electronics OPC Server", "localhost");
			System.out.println("Connection established");
			opcEnv.setDisconnectListener(connectionRef, this);			
			System.out.println("Disconnect listener installed");
			
		} catch (IOException e) {
			System.out.println("Failed open connection");
			e.printStackTrace();
			return;
		}
		
		// Get available items
		itemNameList = opcEnv.getItemList(connectionRef);   		
		for(int i=0;i<itemNameList.length;i++) {
			System.out.println("Available items: " + itemNameList[i]);
		}
		
		// Setup the item notification listener
		try {
			opcEnv.setItemListener(connectionRef, this);
			System.out.println("Item listener configured");
		} catch (IOException e) {
			System.out.println("Failed configuring the item listener ");
		}
		
		// Create an item group
		try {
			groupRef = opcEnv.addGroup(connectionRef, "TestGroup");
			System.out.println("Group added");
		} catch (IOException e) {
			System.out.println("Failed adding group");
		}
		
		// Add known items to the group
		long testArrRef;
		long testShortRef;
		long testStrRef;
		
		try {
			testShortRef = opcEnv.addItem(groupRef, "QEmulator.Simple.TestShort");
			testArrRef = opcEnv.addItem(groupRef, "QEmulator.Simple.TestArr");
			testStrRef = opcEnv.addItem(groupRef, "QEmulator.Simple.TestStr");
			
			System.out.println("Items added");
			
			// Read & write items. Note. reference returned on a read needs to be released
			// testShortRef
			Object value = opcEnv.readItem(testShortRef);
		   	System.out.println(new StringBuffer("Read item ").append(opcEnv.getItemName(testShortRef)).
					append(": Type="). append(getTypeString(value)).
					append(" , Value=").append(value));
		   	opcEnv.writeItem(testShortRef, (short) 123);
		   	
			
		   	//testArrRef
		   	value=opcEnv.readItem(testArrRef);
		   	System.out.println(new StringBuffer("Read item ").append(opcEnv.getItemName(testArrRef)).
					append(": Type="). append(getTypeString(value)).
					append(" , Value=").append(value));
		   	opcEnv.writeItem(testArrRef, new short[]{11, 22} );
		   	
		   	// testStrRef
		   	value=opcEnv.readItem(testStrRef);
		   	System.out.println(new StringBuffer("Read item ").append(opcEnv.getItemName(testStrRef)).
					append(": Type="). append(getTypeString(value)).
					append(" , Value=").append(value));
		   	opcEnv.writeItem(testStrRef, "hakan");
		   	
		} catch (IOException e) {
			System.out.println("Failed adding, reading  or writing items");
		}
		
		
		// Add all available items the group
//		try {
//			for(int i=0;i<itemNameList.length;i++) {
//				itemRef = opcEnv.addItem(groupRef, itemNameList[i]);
//				System.out.println("Item added");
//			}
//		} catch (IOException e) {
//			System.out.println("Failed adding item");
//		}
		
		
		// Wait for keyboard input
		try {
			new BufferedReader(new InputStreamReader(System.in)).readLine();
		} catch (IOException ignored){}

		if(itemRef!=0) opcEnv.removeItem(itemRef);
		if(groupRef!=0) opcEnv.removeGroup(groupRef);
		if(connectionRef!=0) opcEnv.closeConnection(connectionRef);
		
		opcEnv.uninitialize();
	}

}