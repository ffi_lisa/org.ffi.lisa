package org.ffi.lisa.opc.jni;

public class OpcEnvironmentRemovedStuff {
	   /**
	    * Associates a Java object with a native reference
	    * 
	    * @param groupRef	Native reference to a group
	    * @param any        Any Object reference to associate
	    */
	   // public native void setConnectionObject(long connectionRef, Object any);
	   
	   /**
	    * Gets an Java object associated with a native reference
	    * 
	    * @param groupRef	Native reference to a group
	    * @param any        Any Object reference to associate
	    */
	   // public native Object getConnectionObject(long connectionRef);
	   
	   
	   /**
	    * Removes an association a Java object from a native reference
	    * 
	    * @param connectionRef	Native reference
	    */
	   //public native void removeConnectionObject(long connectionRef);
	   
	   /**
	    * Associates a Java object with a native group reference
	    * 
	    * @param groupRef	Native reference to a group
	    * @param any        Any Object reference to associate
	    */
	   // public native void setGroupObject(long groupRef, Object any);

	   /**
	    * Get a Java object assocciated with a native group reference
	    * 
	    * @param groupRef	Native reference to a group
	    * @param any        Any Object reference to associate
	    */
	   // public native Object getGroupObject(long groupRef);
	   
	   /**
	    * Adds subscription on value change notifications for all items in a group
	    * 
	    * A listener must be associated with the server connection for 
	    * events to be reported @see setItemListener  
	    * 
	    * @param itemRef		Native item reference 	
	    */
	   // public native void addGroupSubscription(long groupRef);
	   
	   /**
	    * Removes item value change notifications subscription for a group
	    * 
	    * @param itemRef	Native reference to item
	    */
	   // public native void removeGroupSubscription(long groupRef);
	   
	   
	   /**
	    * Remove a group from an Opc Server
	    *  
	    * @param connectionRef Native reference to server group
	    */
	   // public native void removeGroupObject(long groupRef );

	   /**
	    * Associates a Java object with a native Item reference
	    * 
	    * @param itemRef		Native reference to item group
	    * @param any        Any Object reference to associate
	    */
	   // public native void setItemObject(long itemRef, Object any);
	   
	   /**
	    * Get a Java object assocciated with a native Item reference
	    * 
	    * @param itemRef		Native reference to item group
	    */
	   //public native Object getItemObject(long itemRef);
	   
	   /**
	    * Removes the Java Object associated with a native Item reference
	    * 
	    * @param itemRef   Native reference to an item   
	    */
	   // public native void removeItemObject(long itemRef);

	   /**
	    * Adds subscription on value change notifications for an item
	    * 
	    * A listener must be associated with the server connection for 
	    * events to be reported @see setItemListener  
	    * 
	    * @param itemRef		Native item reference 	
	    */
	   // public native void addItemSubscription(long itemRef);
	   
	   /**
	    * Removes an item value change notifications subscription
	    * 
	    * @param itemRef	Native reference to item
	    */
	//   public native void removeItemSubscription(long itemRef);
}
