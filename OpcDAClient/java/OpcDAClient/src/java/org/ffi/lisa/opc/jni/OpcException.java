package org.ffi.lisa.opc.jni;

public class OpcException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	/**
	 * Default constructor.
	 * Calls super with a "No Message Provided" string
	 */
	public OpcException (){
		super("No Message Provided");
		
	}
	
	/**
	 * standard constructor
	 * @param message
	 */
	public OpcException (String message){
		super(message);
	}
	
	/**
	 * standard constructor
	 * @param message
	 */
	public OpcException (String message, Object caller, String error){
		super(message);
	}
	
}
