package org.ffi.lisa.opc.jni;

import java.util.Date;

public interface OpcItemListener {
	/**
	 * Receives notifications of value changes for an item
	 * 
	 * @param itemRef		a reference to the native side item representation
	 * @param timestamp		a time stamp value representing milliseconds passed 
	 * 						since 1970 00:00:00.000. Suitable for the java.sql.TIMESTAMP type 
	 * @param quality		quality of the item value 
	 * @param value			a reference to the native side value representation			
	 */
//	void onItemNotification(long itemRef, long timestamp, long quality, long valueRef);
	void onItemNotification(long itemRef, Date timestamp, long quality, long valueRef);
	
}
