package org.ffi.lisa.deviceadaptor.cncmachine.kth;

//import org.ffi.lisa.core.esbmessage.EsbMessageObjectFactory;
//import org.ffi.lisa.deviceadapter.NCMachine.kth.xpres.model.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Translator {
    private static final String dataPointID = "se.kth.xpres.cnc";

    private EsbMessageObjectFactory factory = new EsbMessageObjectFactory();
    private static Map<String, String> variables = new HashMap<String, String>();

    public Translator() {
        configureVariables("./xml/config.xml");
    }

    private void configureVariables(String fileName) {
        try {
            JAXBContext context = JAXBContext.newInstance(Config.class);
            Unmarshaller um = context.createUnmarshaller();
            Config conf = (Config) um.unmarshal(new FileReader(fileName));
            ArrayList<ConfigItem> list = conf.getItemList();
            for (ConfigItem item : list) {
                variables.put(item.getGeneric(), item.getForeign());
                // System.out.println("Item: " + item.getGeneric() + " equals " + item.getForeign());
            }
        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        log(variables.size() + " variables declared.");
    }

    public static String searchEqualString(String in) {
        String transform = null;

        Iterator<Map.Entry<String, String>> i = variables.entrySet().iterator();
        while(i.hasNext()){
            String key = i.next().getKey();

            if(variables.get(key).equals(in)) {
                transform = key;
                break;
            }
            else
                transform = "unknown Event";

            // System.out.println("1:"+key+",2:"+variables.get(key));
        }

        return transform;
    }

    private static void log(String s) {
        System.out.println(s);
    }
}
