package org.ffi.lisa.deviceadaptor.cncmachine.kth;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

@XmlRootElement(name = "message")
public class MessageHandler {
    String id;
    String timestamp;
    String event;
    String value;

    XMLGregorianCalendar timestampForward;

    /*

    id;
    from timestamp(yyMMddHHmmssSSS) to XMLGregorianCalendar
     */

    public String getId() {
        return id;
    }

    @XmlElement
    public void setId(String in) {
        this.id = in;
    }

    /*
        ----------
    */

    public String getTimestamp() {
        return timestamp;
    }

    @XmlElement
    public void setTimestamp(String timestamp) throws Exception {
        Date date = new Date();
        DateFormat inputFormat = new SimpleDateFormat("yyMMddHHmmssSSS");
        DateFormat outputDateTime = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        try {
            date = inputFormat.parse(timestamp);
        } catch (Exception e) {
            log("Date(Error): " + e);
        }

        GregorianCalendar c = new GregorianCalendar();
        c.setTime(date);
        XMLGregorianCalendar greCalOut = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);

        this.timestamp = outputDateTime.format(date);
        this.timestampForward = greCalOut;
    }

    /*
        ----------
    */

    public String getEvent() {
        return event;
    }

    @XmlElement
    public void setEvent(String event) {
        this.event = Translator.searchEqualString(event);;
    }

    /*
        ----------
    */
    public String getValue() {
        return value;
    }

    @XmlElement // XMLAttribute
    public void setValue(String value) {
        this.value = value;
    }

    /*
        Print Values
    */

    public String print() {
        return "Values: [id=" + id + ", timestamp=" + timestampForward.toString() + ", event=" + event + ", value=" + value + "]";
    }

    private void log(String s) {
        // System.out.println(s);
    }
}