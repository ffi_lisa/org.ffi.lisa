package org.ffi.lisa.idservice;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.ffi.lisa.core.Util;
import org.ffi.lisa.core.endpoint.Endpoint;
import org.ffi.lisa.core.endpoint.EndpointFactory;
import org.ffi.lisa.core.endpoint.LISAMessageReceiver;
import org.ffi.lisa.core.endpoint.LISAMessageInfo;

import org.ffi.lisa.core.lisamessage.LISAAliveMessage;
import org.ffi.lisa.core.lisamessage.LISAAttribute;
import org.ffi.lisa.core.lisamessage.LISAAttributeType;
import org.ffi.lisa.core.lisamessage.LISAMessage;
import org.ffi.lisa.core.lisamessage.LISATypeMessage;
import org.ffi.lisa.core.lisamessage.LISAVariableType;

public class IDService_Endpoint implements LISAMessageReceiver {
	private final Endpoint endpoint;

	private final String idSserviceTopicPrefix = "IDSERVICE";
	private final String registerTopic = idSserviceTopicPrefix + "." + "Register";
	private final String requestTopic = idSserviceTopicPrefix + "." + "Request";
	private final String aliveTopic = idSserviceTopicPrefix + "." + "Alive";
	private final String keepAliveTopic = idSserviceTopicPrefix + "." + "KeepAlive";

	private HashMap<UUID,LISAVariableType> storedVariableTypes = new HashMap<>();
	private HashMap<UUID,String> storedSenders = new HashMap<>(); // TODO
	private HashMap<UUID,LISAAttributeType> storedAttributeTypes = new HashMap<>(); // TODO

	public static void main(String[] args) {
		new IDService_Endpoint();
	}

	public IDService_Endpoint() {
		endpoint = EndpointFactory.createIDEndpoint(new File("data/config_IDService.txt"), this);
	}

	@Override
	public void receive(LISAMessage message, LISAMessageInfo info) {
		Util.log("DEBUG: LISAMessage received on topic "
				+ (info == null ? null : info.getTopic())
				+ ")\n\tThis should not happen.");
	}

	@Override
	public void receive(LISATypeMessage typeMessage, LISAMessageInfo info) {
		Util.log("\nDEBUG: LISATypeMessage received on topic "
				+ (info == null ? null : info.getTopic()));

		if (info != null) {
			// Debug
			Util.log("info.getTopic(): " + info.getTopic() + "\n" +
					"info.getReplyTo(): " + info.getReplyTo() + "\n" +
					"info.getReceiver(): " + info.getReceiver() + "\n" +
					"info.getSender(): " + info.getSender());
			// END Debug
			
			if (!info.getSender().equals(endpoint.getClientID())) { // Reject local messages
				if (info.getTopic().equals(registerTopic)) {
					Util.log("DEBUG: LISAVariableType received on topic " + registerTopic);
					Util.log("DEBUG: Start 'Search_and_Register' ...");
					Search_and_Register(typeMessage.getVariableTypes(), info);
					Util.log("DEBUG: 'Search_and_Register' done.");

				} else if (info.getTopic().equals(requestTopic)) {
					Util.log("DEBUG: LISAVariableType received on topic " + requestTopic);
					Util.log("DEBUG: Start 'Search_and_Return' ...");
					Search_and_Return(typeMessage, info);
					Util.log("DEBUG: 'Search_and_Return' done.");

				} else {
					Util.log("ERROR: LISAVariableType can only be received on topics " + registerTopic + " or " + requestTopic);
				}
			} else {
				Util.log("DEBUG: Local LISATypeMessage received.");
			}
		} else {
			Util.log("ERROR: LISAMessageInfo is null.");
		}
	}

	@Override
	public void receive(LISAAliveMessage aliveMessage, LISAMessageInfo info) {
		Util.log("DEBUG: LISAAliveMessage received on topic " + (info == null ? null : info.getTopic()) + "\n\tERROR: Not yet implemented!");
		// TODO
	}

	private void Search_and_Register(List<LISAVariableType> reqVarTypes, LISAMessageInfo info) {
		Util.log("INFO: Number of VariableTypes stored in the ID Service, before: " + storedVariableTypes.size());
		// For loop 1 (For loop 1 and For loop 2 can be inverted)
		for (LISAVariableType reqVarType : reqVarTypes) {
			// Util.log("Loop 1");
			LISAMessageInfo messageInfo;
			Set<LISAAttribute> reqAtts = reqVarType.getAttributes();

			// For loop 2
			List<LISAVariableType> resultVarTypes =  new ArrayList<>();
			for (LISAVariableType storedVarType : storedVariableTypes.values()) {
				// Util.log("\tLoop 2");
				
				HashSet<LISAAttribute> resultAtts =  new HashSet<>();

				// This does not work because containsAll does not rely on .equals(Object object)
				// if (storedVarTypeAtts.containsAll(reqVarTypeAtts)) {
				// As a consequence, we have to loop over attributes manually
				// END This does not work

				for (LISAAttribute reqAtt : reqAtts) {
					// Util.log("\t\tLoop 3");
					LISAAttribute storedAtt = storedVarType.getAttributes(reqAtt.getType());
					if (storedAtt != null) {
						if (storedAtt.getValue().equals(reqAtt.getValue())) {
							resultAtts.add(reqAtt);
						}
					}
				}

				if (resultAtts.size() == reqAtts.size()) { // Match found
					resultVarTypes.add(storedVarType); // Contrary to reqVarType, storedVarType contains the UUID
				}				
			}
			// END For loop 2

			// Analyze and send message(s)
			if (resultVarTypes.size() == 0) {
				// Generate an UUID and check its uniqueness
				UUID id = UUID.randomUUID();
				while (storedVariableTypes.keySet().contains(id)) {
					id = UUID.randomUUID();
				}

				LISAVariableType retVarType = new LISAVariableType(id, reqAtts);
				storedVariableTypes.put(id, retVarType);
				Util.log("INFO: VariableType registered: " + retVarType.getId() + " / " + retVarType.getName());
				resultVarTypes.add(retVarType);

				HashMap<String, String> map = new HashMap<>();
				map.put("Sender", endpoint.getClientID());
				map.put("Receiver", info.getReplyTo());
				map.put("RequestStatus", "OK");
				messageInfo = new LISAMessageInfo(map);

			} else if (resultVarTypes.size() == 1) {
				Util.log("WARNING: VariableType already exists: One instance found.");
				HashMap<String, String> map = new HashMap<>();
				map.put("Sender", endpoint.getClientID());
				map.put("Receiver", info.getReplyTo());
				map.put("RequestStatus", "WARNING");
				messageInfo = new LISAMessageInfo(map);

			} else {
				Util.log("ERROR: VariableType is not unique: " + resultVarTypes.size() + " instances found.");
				HashMap<String, String> map = new HashMap<>();
				map.put("Sender", endpoint.getClientID());
				map.put("Receiver", info.getReplyTo());
				map.put("RequestStatus", "ERROR");
				messageInfo = new LISAMessageInfo(map);
			}
			
			LISATypeMessage typeMessage = new LISATypeMessage(resultVarTypes);
			endpoint.registerLISAVariableTypes(typeMessage, messageInfo);
		}
		// END For loop 1
		Util.log("INFO: Number of VariableTypes stored in the ID Service, after: " + storedVariableTypes.size());
	}

	private void Search_and_Return(LISATypeMessage typeMessage, LISAMessageInfo info) {
		Util.log("INFO: Number of VariableTypes stored in the ID Service: " + storedVariableTypes.size());
		
		List<LISAVariableType> reqVarTypes = typeMessage.getVariableTypes();
		
		// For loop 1 (For loop 1 and For loop 2 can be inverted)
		for (LISAVariableType reqVarType : reqVarTypes) {
			// Util.log("Loop 1");
			LISAMessageInfo messageInfo;
			Set<LISAAttribute> reqAtts = reqVarType.getAttributes();

			// For loop 2
			List<LISAVariableType> resultVarTypes =  new ArrayList<>();
			for (LISAVariableType storedVarType : storedVariableTypes.values()) {
				// Util.log("\tLoop 2");

				HashSet<LISAAttribute> resultAtts =  new HashSet<>();

				// This does not work because containsAll does not rely on .equals(Object object)
				//if (storedVarTypeAtts.containsAll(reqVarTypeAtts)) {
				// As a consequence, we have to loop over attributes manually
				// END This does not work

				for (LISAAttribute reqAtt : reqAtts) {
					// Util.log("\t\tLoop 3");
					LISAAttribute storedAtt = storedVarType.getAttributes(reqAtt.getType());
					if (storedAtt != null) {
						if (storedAtt.getValue().equals(reqAtt.getValue())) {
							resultAtts.add(reqAtt);
						}
					}
				}

				if (resultAtts.size() == reqAtts.size()) { // Match found
					resultVarTypes.add(storedVarType); // Contrary to reqVarType, storedVarType contains the UUID
				}				
			}
			// END For loop 2

			// Analyze and send message(s)
			if (resultVarTypes.size() == 1) {
				Util.log("INFO: VariableType already exists: One instance found.");
				HashMap<String, String> map = new HashMap<>();
				map.put("Sender", endpoint.getClientID());
				map.put("Receiver", info.getReplyTo());
				map.put("RequestStatus", "OK");
				messageInfo = new LISAMessageInfo(map);

			} else if (resultVarTypes.size() == 0) {
				Util.log("ERROR: VariableType not found: " + reqVarType.getId() + " / " + reqVarType.getName());

				HashMap<String, String> map = new HashMap<>();
				map.put("Sender", endpoint.getClientID());
				map.put("Receiver", info.getReplyTo());
				map.put("RequestStatus", "ERROR");
				messageInfo = new LISAMessageInfo(map);
				
				resultVarTypes.add(reqVarType);

			} else {
				Util.log("WARNING: VariableType is not unique: " + resultVarTypes.size() + " instances found.");
				HashMap<String, String> map = new HashMap<>();
				map.put("Sender", endpoint.getClientID());
				map.put("Receiver", info.getReplyTo());
				map.put("RequestStatus", "ERROR");
				messageInfo = new LISAMessageInfo(map);

			}
			
			LISATypeMessage newTypeMessage = new LISATypeMessage(resultVarTypes);
			endpoint.queryLISAVariableTypes(newTypeMessage, messageInfo);
		}
		// END For loop 1
	}
}
