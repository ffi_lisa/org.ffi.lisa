package org.ffi.lisa.demonstrator;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.ffi.lisa.core.Util;
import org.ffi.lisa.core.endpoint.Endpoint;
import org.ffi.lisa.core.endpoint.EndpointFactory;
import org.ffi.lisa.core.endpoint.LISAMessageInfo;
import org.ffi.lisa.core.endpoint.LISAMessageReceiver;
import org.ffi.lisa.core.lisamessage.LISAAliveMessage;
import org.ffi.lisa.core.lisamessage.LISAAttribute;
import org.ffi.lisa.core.lisamessage.LISAAttributeType;
import org.ffi.lisa.core.lisamessage.LISAMessage;
import org.ffi.lisa.core.lisamessage.LISASender;
import org.ffi.lisa.core.lisamessage.LISATypeMessage;
import org.ffi.lisa.core.lisamessage.LISATypeVariable;
import org.ffi.lisa.core.lisamessage.LISAVariable;
import org.ffi.lisa.demonstrator.receiver.TimedEventReceiver;
import org.ffi.lisa.demonstrator.test.RunDemo;
import org.ffi.lisa.demonstrator.vo.TimedEventVO;

public class TimedEventEndPoint implements LISAMessageReceiver {
	
	private Logger logger = Logger.getLogger(TimedEventEndPoint.class);
	private TimedEventReceiver timedEventReceiver;

	private Endpoint demoEndPoint;
	
	public TimedEventEndPoint(String configuration) { 
	
	Endpoint demoEndPoint = EndpointFactory.createEndpoint(new File(configuration), this);
	
	try { Thread.sleep(1000); } catch (InterruptedException e) {}
	
	String topic = "LEVEL0.EventNotification";
	
	LISAAttributeType attType_Enterprise = new LISAAttributeType("Enterprise", UUID.fromString("ff0ed2bb-6355-4a98-98b5-ffb58f60d776"));
	LISAAttributeType attType_Site = new LISAAttributeType("Site", UUID.fromString("47cb8652-7871-4698-bc9a-d6daab1b8659"));
	LISAAttributeType attType_Area = new LISAAttributeType("Area", UUID.fromString("918063ad-67b0-474b-9a34-895938e2180b"));
	LISAAttributeType attType_Line = new LISAAttributeType("Line", UUID.fromString("286fc9e1-9f25-42d5-be08-426f56cea74e"));
	LISAAttributeType attType_Unit = new LISAAttributeType("Unit", UUID.fromString("2cad9f1a-376e-4984-a530-42978562d2e2"));
	LISAAttributeType attType_Name = new LISAAttributeType("Name", UUID.fromString("34902ed8-1ea1-4e25-96e4-45b689793392"));

	
	HashSet<LISAAttribute> attributes1 = new HashSet<>();
	HashSet<LISAAttribute> attributes2 = new HashSet<>();
	HashSet<LISAAttribute> attributes3 = new HashSet<>();
	HashSet<LISAAttribute> attributes4 = new HashSet<>();
	HashSet<LISAAttribute> attributes5 = new HashSet<>();
	HashSet<LISAAttribute> attributes6 = new HashSet<>();
	HashSet<LISAAttribute> attributes7 = new HashSet<>();
	HashSet<LISAAttribute> attributes8 = new HashSet<>();
	HashSet<LISAAttribute> attributes9 = new HashSet<>();
	HashSet<LISAAttribute> attributes0 = new HashSet<>();

	LISAAttribute attEnterprise= new LISAAttribute(attType_Enterprise,"LISA");
	LISAAttribute attSite = new LISAAttribute(attType_Site,"Chalmers");
	LISAAttribute attSite2 = new LISAAttribute(attType_Site,"VOLVO");
	LISAAttribute attArea = new LISAAttribute(attType_Area,"PSL");
	LISAAttribute attLine = new LISAAttribute(attType_Line,"Conveyor");
	LISAAttribute attUnit1 = new LISAAttribute(attType_Unit,"Station1");
	LISAAttribute attUnit2 = new LISAAttribute(attType_Unit,"Station2");
	LISAAttribute attUnit3 = new LISAAttribute(attType_Unit,"Station3");
	LISAAttribute attUnit4 = new LISAAttribute(attType_Unit,"Station4");
	LISAAttribute attUnit5 = new LISAAttribute(attType_Unit,"Station5");
	LISAAttribute attUnit6 = new LISAAttribute(attType_Unit,"Station6");
	LISAAttribute attUnit7 = new LISAAttribute(attType_Unit,"Station7");
	LISAAttribute attUnit8 = new LISAAttribute(attType_Unit,"Station8");
	LISAAttribute attUnit9 = new LISAAttribute(attType_Unit,"Station9");
	LISAAttribute attUnit0 = new LISAAttribute(attType_Unit,"Station0");
	LISAAttribute attName1 = new LISAAttribute(attType_Name,"Exit");
	LISAAttribute attName2 = new LISAAttribute(attType_Name,"Enter");
	LISAAttribute attName3 = new LISAAttribute(attType_Name,"RFID");
	LISAAttribute attName4 = new LISAAttribute(attType_Name,"Counter");
	LISAAttribute attName5 = new LISAAttribute(attType_Name,"ProductID");
	
	attributes1.add(attEnterprise);
	attributes1.add(attSite);
	attributes1.add(attArea);
	attributes1.add(attLine);
	attributes1.add(attUnit1);
	attributes1.add(attName1);
	
	attributes2.add(attEnterprise);
	attributes2.add(attSite);
	attributes2.add(attArea);
	attributes2.add(attLine);
	attributes2.add(attUnit2);
	attributes2.add(attName1);
	
	attributes3.add(attEnterprise);
	attributes3.add(attSite);
	attributes3.add(attArea);
	attributes3.add(attLine);
	attributes3.add(attUnit3);
	attributes3.add(attName2);
	
	attributes4.add(attEnterprise);
	attributes4.add(attSite);
	attributes4.add(attArea);
	attributes4.add(attLine);
	attributes4.add(attUnit4);
	attributes4.add(attName2);
	
	attributes5.add(attEnterprise);
	attributes5.add(attSite);
	attributes5.add(attArea);
	attributes5.add(attLine);
	attributes5.add(attUnit5);
	attributes5.add(attName3);
	
	attributes6.add(attEnterprise);
	attributes6.add(attSite);
	attributes6.add(attArea);
	attributes6.add(attLine);
	attributes6.add(attUnit6);
	attributes6.add(attName3);
	
	attributes7.add(attEnterprise);
	attributes7.add(attSite);
	attributes7.add(attArea);
	attributes7.add(attLine);
	attributes7.add(attUnit7);
	attributes7.add(attName3);
	
	attributes8.add(attEnterprise);
	attributes8.add(attSite);
	attributes8.add(attArea);
	attributes8.add(attLine);
	attributes8.add(attUnit8);
	attributes8.add(attName4);
	
	attributes9.add(attEnterprise);
	attributes9.add(attSite);
	attributes9.add(attArea);
	attributes9.add(attLine);
	attributes9.add(attUnit9);
	attributes9.add(attName5);
	
	attributes0.add(attEnterprise);
	attributes0.add(attSite2);
	attributes0.add(attName1);

	// First step --> REGISTER
	LISATypeVariable lisaVarType1 = new LISATypeVariable(null,attributes1);
	LISATypeVariable lisaVarType2 = new LISATypeVariable(null,attributes2);
	LISATypeVariable lisaVarType3 = new LISATypeVariable(null,attributes3);
	LISATypeVariable lisaVarType4 = new LISATypeVariable(null,attributes4);
	LISATypeVariable lisaVarType5 = new LISATypeVariable(null,attributes5);
	LISATypeVariable lisaVarType6 = new LISATypeVariable(null,attributes6);
	LISATypeVariable lisaVarType7 = new LISATypeVariable(null,attributes7);
	LISATypeVariable lisaVarType8 = new LISATypeVariable(null,attributes8);
	LISATypeVariable lisaVarType9 = new LISATypeVariable(null,attributes9);
	LISATypeVariable lisaVarType0 = new LISATypeVariable(null,attributes0);
	
	List<LISATypeVariable> lisaVarTypes = new ArrayList<>();
	lisaVarTypes.add(lisaVarType1);
	lisaVarTypes.add(lisaVarType2);
	lisaVarTypes.add(lisaVarType3);
	lisaVarTypes.add(lisaVarType4);
	lisaVarTypes.add(lisaVarType5);
	lisaVarTypes.add(lisaVarType6);
	lisaVarTypes.add(lisaVarType7);
	lisaVarTypes.add(lisaVarType8);
	lisaVarTypes.add(lisaVarType9);
	lisaVarTypes.add(lisaVarType0);
	
	LISASender sender = new LISASender(demoEndPoint.getClientId(), UUID.randomUUID());		
	LISATypeMessage typeMessage = new LISATypeMessage(null, lisaVarTypes, sender);
	
	HashMap<String, String> map = new HashMap<>();
	map.put("ReplyTo", demoEndPoint.getClientId());
//	map.put("Sender", endpoint.getClientId());
	LISAMessageInfo messageInfo = new LISAMessageInfo(map);
	Util.log("messageInfo sent: " + messageInfo.getInfos());
	
//	endpoint.registerID(typeMessage, messageInfo);
//	endpoint.requestID(typeMessage, messageInfo); // This was not supposed to be sent by the end-user
	
	
	
	// Secondly, SEND
//	String CopyHereTheUUIDReceived = "4a27aee1-d78f-4b91-9ad1-5cf6c57f0694";
	String CopyHereTheUUIDReceived = "";
	UUID idVar = null;
	if (CopyHereTheUUIDReceived != "") {
		idVar = UUID.fromString(CopyHereTheUUIDReceived);
	}
	
	// Test with wrong attribute
	HashSet<LISAAttribute> attributesWrong1 = new HashSet<>();
	attributesWrong1.add(attEnterprise);
	attributesWrong1.add(attSite);
	attributesWrong1.add(attArea);
	
	LISAVariable lisaVar1 = new LISAVariable(new Boolean(true), idVar, attributes1);
	LISAVariable lisaVar2 = new LISAVariable(new Boolean(false), idVar, attributes2);
	LISAVariable lisaVar3 = new LISAVariable(new Boolean(true), idVar, attributes3);
	LISAVariable lisaVar4 = new LISAVariable(new String("abc"), idVar, attributes4);
	LISAVariable lisaVar5 = new LISAVariable(new String("DEF"), idVar, attributes5);
	LISAVariable lisaVar6 = new LISAVariable(new String("1A%"), idVar, attributes6);
	LISAVariable lisaVar7 = new LISAVariable(new Double(0.0789), idVar, attributes7);
	LISAVariable lisaVar8 = new LISAVariable(new Double(-987.654), idVar, attributes8);
	LISAVariable lisaVar9 = new LISAVariable(new Integer(147), idVar, attributes9);
	LISAVariable lisaVar0 = new LISAVariable(new Integer(-35), idVar, attributes0);
	
	
	List<LISAVariable> lisaVars = new ArrayList<>();
	//LISAVariableBuilder builder = new LISAVariableBuilder(); // TODO
	
//	lisaVars.add(lisaVar1);
//	lisaVars.add(lisaVar2);
//	lisaVars.add(lisaVar0);
	
			
	LISAMessage message = new LISAMessage(topic, null, lisaVars, sender);
	
	
	HashMap<String, String> map3 = new HashMap<>();
	LISAMessageInfo messageInfo3 = new LISAMessageInfo(map3);
	Util.log("messageInfo sent: " + messageInfo.getInfos());
	demoEndPoint.send(message, messageInfo3); // TODO If message info is null, many NullPointerException are returned
	
	}	
	
//	Util.log("Waiting 15 seconds");
//	Thread.sleep(15000);
	
	
//	Thread.sleep(5000);
//	endpoint.registerID(typeMessage);
//	
//	Thread.sleep(5000);
//	endpoint.registerID(typeMessage);
//	
//	Thread.sleep(5000);
//	endpoint.registerID(typeMessage);
	
//	System.exit(1);
	
	
//	Thread.sleep(1000);
//	LISAVariable lisaVar = new LISAVariable(new Boolean(false),UUID.randomUUID(),attributes);
//	
//	List<LISAVariable> lisaVars = new ArrayList<>();
//	//LISAVariableBuilder builder = new LISAVariableBuilder(); // TODO
//	lisaVars.add(lisaVar);
//	
//	LISAMessage lisaMessage = new LISAMessage(topic, "2013-06-28T12:34:56.789Z", lisaVars, sender);
//	endpoint.send(lisaMessage);
	
	
	@Override
	public void receive(LISAMessage message, LISAMessageInfo info) {
		if(logger.isDebugEnabled()) {
			logger.debug("LISAMessage received on topic " + (info == null ? null : info.getTopic()) + 
					" (@" + this.getClass().getSimpleName() + ")\n " +
					"\tTopic: " + (info == null ? null : info.getTopic()) + "\n" +
					"\tReplyTo: " + (info == null ? null : info.getReplyTo()) + "\n" +
					"\tReceiver: " + (info == null ? null : info.getReceiver()) + "\n" +
					"\tSender: " + (info == null ? null : info.getSender()) + "\n" +
					"\tInfos: " + (info == null ? null : info.getInfos()));
		}
		
		if (info != null) {
			for (LISAVariable var : message.getVariables()){
				StringBuffer sb= new StringBuffer("").
				append("UUID: " + (var.getId() == null ? null : var.getId().toString())).
				append(", String: "+ var.getString()).
				append(", Boolean: "+ var.getBoolean()).
				append(", Integer: "+ var.getInteger()).
				append(", Double: "+ var.getDouble());
				
				logger.info(sb.toString());
				
				for (LISAAttribute att : var.getAttributes()) {
					logger.info("Attribute: " + att.getType().getName() + " = " + att.getValue()) ;
				}
			}
		}
		
		
		if(timedEventReceiver!=null) {
			TimedEventVO eventVO = new TimedEventVO("message", true, new Date(System.currentTimeMillis()),"info");
			timedEventReceiver.onTimedEventNotification(eventVO);	
		}
	}
	

	@Override
	public void receive(LISATypeMessage typeMessage, LISAMessageInfo info) {
		if(logger.isDebugEnabled()) {
			logger.debug("LISATypeMessage received on topic " + (info == null ? null : info.getTopic()) + 
					" (@" + this.getClass().getSimpleName() + ")\n " +
					"\tTopic: " + (info == null ? null : info.getTopic()) + "\n" +
					"\tReplyTo: " + (info == null ? null : info.getReplyTo()) + "\n" +
					"\tReceiver: " + (info == null ? null : info.getReceiver()) + "\n" +
					"\tSender: " + (info == null ? null : info.getSender()) + "\n" +
					"\tInfos: " + (info == null ? null : info.getInfos()));
		}
		
		if (info != null) {
			logger.info("LISAVariableType received on topic " + info.getTopic());
			
			if (typeMessage.getVariableTypes().size() > 0) {
				for (LISATypeVariable varType : typeMessage.getVariableTypes()) {
					logger.info("VariableType's Name|Id: " + varType.getName() + "|" + (varType.getId() == null ? null : varType.getId().toString()));
					HashSet<LISAAttribute> varTypeAtts = varType.getAttributes();
					for (LISAAttribute varTypeAtt : varTypeAtts) {
						logger.info("\t\tAttribute's Value|AttTypeName|AttTypeId: " + 
									varTypeAtt.getValue() + "|" +
									varTypeAtt.getType().getName() + "|" +
									varTypeAtt.getType().getId());
					}
					
				}
			}
		} else {
			if(logger.isDebugEnabled()) {
				logger.debug("MessageInfo is empty");
			}
		}
		
		if(timedEventReceiver!=null) {
			TimedEventVO eventVO = new TimedEventVO("type", true, new Date(System.currentTimeMillis()),"info");
			timedEventReceiver.onTimedEventNotification(eventVO);	
		}
		
	}

	@Override
	public void receive(LISAAliveMessage aliveMessage, LISAMessageInfo info) {
		logger.debug("LISAAliveMessage received on topic " + (info == null ? null : info.getTopic()) + " (@" + this.getClass().getSimpleName() + ")");
		
		if(timedEventReceiver!=null) {
			TimedEventVO eventVO = new TimedEventVO("alive", true, new Date(System.currentTimeMillis()),"info");
			timedEventReceiver.onTimedEventNotification(eventVO);	
		}
	}
	
	
	public void setTimedEventReceiver(TimedEventReceiver timedEventReceiver) {
		this.timedEventReceiver = timedEventReceiver;
	}
	
}	
