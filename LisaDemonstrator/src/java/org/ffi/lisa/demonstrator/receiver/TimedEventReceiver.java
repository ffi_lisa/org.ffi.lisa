package org.ffi.lisa.demonstrator.receiver;

import org.ffi.lisa.demonstrator.vo.TimedEventVO;

public interface TimedEventReceiver {
	
	public void onTimedEventNotification(TimedEventVO event);

}
