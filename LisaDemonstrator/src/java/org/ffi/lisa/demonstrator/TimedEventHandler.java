package org.ffi.lisa.demonstrator;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.ffi.lisa.demonstrator.vo.TimedEventVO;

import sun.util.logging.resources.logging;

/**
 * 
 * This handler do required calculation for the work cell and production objects associated with a timed Event 
 *
 * @author HPETTE11
 *
 */
public class TimedEventHandler extends Thread {

	// Our logger
	private Logger logger = Logger.getLogger(TimedEventHandler.class);
	
	// Place holder for timed events
	private final List<TimedEventVO> timedEvents = new ArrayList<TimedEventVO>();

	// Heart-beat time
	private static long DEFAULT_HEARTBEAT_TIME = 30000;
	private long heartbeatTime = DEFAULT_HEARTBEAT_TIME;
	
	private WorkCellHandler workcellHandler;
	private ProductHandler productHandler; 

	private boolean running = false;
	
	public void open() {
		this.start();
        if(logger.isDebugEnabled()) {
        	logger.debug("TimedEventHandler opened");
        }
	}
	
	public void close() {
		running=false;
        if(logger.isDebugEnabled()) {
        	logger.debug("Closing of TimedEventHandler initiated");
        }
        synchronized(timedEvents) {
        	timedEvents.notify();
        }
		try {
			this.join();
		} catch (InterruptedException e) {
			this.interrupt();
		}
	}

	/**
	 * Adds a new timed event to the que
	 * @param event
	 */
	public void addTimedEvent(TimedEventVO event) {
        synchronized (timedEvents) {
    		timedEvents.add(event);
    		timedEvents.notify();
        }
	}
	
	public void run() {
		
		// We are alive
		running = true;
		
		while (running) {
		   	long startTime;
			long elapsedTime=0;
			
			// Wait for messages to arrive
		    if (timedEvents.size() == 0) {
		        startTime = System.currentTimeMillis();
		        if(logger.isDebugEnabled()) {
		        	logger.debug("Waiting for timed events to arrive");
		        }
		        try {
		            synchronized (timedEvents) {
		            	timedEvents.wait(heartbeatTime);
		            }
		        } catch (InterruptedException e) {
		            // Ignore
		        }
		        elapsedTime = System.currentTimeMillis()-startTime ;
		    }
		    if(!running) break;
		    
		    if(elapsedTime>=heartbeatTime && timedEvents.isEmpty()) {
			    // Any heart beat related tasks should be placed here
	        	logger.debug("Heart beat");
		    }
		    
		    // We have something to take care of
		    if(!timedEvents.isEmpty()) {
		    	
		    	// Transfer newly arrived messages to pending buffer
		    	TimedEventVO[] pendingEvents = null;
		        synchronized (timedEvents) {
		        	pendingEvents = timedEvents.toArray(new TimedEventVO[0]);
		        	timedEvents.clear();
		        }
		        
		        if(logger.isDebugEnabled()) {
		        	logger.debug("Handling pending timed events");
		        }
		        
		        // Do the stuff we should do
				for (int i=0; i < pendingEvents.length; i++) {
					if(logger.isDebugEnabled()) {
						logger.debug("Handled TimedEvent: " + pendingEvents[i].toString());
					}
					
					// Get associated workCell
					
					// If online workcell
					//    Add the product to our product handler
					
					// If OP1 workcell and start event 
					//    Get the newly online entered product from FIFO  
					// 	  Associate the RFID carrier with the newly added 
					
				}		        
		        
		    } // eof if(!messages.isEmpty())
		} // eof while(running)
	} // eof doRun
	
	public void setWorkcellHandler(WorkCellHandler workcellHandler) {
		this.workcellHandler = workcellHandler;
	}

	public void setProductHandler(ProductHandler productHandler) {
		this.productHandler = productHandler;
	}
	
	public boolean isRunning() {
		return running;
	}
	
	
}
