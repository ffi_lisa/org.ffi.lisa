package org.ffi.lisa.demonstrator.vo;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.sun.org.apache.xpath.internal.operations.Bool;

public class TimedEventVO {
	
	private String workCell;
	private Boolean eventType;
	private Date timeStamp;
	private String referenceID;
	
	private SimpleDateFormat timestampFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

	public TimedEventVO(String workcell, Boolean eventtype, Date timestamp, String referenceID) {
		this.workCell= workcell;
		this.eventType = eventtype;
		this.timeStamp = timestamp;
		this.referenceID = referenceID;
	}
	
	
	public String getWorkCell() {
		return workCell;
	}

	public void setWorkCell(String workCell) {
		this.workCell = workCell;
	}

	public Boolean getEventType() {
		return eventType;
	}

	public void setEventType(Boolean eventType) {
		this.eventType = eventType;
	}

	public Date getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getProductID() {
		return referenceID;
	}

	public void setProductID(String productID) {
		this.referenceID = productID;
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer("TimedEvent: workCell=").append(workCell).
											append(", eventType=").append(eventType).
											append(", referenceID=").append(referenceID).
											append(", timeStamp=").append(timestampFormat.format(timeStamp));

		return sb.toString();
	}
}
