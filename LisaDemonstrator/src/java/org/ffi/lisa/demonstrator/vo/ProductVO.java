package org.ffi.lisa.demonstrator.vo;

// Contains static information about the different product types that will be handled
public class ProductVO {
	
	private String id;
	
	public ProductVO(String id) {
		this.id=id; 
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	
}
