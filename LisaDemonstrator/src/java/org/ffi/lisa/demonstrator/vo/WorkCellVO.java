package org.ffi.lisa.demonstrator.vo;

//Contains static information about the workcells that will be handled
public class WorkCellVO {
	
	private String id;

	public WorkCellVO(String id) {
		this.id=id; 
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
}
