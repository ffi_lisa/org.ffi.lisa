package org.ffi.lisa.demonstrator.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.HashSet;
import java.util.Timer;

import javax.xml.parsers.FactoryConfigurationError;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import org.ffi.lisa.core.Util;
import org.ffi.lisa.core.endpoint.Endpoint;
import org.ffi.lisa.core.endpoint.EndpointFactory;
import org.ffi.lisa.core.endpoint.LISAMessageInfo;
import org.ffi.lisa.core.endpoint.LISAMessageReceiver;
import org.ffi.lisa.core.lisamessage.LISAAliveMessage;
import org.ffi.lisa.core.lisamessage.LISAAttribute;
import org.ffi.lisa.core.lisamessage.LISAMessage;
import org.ffi.lisa.core.lisamessage.LISATypeMessage;
import org.ffi.lisa.core.lisamessage.LISATypeVariable;
import org.ffi.lisa.core.lisamessage.LISAVariable;
import org.ffi.lisa.demonstrator.ProductHandler;
import org.ffi.lisa.demonstrator.TimedEventEndPoint;
import org.ffi.lisa.demonstrator.TimedEventHandler;
import org.ffi.lisa.demonstrator.WorkCellHandler;
import org.ffi.lisa.demonstrator.receiver.TimedEventReceiver;
import org.ffi.lisa.demonstrator.vo.TimedEventVO;

public class RunDemo implements TimedEventReceiver{
	
	// Our logger
	private Logger logger = Logger.getLogger(RunDemo.class);
	
	private LISAMessage	demoMessage;
	
	TimedEventEndPoint endPoint; 
	
	TimedEventHandler eventHandler;
	WorkCellHandler workcellHandler;
	ProductHandler productHandler;
	
	
	/**
	 * Tests the native OpcEnvironment functions
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		RunDemo rd = new RunDemo ();
		rd.doRun();
	}
	

	// The Item notification call back
	public void onTimedEventNotification(TimedEventVO event) {

		if(!eventHandler.isRunning()) {
			logger.warn("Timed Event Handler not started");
			return;
		}
		
		// Handle the event
		eventHandler.addTimedEvent(event);
		
	}

	private void doRun() {
		
		// Load log4j configuration
		try {
			DOMConfigurator.configureAndWatch("./src/conf/log4j.xml"); 
		} catch (FactoryConfigurationError e) {
			System.out.println("Could not read the log4j XML configuration file " + e.toString());
			return;
		}
		
		// Create the EndPoint used to receive stuff5
		
		endPoint = new TimedEventEndPoint("./src/conf/demoEndPoint.cfg"); 		

		// Create Event handler thread
		workcellHandler = new WorkCellHandler();
		productHandler = new ProductHandler();
		eventHandler = new TimedEventHandler();
		
		
		demoMessage = new LISAMessage(null, null, null, null);
		
		eventHandler.setWorkcellHandler(workcellHandler);
		eventHandler.setProductHandler(productHandler);		
		
		eventHandler.open();
		
	
		// Wait for keyboard input
		try {
//			
//			TimedEventVO eventVO;
//			
//			// Emulate some events
//			eventVO = new TimedEventVO("online", true, new Date(System.currentTimeMillis()), "Bacon&Egg");			
//			this.onTimedEventNotification(eventVO);
//			
//			try { Thread.sleep(2000); } catch (InterruptedException e) {}
//			
//			eventVO = new TimedEventVO("cnc", true, new Date(System.currentTimeMillis()), null);
//			this.onTimedEventNotification(eventVO);
//			
//			try { Thread.sleep(4000); } catch (InterruptedException e) {}
//			
//			eventVO = new TimedEventVO("cnc", false, new Date(System.currentTimeMillis()), null);
//			this.onTimedEventNotification(eventVO);
//			
//			try { Thread.sleep(1000); } catch (InterruptedException e) {}
//			
//			eventVO = new TimedEventVO("op1", true, new Date(System.currentTimeMillis()), "1234");
//			this.onTimedEventNotification(eventVO);
//			
//			try { Thread.sleep(1800); } catch (InterruptedException e) {}
//			
//			eventVO = new TimedEventVO("op1", false, new Date(System.currentTimeMillis()), "1234");
//			this.onTimedEventNotification(eventVO);
//			
			logger.info("Wait for keyboard input");
			new BufferedReader(new InputStreamReader(System.in)).readLine();
			
		} 
		catch (IOException ignored){}
		
		
		eventHandler.close();
		
	}


}
