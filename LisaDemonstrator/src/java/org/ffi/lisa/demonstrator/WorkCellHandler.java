package org.ffi.lisa.demonstrator;

import java.util.HashMap;
import java.util.Map;

import org.ffi.lisa.demonstrator.vo.WorkCellVO;

public class WorkCellHandler {
	
	// We define the workcells we will use statically here 
	private static String WORKCELL_ONLINE_ID = "online";
	private static String WORKCELL_CNC_ID = "cnc";
	private static String WORKCELL_OP1_ID = "op1";
	private static String WORKCELL_OP2_ID = "op2";
	private static String WORKCELL_OFFLINE_ID = "offline";

	// The map of workcells
	private Map<String, WorkCellVO> workcells = new HashMap<String, WorkCellVO>();
	
	public WorkCellHandler()  {
		
		// Create ProductionLine
		workcells.put(WORKCELL_ONLINE_ID, new WorkCellVO(WORKCELL_ONLINE_ID));		
		workcells.put(WORKCELL_CNC_ID, new WorkCellVO(WORKCELL_CNC_ID));		
		workcells.put(WORKCELL_OP1_ID, new WorkCellVO(WORKCELL_OP1_ID));		
		workcells.put(WORKCELL_OP2_ID, new WorkCellVO(WORKCELL_OP2_ID));		
		workcells.put(WORKCELL_OFFLINE_ID, new WorkCellVO(WORKCELL_OFFLINE_ID));
		
	}
	
}
