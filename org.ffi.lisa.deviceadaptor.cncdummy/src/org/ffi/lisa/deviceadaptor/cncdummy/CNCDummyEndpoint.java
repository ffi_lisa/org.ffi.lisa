package org.ffi.lisa.deviceadaptor.cncdummy;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.ffi.lisa.core.Util;
import org.ffi.lisa.core.endpoint.EndpointExtended;
import org.ffi.lisa.core.endpoint.EndpointFactory;
import org.ffi.lisa.core.endpoint.LISAMessageReceiver;
import org.ffi.lisa.core.endpoint.LISAMessageInfo;
import org.ffi.lisa.core.lisamessage.LISAAliveMessage;
import org.ffi.lisa.core.lisamessage.LISAAttribute;
import org.ffi.lisa.core.lisamessage.LISAAttributeType;
import org.ffi.lisa.core.lisamessage.LISAMessage;
import org.ffi.lisa.core.lisamessage.LISATypeMessage;
import org.ffi.lisa.core.lisamessage.LISAVariable;
import org.ffi.lisa.core.lisamessage.LISAVariableType;

public class CNCDummyEndpoint implements LISAMessageReceiver, Runnable {
	private EndpointExtended endpoint;
	private Map<String, UUID> name2id = new HashMap<>();
	private Map<UUID, LISAVariableType> id2varType = new HashMap<>();
	private List<LISAVariableType> pending = new ArrayList<>();
	private List<LISAMessage> messages = new ArrayList<>();
	
	public static void main(String[] args) throws Exception {
		new CNCDummyEndpoint();
	}
	
	public CNCDummyEndpoint() {
		try {
			endpoint = EndpointFactory.createEndpoint(new File("data/topics.txt"), this);
			if (!endpoint.isRunning()) {
				Util.log("FATAL: Endpoint setup failed.");
				System.exit(1);
			} else {
				Util.log("INFO: Endpoint setup successful.");
			}
			
			configureVariables();
			synchronized (pending) {
				while (pending.size() != 0) {
					pending.wait();
				}
			}
			
			new Thread(this).start();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	private void configureVariables() {
		// Set up AttributeTypes
		LISAAttributeType attrTypeEnterprise = new LISAAttributeType("Enterprise", UUID.fromString("ff0ed2bb-6355-4a98-98b5-ffb58f60d776"));
		LISAAttributeType attrTypeSite = new LISAAttributeType("Site", UUID.fromString("47cb8652-7871-4698-bc9a-d6daab1b8659"));
		LISAAttributeType attrTypeArea = new LISAAttributeType("Area", UUID.fromString("918063ad-67b0-474b-9a34-895938e2180b"));
		LISAAttributeType attrTypeLine = new LISAAttributeType("Line", UUID.fromString("286fc9e1-9f25-42d5-be08-426f56cea74e"));
		LISAAttributeType attrTypeUnit = new LISAAttributeType("Unit", UUID.fromString("2cad9f1a-376e-4984-a530-42978562d2e2"));
		LISAAttributeType attrTypeName = new LISAAttributeType("Name", UUID.fromString("34902ed8-1ea1-4e25-96e4-45b689793392"));
		
		// Create types
		Set<LISAAttribute> attributes;
		LISAVariableType type;
		
		synchronized (pending) {
			attributes = new HashSet<>();
			attributes.add(new LISAAttribute(attrTypeEnterprise, "LISA"));
			attributes.add(new LISAAttribute(attrTypeSite, "Chalmers"));
			attributes.add(new LISAAttribute(attrTypeArea, "PSL"));
			attributes.add(new LISAAttribute(attrTypeUnit, "CNC"));
			attributes.add(new LISAAttribute(attrTypeName, "Started"));
			type = new LISAVariableType(null, attributes);
			pending.add(type);
			Util.log("INFO: Registering LISAVariableType \"Started\".");
			endpoint.registerLISAVariableType(type);
			
			attributes = new HashSet<>();
			attributes.add(new LISAAttribute(attrTypeEnterprise, "LISA"));
			attributes.add(new LISAAttribute(attrTypeSite, "Chalmers"));
			attributes.add(new LISAAttribute(attrTypeArea, "PSL"));
			attributes.add(new LISAAttribute(attrTypeUnit, "CNC"));
			attributes.add(new LISAAttribute(attrTypeName, "Finished"));
			type = new LISAVariableType(null, attributes);
			pending.add(type);
			Util.log("INFO: Registering LISAVariableType \"Finished\".");
			endpoint.registerLISAVariableType(type);
			
			attributes = new HashSet<>();
			attributes.add(new LISAAttribute(attrTypeEnterprise, "LISA"));
			attributes.add(new LISAAttribute(attrTypeSite, "Chalmers"));
			attributes.add(new LISAAttribute(attrTypeArea, "PSL"));
			attributes.add(new LISAAttribute(attrTypeLine, "Conveyor"));
			attributes.add(new LISAAttribute(attrTypeName, "ProductID"));
			type = new LISAVariableType(null, attributes);
			pending.add(type);
			Util.log("INFO: Querying LISAVariableType \"ProductID\".");
			endpoint.queryLISAVariableTypes(type);
		}
	}
	
	@Override
	public void receive(LISAMessage message, LISAMessageInfo info) {
		synchronized (messages) {
			messages.add(message);
			messages.notifyAll();
		}
	}
	
	@Override
	public void run() {
		while (!Thread.currentThread().isInterrupted()) {
			// Consume message
			synchronized (messages) {
				if (!messages.isEmpty()) {
					consumeLISAMessage(messages.get(0));
					messages.remove(0);
				}
			}
			
			// Wait for messages
			synchronized (messages) {
				if (messages.isEmpty()) {
					try {
						messages.wait();
					} catch (InterruptedException e) {
					}
				}
			}
		}
		Util.log("CNCEndpoint terminated");
	}
	
	private void consumeLISAMessage(LISAMessage message) {
		try {
			if (message.getTopic().equals("LEVEL0.EventNotification")) {
				boolean isProductIDMessage = false;
				for (LISAVariable var : message.getVariables()) {
					if (var.getType().getId().equals(name2id.get("ProductID"))) {
						isProductIDMessage = true;
						Integer productID = var.getInteger();
						if (productID != null) {
							Util.log("INFO: Product " + productID + ": Started");
							endpoint.send(new LISAMessage("LEVEL0.EventNotification", Arrays.asList(new LISAVariable[]{new LISAVariable(new Integer(productID), id2varType.get(name2id.get("Started")))})), null);
							try {
								Thread.sleep(4000);
							} catch (InterruptedException e) {
							}
							Util.log("INFO: Product " + productID + ": Finished");
							endpoint.send(new LISAMessage("LEVEL0.EventNotification", Arrays.asList(new LISAVariable[]{new LISAVariable(new Integer(productID), id2varType.get(name2id.get("Finished")))})), null);
						} else {
							Util.log("ERROR: ProductID is not an Integer.");
						}
					}
				}
				if (!isProductIDMessage) {
					Util.log("INFO: Not a ProductID message, ignoring.");
				}
			} else {
				Util.log("DEBUG: Ignored topic: " + message.getTopic());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void receive(LISATypeMessage typeMessage, LISAMessageInfo info) {
		if (info != null) {
			if (!(info.getSender().equals(endpoint.getClientID()))) {
				if (typeMessage.getVariableTypes().size() == 1) {
					LISAVariableType msgVarType = typeMessage.getVariableTypes().get(0);
					boolean foundVarType = false;
					synchronized (pending) {
						for (LISAVariableType varType : pending) {
							if (msgVarType.getAttributes().containsAll(varType.getAttributes())) {
								foundVarType = true;
								String name = msgVarType.getName();
								UUID id = msgVarType.getId();
								if (id != null) {
									name2id.put(name, id);
									id2varType.put(id, msgVarType);
									Util.log("INFO: LISAVariableType response (Name, UUID) = (" + name + ", " + id + ")");
								} else {
									Util.log("ERROR: Got LISAVariableType without ID, the requested variable does not exist.");
								}
								pending.remove(varType);
								pending.notifyAll();
								break;
							}
						}
					}
					if (!foundVarType) {
						Util.log("ERROR: Non-pending LISAVariableType.");
					}
				} else if (typeMessage.getVariableTypes().size() == 0) {
					Util.log("ERROR: No matching LISAVariableType.");
				} else if (typeMessage.getVariableTypes().size() > 1) {
					Util.log("ERROR: " + typeMessage.getVariableTypes().size() + " matching LISAVariableTypes.");
				}
			} else {
				Util.log("DEBUG: Local LISATypeMessage, ignored.");
			}
		} else {
			Util.log("ERROR: LISAMessageInfo is null, ignored.");
		}
	}

	@Override
	public void receive(LISAAliveMessage aliveMessage, LISAMessageInfo info) {
		Util.log("DEBUG: LISAAliveMessage received.");
	}
}
