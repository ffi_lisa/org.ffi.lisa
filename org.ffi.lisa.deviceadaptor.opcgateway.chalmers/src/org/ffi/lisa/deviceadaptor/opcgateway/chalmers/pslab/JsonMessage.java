package org.ffi.lisa.deviceadaptor.opcgateway.chalmers.pslab;

import java.util.HashMap;

public class JsonMessage {
	private JsonMessageType messType;
	private String id;
	private HashMap<String, Object> mess;

	JsonMessage() {
	}

	protected JsonMessage(JsonMessageType mt, String id,
			HashMap<String, Object> mess) {
		this.messType = mt;
		this.id = id;
		this.mess = mess;
	}

	public JsonMessageType getMessageType() {
		return this.messType;
	}
	
	public void setMessageType(JsonMessageType value) {
        this.messType = value;
    }

	public String getSenderID() {
		return this.id;
	}
	
	public void setSenderID(String value) {
        this.id = value;
    }

	public HashMap<String, Object> getMessage() {
		return this.mess;
	}

	public void setMessage(HashMap<String, Object> value) {
		this.mess = value;
	}
}

enum JsonMessageType {
	REG, SUBSCRIBE, WRITE, VALUE, ERROR 
}
