package org.ffi.lisa.deviceadaptor.opcgateway.chalmers.pslab;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundMessageHandlerAdapter;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.Map.Entry;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.ffi.lisa.core.Util;
import org.ffi.lisa.core.endpoint.Endpoint;
import org.ffi.lisa.core.endpoint.EndpointFactory;
import org.ffi.lisa.core.endpoint.LISAMessageInfo;
import org.ffi.lisa.core.endpoint.LISAMessageReceiver;
import org.ffi.lisa.core.lisamessage.LISAAliveMessage;
import org.ffi.lisa.core.lisamessage.LISAAttribute;
import org.ffi.lisa.core.lisamessage.LISAAttributeType;
import org.ffi.lisa.core.lisamessage.LISAMessage;
import org.ffi.lisa.core.lisamessage.LISATypeMessage;
import org.ffi.lisa.core.lisamessage.LISAVariableType;
import org.ffi.lisa.core.lisamessage.LISAVariable;
import org.ffi.lisa.core.tcp.TCPClient;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

public class PLC_Endpoint extends ChannelInboundMessageHandlerAdapter<String> implements LISAMessageReceiver {
	private final Endpoint endpoint;
	private final TCPClient client_to_OPC;

	// Configuration variables
	private final LISAAttributeType attType_Enterprise = new LISAAttributeType("Enterprise", UUID.fromString("ff0ed2bb-6355-4a98-98b5-ffb58f60d776"));
	private final LISAAttributeType attType_Site = new LISAAttributeType("Site", UUID.fromString("47cb8652-7871-4698-bc9a-d6daab1b8659"));
	private final LISAAttributeType attType_Area = new LISAAttributeType("Area", UUID.fromString("918063ad-67b0-474b-9a34-895938e2180b"));
	private final LISAAttributeType attType_Line = new LISAAttributeType("Line", UUID.fromString("286fc9e1-9f25-42d5-be08-426f56cea74e"));
	private final LISAAttributeType attType_Unit = new LISAAttributeType("Unit", UUID.fromString("2cad9f1a-376e-4984-a530-42978562d2e2"));
	private final LISAAttributeType attType_Name = new LISAAttributeType("Name", UUID.fromString("34902ed8-1ea1-4e25-96e4-45b689793392"));

	// Internal variables
	static HashMap<Integer,OPCVariableType> tmp_map_Int_OPCVariable = new HashMap<>();
	static HashMap<Integer,LISAVariableType> tmp_map_Int_LISAVariableType = new HashMap<>();
	static boolean running = false;


	static HashMap<UUID,OPCVariableType> map_UUID_OPCVariable = new HashMap<>();
	static HashMap<UUID,LISAVariableType> map_UUID_LISAVariable = new HashMap<>();
	static HashMap<String,UUID> map_Name_UUID_OPCVariable = new HashMap<>();

	static SimpleDateFormat dateFormater_OPC_to_ESB = new SimpleDateFormat(
			"MMM d, yyyy hh:mm:ss aaa", Locale.ENGLISH);

	private Gson gson = new Gson();

	public static void main(String[] args) throws InterruptedException, InstantiationException, IllegalAccessException, URISyntaxException {
		new PLC_Endpoint();
	}

	public PLC_Endpoint() throws InterruptedException, InstantiationException, IllegalAccessException, URISyntaxException {
		Util.log("INFO: Declaring and starting endpoints ..");
		endpoint = EndpointFactory.createEndpoint(new File("data/config_PLC_Endpoint.txt"), this);

		client_to_OPC = new TCPClient(new URI("http://129.16.63.35:7070"), "client_to_OPC", this, null);
		//client_to_OPC = new TCPClient(new URI("http://129.16.37.136:7070"), "client_to_OPC", this, null);
		//client_to_OPC = new TCPClient(new URI("http://129.16.180.86:7070"), "client_to_OPC", this, null);


		Util.log("INFO: All threads have been declared!");

		new Thread(client_to_OPC).start();

		Util.log("INFO: All threads have been started!");

		while (!(endpoint.isRunning() && client_to_OPC.isRunning())) {
			Util.log("INFO: Waiting for all servers and clients to be ready...");
			Thread.sleep(200);
		}

		Thread.sleep(1000);

		Util.log("INFO: Configuring the OPC Gateway...");
//		configure_PLC_Variables("data/PLC_Variables(few).json");
		configure_PLC_Variables("data/PLC_Variables.json");
		Util.log("INFO: OPC Gateway configured!");



		Util.log("INFO: Registering and subscribing to the ESB...");
		register_ESB();

		while (map_UUID_OPCVariable.size() < tmp_map_Int_LISAVariableType.size()) {
			Thread.sleep(100);
			Util.log("INFO: Waiting for all LISAVariableType to be registered! - " + map_UUID_OPCVariable.size() + " out of " + tmp_map_Int_LISAVariableType.size());

//			Util.log("DEBUG: nb_registrations: " + map_UUID_OPCVariable.size());
//			Util.log("DEBUG: nb_subscriptions: " + tmp_map_Int_LISAVariableType.size());
//			Util.log("DEBUG: map_UUID_OPCVariable.size(): " + map_UUID_OPCVariable.size());
//			Util.log("DEBUG: map_UUID_LISAVariable.size(): " + map_UUID_LISAVariable.size());
//			Util.log("DEBUG: map_Name_UUID_OPCVariable.size(): " + map_Name_UUID_OPCVariable.size());

		}
		// Clear temporary HashMaps
		tmp_map_Int_LISAVariableType.clear();
		tmp_map_Int_OPCVariable.clear();


		Util.log("INFO: Registering and subscribing to the OPC Gateway...");
		register_OPCGateway();
		Util.log("INFO: OPC Gateway registered!");
		Util.log("INFO: Subscription to " + tmp_map_Int_LISAVariableType.size() + " OPC variables done!");

		running = true;

		Util.log("DEBUG: map_UUID_OPCVariable.size(): " + map_UUID_OPCVariable.size());
		Util.log("DEBUG: map_UUID_LISAVariable.size(): " + map_UUID_LISAVariable.size());
		Util.log("DEBUG: map_Name_UUID_OPCVariable.size(): " + map_Name_UUID_OPCVariable.size());

		while(true) {
			Thread.sleep(100);
		}
	}

	// Override ChannelInboundMessageHandlerAdapter<String>
	@Override
	public void messageReceived(ChannelHandlerContext ctx, String message)
			throws Exception {
		Util.log("DEBUG: TCP message received: " + message);
		// In this case, we are supposed to received messages only from "client_to_OPC"
		if (ctx.channel().equals(client_to_OPC.channel)) {
			Util.log("DEBUG: Client_to_OPC got message: " + message);
			LISAMessage lisaMessage = translate_OPC_to_LISA(message);

			HashMap<String, String> map = new HashMap<>();
			LISAMessageInfo messageInfo = new LISAMessageInfo(map);
			endpoint.send(lisaMessage, messageInfo);

		} else {
			Util.log("ERROR: Cannot handle this case!");
		}
	}

	// Override LISAMessageReceiver
	@Override
	public void receive(LISAMessage message, LISAMessageInfo info) {
		Util.log("DEBUG: LISAMessage received");
//		Util.log("DEBUG: message.getVariables().size(): " + message.getVariables().size());

		for (LISAVariable var : message.getVariables()){
			String sVal = var.getString();
			if (sVal != null) {
				Util.log("\tString  : " + sVal);
			}

			Boolean bVal = var.getBoolean();
			if (bVal != null) {
				Util.log("\tBoolean : " + bVal);
			}

			Integer iVal = var.getInteger();
			if (iVal != null) {
				Util.log("\tInteger : " + iVal);
			}

			Double dVal = var.getDouble();
			if (dVal != null) {
				Util.log("\tDouble  : " + dVal);
			}
			for (LISAAttribute att : var.getAttributes()) {
				Util.log("\t\tAttribute : " + att.getType().getName() + " = " + att.getValue()) ;
			}
		}

		// In this case, we are supposed to received messages from topic
		// "LEVEL1.ControlRequest"
		if (message.getTopic().equalsIgnoreCase("LEVEL1.ControlRequest")) {
			Util.log("INFO: LISAMessage received from \"LEVEL1.ControlRequest\"");
			// TODO: Implement WRITE access to variable: An OPC Variable could be accessible only on READ or WRITE access
			String opcMessage = translate_LISA_2_OPC(message, JsonMessageType.WRITE);
			Util.sendTCPStringMessage(client_to_OPC.channel, opcMessage);
		} else if (message.getTopic().equalsIgnoreCase("LEVEL0.EventNotification")) {
			Util.log("INFO: LISAMessage received from \"LEVEL0.EventNotification\"");
		} else {
			Util.log("ERROR: LISAMessage received from another topic than \"LEVEL1.ControlRequest\" or \"LEVEL0.EventNotification\" ");
		}
	}

	@Override
	public void receive(LISATypeMessage typeMessage, LISAMessageInfo info) {
		Util.log("DEBUG: LISATypeMessage received on topic " + (info == null ? null : info.getTopic()) + "\n " +
				"\tTopic: " + (info == null ? null : info.getTopic()) + "\n" +
				"\tReplyTo: " + (info == null ? null : info.getReplyTo()) + "\n" +
				"\tReceiver: " + (info == null ? null : info.getReceiver()) + "\n" +
				"\tSender: " + (info == null ? null : info.getSender()) + "\n" +
				"\tInfos: " + (info == null ? null : info.getInfos()));

		if (info != null) {
			if (!(info.getSender().equals(endpoint.getClientID()))) {
				Util.log("INFO: LISAVariableType received on topic " + info.getTopic());

				List<LISAVariableType> reqVarTypes = typeMessage.getVariableTypes();
				if (reqVarTypes.size() == 1) {
					Util.log("\tUUID : " + (reqVarTypes.get(0).getId() == null ? null : reqVarTypes.get(0).getId().toString()));
					
					Set<LISAAttribute> reqAtts = reqVarTypes.get(0).getAttributes();

					for (Entry<Integer, LISAVariableType> tmp_entry : tmp_map_Int_LISAVariableType.entrySet()) {
						Integer tmp_key = tmp_entry.getKey();
						LISAVariableType tmp_storedVarType = tmp_entry.getValue();

						HashSet<LISAAttribute> resultAtts =  new HashSet<>();

						for (LISAAttribute reqAtt : reqAtts) {
							LISAAttribute storedAtt = tmp_storedVarType.getAttributes(reqAtt.getType());
							if (storedAtt != null) {
								if (storedAtt.getValue().equals(reqAtt.getValue())) {
									resultAtts.add(reqAtt);
								}
							}
						}
						if (resultAtts.size() == reqAtts.size()) { // Match found
							UUID uuid = typeMessage.getVariableTypes().get(0).getId();
							LISAVariableType lisaVar = new LISAVariableType(uuid, resultAtts);

							OPCVariableType opcVar = tmp_map_Int_OPCVariable.get(tmp_key);
							map_UUID_LISAVariable.put(uuid, lisaVar);
							OPCVariableType new_opcVar = new OPCVariableType(opcVar.getOPCType(), opcVar.getESBType(), opcVar.getName(), uuid, opcVar.isRegistered());
							map_UUID_OPCVariable.put(uuid, new_opcVar);
							map_Name_UUID_OPCVariable.put(new_opcVar.getName(), uuid);
						}
					}
				}
			} else {
				Util.log("DEBUG: Local LISATypeMessage received on topic " + info.getTopic());
			}
		} else {
			Util.log("DEBUG: MessageInfo is empty ");
		}

	}

	@Override
	public void receive(LISAAliveMessage aliveMessage, LISAMessageInfo info) {
		// TODO receive(LISAAliveMessage aliveMessage, LISAMessageInfo info)

	}

	private void configure_PLC_Variables(String filename) {
		List<LISAVariableType> varTypes = new ArrayList<>();

		// Read the config file
		String content = Util.readFile(new File(filename));

		@SuppressWarnings("unchecked")
		List<Map<String, Object>> config = gson.fromJson(content, List.class);
		int count = 0;
		for (Map<String, Object> map : config) {
			HashSet<LISAAttribute> attributes = new HashSet<>();
			String opcName = null;
			OPCVariableValueType opcType = null;
			OPCVariableValueType esbType = null;

			count +=1;
			boolean toBeRegistered = false;
			for (String key : map.keySet()) {
				LISAAttribute attribute;

				if (key.equalsIgnoreCase(attType_Enterprise.getName())) {
					attribute = new LISAAttribute(attType_Enterprise, (String)map.get(key));
				} else if (key.equalsIgnoreCase(attType_Site.getName())) {
					attribute = new LISAAttribute(attType_Site, (String)map.get(key));
				} else if (key.equalsIgnoreCase(attType_Area.getName())) {
					attribute = new LISAAttribute(attType_Area, (String)map.get(key));
				} else if (key.equalsIgnoreCase(attType_Line.getName())) {
					attribute = new LISAAttribute(attType_Line, (String)map.get(key));
				} else if (key.equalsIgnoreCase(attType_Unit.getName())) {
					attribute = new LISAAttribute(attType_Unit, (String)map.get(key));
				} else if (key.equalsIgnoreCase(attType_Name.getName())) {
					attribute = new LISAAttribute(attType_Name, (String)map.get(key));
				} else if (key.equalsIgnoreCase("isRegistered")) {
					toBeRegistered = (((Double)map.get(key) >= 1) ? true : false);
					attribute = null;
				} else if (key.equalsIgnoreCase("OPC Name")) {
					opcName = (String)map.get(key);
					attribute = null;
				} else if (key.equalsIgnoreCase("OPC Type")) {
					opcType = OPCVariableType_from_String((String)map.get(key));
					attribute = null;
				} else if (key.equalsIgnoreCase("ESB Type")) {
					esbType = OPCVariableType_from_String((String)map.get(key));
					attribute = null;
				} else {
					attribute = null;
				}

				if (attribute != null) {
					attributes.add(attribute);
				}
			}

			if (toBeRegistered && (attributes.size() > 0)) {
				OPCVariableType opcVarType = new OPCVariableType(opcType, esbType, opcName, null, toBeRegistered);
				LISAVariableType lisaVarType = new LISAVariableType(null, attributes);
				tmp_map_Int_LISAVariableType.put(count, lisaVarType);
				tmp_map_Int_OPCVariable.put(count, opcVarType);

				varTypes.add(lisaVarType);
			}

			toBeRegistered = false;
		}

		if (tmp_map_Int_LISAVariableType.size() == tmp_map_Int_OPCVariable.size()) {
			Util.log("INFO: " + tmp_map_Int_LISAVariableType.size() + " OPC and ESB variables declared");
		} else {
			Util.log("ERROR: " + tmp_map_Int_OPCVariable.size() + " OPC variables declared | " + tmp_map_Int_LISAVariableType.size() + " LISA variables declared" );
		}
	}

	void register_ESB() {
		List<LISAVariableType> varTypes = new ArrayList<>();
		for (LISAVariableType varType : tmp_map_Int_LISAVariableType.values()) {
			varTypes.add(varType);
		}
		LISATypeMessage typeMessage = new LISATypeMessage(varTypes);

		HashMap<String, String> map = new HashMap<>();
		map.put("ReplyTo", endpoint.getClientID());
		map.put("Sender", endpoint.getClientID());
		LISAMessageInfo messageInfo = new LISAMessageInfo(map);
		Util.log("DEBUG: messageInfo sent: " + messageInfo.getInfos());

		endpoint.registerLISAVariableTypes(typeMessage, messageInfo);
	}

	void register_OPCGateway() {
		// First, register to the OPC Gateway
		Util.sendTCPStringMessage(client_to_OPC.channel, gson.toJson(new JsonMessage(JsonMessageType.REG, endpoint.getClientID(), null)));
		try {
			// TODO Kristofer: Do we need to wait before subscribing? (between register and subscribe)
			Thread.sleep(500);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}

		for (OPCVariableType elt : map_UUID_OPCVariable.values()) {
			if (elt.isRegistered()) {
				// // All-in-one
				// subscribeList.put(elt.getName(), "");
				// // END All-in-one

				// One-by-one
				HashMap<String, Object> subscribeList = new HashMap<String, Object>();
				subscribeList.put(elt.getName(), "");
				//				Util.log(gson.toJson(new JsonMessage(JsonMessageType.SUBSCRIBE, dataPointID, subscribeList)));
				Util.sendTCPStringMessage(this.client_to_OPC.channel, gson.toJson(new JsonMessage(JsonMessageType.SUBSCRIBE, endpoint.getClientID(), subscribeList)));
				// END One-by-one
			}
		}
		// // All-in-one
		// Util.log(gson.toJson(new JsonMessage(JsonMessageType.SUBSCRIBE, dataPointID, subscribeList)));
		// Util.sendTCPStringMessage(this.client_to_OPC.channel, gson.toJson(new JsonMessage(JsonMessageType.SUBSCRIBE, dataPointID, subscribeList)));
		// // END All-in-one
	}

	private LISAMessage translate_OPC_to_LISA(String message) {
		// LISAMessage
		String msg_topic;
		String msg_timestamp;

		// List LISAVariable
		ArrayList<LISAVariable> msg_variables = new ArrayList<>();

		boolean success=false;

		try {
			JsonMessage jsonMessage = gson.fromJson(message, JsonMessage.class);

			if (jsonMessage.getMessageType() == JsonMessageType.VALUE) {
				msg_topic = "LEVEL0.EventNotification";
				if (jsonMessage.getMessage().containsKey("DATE")) {
					msg_timestamp = timestamp_from_OPC(jsonMessage.getMessage().get("DATE").toString());
				} else {
					msg_timestamp = timestamp_from_Now();
				}

				for (Entry<String, Object> elt : jsonMessage.getMessage()
						.entrySet()) {
					if (!elt.getKey().equalsIgnoreCase("DATE")) {

						String opcVar_Name = elt.getKey();
						OPCVariableType opcVarType = map_UUID_OPCVariable.get(map_Name_UUID_OPCVariable.get(opcVar_Name));
						LISAVariableType old_lisaVarType = map_UUID_LISAVariable.get(opcVarType.getId());

						// TODO: Cast the type of elt.getValue to the expected type of the LISAVariable
						LISAVariable lisaVar = new LISAVariable(elt.getValue(), msg_timestamp, old_lisaVarType);

						if (lisaVar != null) {
							msg_variables.add(lisaVar);

							// Debug
							for (LISAVariable var : msg_variables){
								String sVal = var.getString();
								if (sVal != null) {
									Util.log("new String  : " + sVal);
								}

								Boolean bVal = var.getBoolean();
								if (bVal != null) {
									Util.log("new Boolean : " + bVal);
								}

								Integer iVal = var.getInteger();
								if (iVal != null) {
									Util.log("new Integer : " + iVal);
								}

								Double dVal = var.getDouble();
								if (dVal != null) {
									Util.log("new Double  : " + dVal);
								}
							}
							// END Debug

							success = true;
//							Util.log("DEBUG: success: " + success);
						}
					}
				}
				if (success) {
					LISAMessage lisaMessage = new LISAMessage(msg_topic, msg_variables);
					return lisaMessage;
				}
			} else if (jsonMessage.getMessageType() == JsonMessageType.REG) {
				// TODO
				Util.log("ERROR: Cannot handle this case (yet)! - JsonMessageType.REG");
			} else if (jsonMessage.getMessageType() == JsonMessageType.ERROR) {
				// TODO
				Util.log("ERROR: Cannot handle this case (yet)! - JsonMessageType.ERROR");
			} else {
				Util.log("ERROR: Cannot handle this case!");
			}
		} catch (JsonSyntaxException e1) {
			Util.log("ERROR: translate_OPC_to_LISA: " + e1.getMessage());
		} catch (Exception e) {
			Util.log("ERROR: " + e.getMessage());
			e.printStackTrace();
		}

		return null;
	}

	private String translate_LISA_2_OPC(LISAMessage message, JsonMessageType jsonMessageType) {
		String outputOPCString = null;
		JsonMessage jsonMessage = new JsonMessage();

		jsonMessage.setMessageType(jsonMessageType);
		jsonMessage.setSenderID(endpoint.getClientName() + endpoint.getClientID());

		if (message.getTopic().equalsIgnoreCase("LEVEL1.ControlRequest") && jsonMessageType.equals(JsonMessageType.WRITE)) { // Right now this is the only case we can handle
			for (LISAVariable variable : message.getVariables()) {
				UUID uuid = variable.getId();

				if (map_UUID_OPCVariable.containsKey(uuid)){
					OPCVariableType opcVar = map_UUID_OPCVariable.get(uuid);

					Util.log("INFO: OPC variable found!");

					HashMap<String, Object> tmp_mess = new HashMap<String, Object>();
					if (variable.getBoolean() != null) {
						tmp_mess.put(opcVar.getName(), variable.getBoolean());
					} else if (variable.getDouble() != null) {
						tmp_mess.put(opcVar.getName(), variable.getDouble());
					} else if (variable.getInteger() != null) {
						tmp_mess.put(opcVar.getName(), variable.getInteger());
					} else if (variable.getString() != null) {
						tmp_mess.put(opcVar.getName(), variable.getString());
					} else {
						Util.log("ERROR: (@translate_LISA_2_OPC) - Cannot retrieve the variable's value");
					}

					if (tmp_mess.size()>0){
						jsonMessage.setMessage(tmp_mess);

						outputOPCString = gson.toJson(jsonMessage);
						Util.log("INFO: (@translate_LISA_2_OPC) - outputString: " + outputOPCString);
					}
				}
			}
		}

		return outputOPCString;
	}

	private OPCVariableValueType OPCVariableType_from_String(String string) {
		OPCVariableValueType opcVariableType = null;

		for (OPCVariableValueType elt : OPCVariableValueType.values()) {
			if (string.equalsIgnoreCase(elt.name())) {
				opcVariableType = elt;
			}
		}
		if (opcVariableType == null) {
			Util.log("ERROR: OPC variable type not found for: " + string);
		}
		return opcVariableType;
	}

	private static String timestamp_from_OPC(String inputString) {
		String outputString = null;

		XMLGregorianCalendar xMLCalendar = null;
		try {
			GregorianCalendar tmp_gregCalendar = new GregorianCalendar();
			tmp_gregCalendar.setTime(dateFormater_OPC_to_ESB.parse(inputString));
			xMLCalendar = DatatypeFactory.newInstance()
					.newXMLGregorianCalendar(tmp_gregCalendar);
			outputString = xMLCalendar.toString();
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
		}

		// Debug
		Util.log("DEBUG: (@timestamp_from_OPC) in: " + inputString);
		Util.log("DEBUG: (@timestamp_from_OPC) out: " + outputString);
		// END Debug

		return outputString;
	}

	private static String timestamp_from_Now() {
		String outputString = null;

		XMLGregorianCalendar xMLCalendar = null;
		try {
			GregorianCalendar tmp_gregCalendar = new GregorianCalendar();
			tmp_gregCalendar.setTime(new Date());
			xMLCalendar = DatatypeFactory.newInstance()
					.newXMLGregorianCalendar(tmp_gregCalendar);
			outputString = xMLCalendar.toString();
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
		}

		return outputString;
	}
}
