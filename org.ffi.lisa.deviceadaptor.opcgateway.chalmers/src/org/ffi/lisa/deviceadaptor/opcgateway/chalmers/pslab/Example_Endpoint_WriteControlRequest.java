package org.ffi.lisa.deviceadaptor.opcgateway.chalmers.pslab;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

import org.ffi.lisa.core.Util;
import org.ffi.lisa.core.endpoint.Endpoint;
import org.ffi.lisa.core.endpoint.EndpointFactory;
import org.ffi.lisa.core.endpoint.LISAMessageReceiver;
import org.ffi.lisa.core.endpoint.LISAMessageInfo;
import org.ffi.lisa.core.lisamessage.LISAAliveMessage;
import org.ffi.lisa.core.lisamessage.LISAAttribute;
import org.ffi.lisa.core.lisamessage.LISAAttributeType;
import org.ffi.lisa.core.lisamessage.LISAMessage;
import org.ffi.lisa.core.lisamessage.LISATypeMessage;
import org.ffi.lisa.core.lisamessage.LISAVariable;
import org.ffi.lisa.core.lisamessage.LISAVariableType;

public class Example_Endpoint_WriteControlRequest implements LISAMessageReceiver {
	private final Endpoint endpoint;
	
	public static void main(String[] args) throws InterruptedException {
		new Example_Endpoint_WriteControlRequest();
	}
	
	public Example_Endpoint_WriteControlRequest() throws InterruptedException {
		endpoint = EndpointFactory.createEndpoint(new File("data/config_Example_WriteControlRequest.txt"), this);
		
		Thread.sleep(2000);
		
		String topic = "LEVEL1.ControlRequest";
		
		LISAAttributeType attType_Enterprise = new LISAAttributeType("Enterprise", UUID.fromString("ff0ed2bb-6355-4a98-98b5-ffb58f60d776"));
		LISAAttributeType attType_Site = new LISAAttributeType("Site", UUID.fromString("47cb8652-7871-4698-bc9a-d6daab1b8659"));
		LISAAttributeType attType_Area = new LISAAttributeType("Area", UUID.fromString("918063ad-67b0-474b-9a34-895938e2180b"));
		LISAAttributeType attType_Line = new LISAAttributeType("Line", UUID.fromString("286fc9e1-9f25-42d5-be08-426f56cea74e"));
		LISAAttributeType attType_Unit = new LISAAttributeType("Unit", UUID.fromString("2cad9f1a-376e-4984-a530-42978562d2e2"));
		LISAAttributeType attType_Name = new LISAAttributeType("Name", UUID.fromString("34902ed8-1ea1-4e25-96e4-45b689793392"));

		HashSet<LISAAttribute> attributes = new HashSet<>();

		LISAAttribute attEnterprise = new LISAAttribute(attType_Enterprise,"LISA");
		LISAAttribute attSite = new LISAAttribute(attType_Site,"Chalmers");
		LISAAttribute attArea = new LISAAttribute(attType_Area,"PSL");
		LISAAttribute attLine = new LISAAttribute(attType_Line,"Conveyor");
		LISAAttribute attUnit = new LISAAttribute(attType_Unit,"Station1");
//		LISAAttribute attName = new LISAAttribute(attType_Name,"IL_PB_Green");
		LISAAttribute attName = new LISAAttribute(attType_Name,"StopperOut");
		
		attributes.add(attEnterprise);
		attributes.add(attSite);
		attributes.add(attArea);
		attributes.add(attLine);
		attributes.add(attUnit);
		attributes.add(attName);
		
		LISAVariableType varTypeQuery = new LISAVariableType(null, attributes);
		List<LISAVariableType> listVarTypeQuery = new ArrayList<>();
		listVarTypeQuery.add(varTypeQuery);
		
		LISATypeMessage typeMessage = new LISATypeMessage(listVarTypeQuery);
		
		endpoint.queryLISAVariableTypes(typeMessage, null);
		
		UUID idVarType = null;
		System.out.println("Enter UUID here : ");
		try{
		    BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
		    String s = bufferRead.readLine();
		    idVarType = UUID.fromString(s);
	 
		    System.out.println(s);
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}		
		
		LISAVariableType varType = new LISAVariableType(idVarType, attributes);
		
		
		
		while (true) {
			// Send Value 1
			LISAVariable lisaVar1 = new LISAVariable(new Boolean(false), varType);	
			
			List<LISAVariable> lisaVars1 = new ArrayList<>();
			//LISAVariableBuilder builder = new LISAVariableBuilder(); // TODO
			lisaVars1.add(lisaVar1);			
			
			LISAMessage message1 = new LISAMessage(topic, lisaVars1);
			
			endpoint.send(message1, null);
			
			Util.log("Waiting 2 seconds");
			Thread.sleep(2000);
			
			// Send Value 2
			LISAVariable lisaVar2 = new LISAVariable(new Boolean(true), varType);	
			
			List<LISAVariable> lisaVars2 = new ArrayList<>();
			//LISAVariableBuilder builder = new LISAVariableBuilder(); // TODO
			lisaVars2.add(lisaVar2);			
			
			LISAMessage message2 = new LISAMessage(topic, lisaVars2);
			
			endpoint.send(message2, null);
			
			Util.log("Waiting 5 seconds");
			Thread.sleep(5000);
		}
	}

	@Override
	public void receive(LISAMessage message, LISAMessageInfo info) {
		Util.log("LISAMessage received on topic " + (info == null ? null : info.getTopic()) + 
				"\tTopic: " + (info == null ? null : info.getTopic()) + "\n" +
				"\tReplyTo: " + (info == null ? null : info.getReplyTo()) + "\n" +
				"\tReceiver: " + (info == null ? null : info.getReceiver()) + "\n" +
				"\tSender: " + (info == null ? null : info.getSender()) + "\n" +
				"\tInfos: " + (info == null ? null : info.getInfos()));
		
		if (info != null) {
			if (!(info.getSender().equals(endpoint.getClientID()))) {
				for (LISAVariable var : message.getVariables()){
					Util.log("\tUUID  : " + (var.getId() == null ? null : var.getId().toString()));
										
					String sVal = var.getString();
					if (sVal != null) {
						Util.log("\tString  : " + sVal);
					}

					Boolean bVal = var.getBoolean();
					if (bVal != null) {
						Util.log("\tBoolean : " + bVal);
					}

					Integer iVal = var.getInteger();
					if (iVal != null) {
						Util.log("\tInteger : " + iVal);
					}

					Double dVal = var.getDouble();
					if (dVal != null) {
						Util.log("\tDouble  : " + dVal);
					}

					for (LISAAttribute att : var.getAttributes()) {
						Util.log("\t\tAttribute : " + att.getType().getName() + " = " + att.getValue()) ;
					}
				}
			} else {
				Util.log("DEBUG: Local LISAMessage received:");
				
				for (LISAVariable var : message.getVariables()){
					Util.log("\tUUID  : " + (var.getId() == null ? null : var.getId().toString()));
										
					String sVal = var.getString();
					if (sVal != null) {
						Util.log("\tString  : " + sVal);
					}

					Boolean bVal = var.getBoolean();
					if (bVal != null) {
						Util.log("\tBoolean : " + bVal);
					}

					Integer iVal = var.getInteger();
					if (iVal != null) {
						Util.log("\tInteger : " + iVal);
					}

					Double dVal = var.getDouble();
					if (dVal != null) {
						Util.log("\tDouble  : " + dVal);
					}

					for (LISAAttribute att : var.getAttributes()) {
						Util.log("\t\tAttribute : " + att.getType().getName() + " = " + att.getValue()) ;
					}
				}
			}
		}
	}

	@Override
	public void receive(LISATypeMessage typeMessage, LISAMessageInfo info) {
		Util.log("LISATypeMessage received on topic " + (info == null ? null : info.getTopic()) + 
				"\tTopic: " + (info == null ? null : info.getTopic()) + "\n" +
				"\tReplyTo: " + (info == null ? null : info.getReplyTo()) + "\n" +
				"\tReceiver: " + (info == null ? null : info.getReceiver()) + "\n" +
				"\tSender: " + (info == null ? null : info.getSender()) + "\n" +
				"\tInfos: " + (info == null ? null : info.getInfos()));
		
		if (info != null) {
			if (!(info.getSender().equals(endpoint.getClientID()))) {
				for (LISAVariableType var : typeMessage.getVariableTypes()){
					Util.log("\tUUID : " + (var.getId() == null ? null : var.getId().toString()));

					for (LISAAttribute att : var.getAttributes()) {
						Util.log("\t\tAttribute : " + att.getType().getName() + " = " + att.getValue()) ;
					}
				}
			} else {
				Util.log("DEBUG: Local LISAMessage received.");
			}
		} else {
			Util.log("DEBUG: MessageInfo is empty ");
		}
	}

	@Override
	public void receive(LISAAliveMessage aliveMessage, LISAMessageInfo info) {
		Util.log("LISAAliveMessage received on topic " + (info == null ? null : info.getTopic()) + 
				"\tTopic: " + (info == null ? null : info.getTopic()) + "\n" +
				"\tReplyTo: " + (info == null ? null : info.getReplyTo()) + "\n" +
				"\tReceiver: " + (info == null ? null : info.getReceiver()) + "\n" +
				"\tSender: " + (info == null ? null : info.getSender()) + "\n" +
				"\tInfos: " + (info == null ? null : info.getInfos()));
	}
}
