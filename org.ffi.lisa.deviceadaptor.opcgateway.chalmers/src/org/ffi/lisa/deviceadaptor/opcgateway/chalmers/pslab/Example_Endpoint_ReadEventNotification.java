package org.ffi.lisa.deviceadaptor.opcgateway.chalmers.pslab;

import java.io.File;
import java.util.Set;

import org.ffi.lisa.core.Util;
import org.ffi.lisa.core.endpoint.Endpoint;
import org.ffi.lisa.core.endpoint.EndpointFactory;
import org.ffi.lisa.core.endpoint.LISAMessageReceiver;
import org.ffi.lisa.core.endpoint.LISAMessageInfo;
import org.ffi.lisa.core.lisamessage.LISAAliveMessage;
import org.ffi.lisa.core.lisamessage.LISAAttribute;
import org.ffi.lisa.core.lisamessage.LISAMessage;
import org.ffi.lisa.core.lisamessage.LISATypeMessage;
import org.ffi.lisa.core.lisamessage.LISAVariable;
import org.ffi.lisa.core.lisamessage.LISAVariableType;

public class Example_Endpoint_ReadEventNotification implements LISAMessageReceiver {
	private final Endpoint endpoint;
	
	public static void main(String[] args) {
		new Example_Endpoint_ReadEventNotification();
	}
	
	public Example_Endpoint_ReadEventNotification() {
		endpoint = EndpointFactory.createEndpoint(new File("data/config_Example_ReadEventNotification.txt"), this);
	}

	@Override
	public void receive(LISAMessage message, LISAMessageInfo info) {
		Util.log("LISAMessage received on topic " + (info == null ? null : info.getTopic()) + 
				"\tTopic: " + (info == null ? null : info.getTopic()) + "\n" +
				"\tReplyTo: " + (info == null ? null : info.getReplyTo()) + "\n" +
				"\tReceiver: " + (info == null ? null : info.getReceiver()) + "\n" +
				"\tSender: " + (info == null ? null : info.getSender()) + "\n" +
				"\tInfos: " + (info == null ? null : info.getInfos()));
		
		if (info != null) {
			if (!(info.getSender().equals(endpoint.getClientID()))) {
				for (LISAVariable var : message.getVariables()){
					Util.log("\tUUID : " + (var.getId() == null ? null : var.getId().toString()));
										
					String sVal = var.getString();
					if (sVal != null) {
						Util.log("\tString  : " + sVal);
					}

					Boolean bVal = var.getBoolean();
					if (bVal != null) {
						Util.log("\tBoolean : " + bVal);
					}

					Integer iVal = var.getInteger();
					if (iVal != null) {
						Util.log("\tInteger : " + iVal);
					}

					Double dVal = var.getDouble();
					if (dVal != null) {
						Util.log("\tDouble  : " + dVal);
					}

					for (LISAAttribute att : var.getAttributes()) {
						Util.log("\t\tAttribute : " + att.getType().getName() + " = " + att.getValue()) ;
					}
				}
			} else {
				Util.log("DEBUG: Local LISAMessage received.");
			}
		}
	}

	@Override
	public void receive(LISATypeMessage typeMessage, LISAMessageInfo info) {
		Util.log("LISATypeMessage received on topic " + (info == null ? null : info.getTopic()) + 
				"\tTopic: " + (info == null ? null : info.getTopic()) + "\n" +
				"\tReplyTo: " + (info == null ? null : info.getReplyTo()) + "\n" +
				"\tReceiver: " + (info == null ? null : info.getReceiver()) + "\n" +
				"\tSender: " + (info == null ? null : info.getSender()) + "\n" +
				"\tInfos: " + (info == null ? null : info.getInfos()));
		
		if (info != null) {
			if (!(info.getSender().equals(endpoint.getClientID()))) {
				Util.log("DEBUG: LISAVariableType received on topic " + info.getTopic());
				
				if (typeMessage.getVariableTypes().size() > 0) {
					for (LISAVariableType varType : typeMessage.getVariableTypes()) {
						Util.log("\tVariableType's Name|Id: " + varType.getName() + "|" + (varType.getId() == null ? null : varType.getId().toString()));
						Set<LISAAttribute> varTypeAtts = varType.getAttributes();
						for (LISAAttribute varTypeAtt : varTypeAtts) {
							Util.log("\t\tAttribute's Value|AttTypeName|AttTypeId: " + 
									varTypeAtt.getValue() + "|" +
									varTypeAtt.getType().getName() + "|" +
									varTypeAtt.getType().getId());
						}
						
					}
				}
			} else {
				Util.log("DEBUG: Local LISATypeMessage received.");
			}
		} else {
			Util.log("DEBUG: MessageInfo is empty ");
		}
	}

	@Override
	public void receive(LISAAliveMessage aliveMessage, LISAMessageInfo info) {
		Util.log("LISAAliveMessage received on topic " + (info == null ? null : info.getTopic()));
	}
}
