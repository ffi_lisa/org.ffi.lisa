package org.ffi.lisa.deviceadaptor.opcgateway.chalmers.pslab;

import java.util.UUID;

public class OPCVariableType {
	private OPCVariableValueType opcType;
	private OPCVariableValueType esbType;
	private String name;
	private UUID id;
	private boolean registered;

	public OPCVariableType(OPCVariableValueType opcType, OPCVariableValueType esbType, String name, UUID uuid) {
		this.opcType = opcType;
		this.esbType = esbType;
		this.name = name;
		this.id = uuid;
	}
	
	public OPCVariableType(OPCVariableValueType opcType, OPCVariableValueType esbType, String name, UUID uuid, boolean registered) {
		this.opcType = opcType;
		this.esbType = esbType;
		this.name = name;
		this.id = uuid;
		this.registered = registered;
	}

	public OPCVariableValueType getOPCType() {
		return this.opcType;
	}

	public OPCVariableValueType getESBType() {
		return this.esbType;
	}

	public String getName() {
		return this.name;
	}

	public UUID getId() {
		return id;
	}

	public boolean isRegistered() {
		return registered;
	}

	public void setRegistered(boolean registered) {
		this.registered = registered;
	}

	// set.contains calls internally the equals methods. So we have to override this method
	// set.add() calls internally the equals and hashCode methods. So we have to override these two methods
	@Override
	public boolean equals(Object obj) {
		// if the two objects are equal in reference, they are equal
		if (this == obj) {
			return true;
		} else if (obj instanceof OPCVariableType) {
			if (this.getId().equals(((OPCVariableType) obj).getId())){
				return true;
			} else {
				return false;
			}
		} else {
			return false;			
		}
	}
	
	// set.contains calls internally the equals methods. So we have to override this method
	// set.add() calls internally the equals and hashCode methods. So we have to override these two methods
	@Override
	public int hashCode(){
	    return Integer.parseInt(this.id.toString());
	}
}

enum OPCVariableValueType {
	List, Double, Float, Long, Int, Short, Byte, Boolean, String, NormalString, TokenString, Date, Datetime, Time, Duration, BigDecimal, BigInteger, Base16Binary, Base64Binary
}