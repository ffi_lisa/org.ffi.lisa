{
	"URL" : "tcp://129.16.80.197:61616",
	"ClientID" : "12321ad4-0f72-4a6d-ade2-450944def165",
	"ClientName" : "Example_Endpoint_WriteControlRequest",
	"Topics" : [
		{
			"Topic" : "LEVEL0.EventNotification",
			"Username" : "user2",
			"Password" : "pwd2",
			"ClientID" : "5af85067-ad09-4766-a86d-39007a5860ec",
			"ReadAccess" : false,
			"WriteAccess" : true,
			"AdminAccess" : false,
			"Durable" : true,
			"MessageTTL" : 0,
			"Type" : "ActiveMQ"
		},
		{
			"Topic" : "IDSERVICE.*",
			"Username" : "smx",
			"Password" : "smx",
			"ReadAccess" : true,
			"WriteAccess" : true,
			"AdminAccess" : false,
			"Durable" : true,
			"MessageTTL" : 0,
			"Type" : "ActiveMQ"
		}
	]
}
