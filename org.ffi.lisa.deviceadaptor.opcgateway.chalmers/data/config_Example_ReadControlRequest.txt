{
	"URL" : "tcp://129.16.80.197:61616",
	"ClientID" : "55725d0c-f2a0-4d76-9d93-445a7978d558",
	"ClientName" : "Example_Endpoint_ReadControlRequest",
	"Topics" : [
		{
			"Topic" : "LEVEL1.ControlRequest",
			"Username" : "user3",
			"Password" : "pwd3",
			"ClientID" : "68fbc9d6-6bb6-4efe-8082-26b951a0a8dd",
			"ReadAccess" : true,
			"WriteAccess" : false,
			"AdminAccess" : false,
			"Durable" : true,
			"MessageTTL" : 0,
			"Type" : "ActiveMQ"
		},
		{
			"Topic" : "IDSERVICE.*",
			"Username" : "smx",
			"Password" : "smx",
			"ReadAccess" : true,
			"WriteAccess" : true,
			"AdminAccess" : false,
			"Durable" : true,
			"MessageTTL" : 0,
			"Type" : "ActiveMQ"
		}
	]
}
