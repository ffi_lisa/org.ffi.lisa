package org.ffi.lisa.core.websocket;

import org.ffi.lisa.core.Util;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelOutboundMessageHandlerAdapter;

public class WSDefaultOutboundStringHandler extends ChannelOutboundMessageHandlerAdapter<String> {
	@Override
	public void flush(ChannelHandlerContext ctx, String msg) throws Exception {
		ctx.write(msg);
		Util.log("WS - String sent: " + msg);
	}
}
