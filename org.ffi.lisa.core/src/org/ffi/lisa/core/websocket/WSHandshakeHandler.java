/*
 * Copyright 2012 The Netty Project
 *
 * The Netty Project licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
//The MIT License
//
//Copyright (c) 2009 Carl Bystr�m
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

package org.ffi.lisa.core.websocket;

import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundMessageHandlerAdapter;
import io.netty.channel.ChannelPromise;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.websocketx.CloseWebSocketFrame;
import io.netty.handler.codec.http.websocketx.PongWebSocketFrame;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.handler.codec.http.websocketx.WebSocketClientHandshaker;
import io.netty.handler.codec.http.websocketx.WebSocketFrame;
import io.netty.util.CharsetUtil;

public class WSHandshakeHandler extends
		ChannelInboundMessageHandlerAdapter<Object> {

	private final WebSocketClientHandshaker handshaker;
	private ChannelPromise handshakeFuture;

	public WSHandshakeHandler(WebSocketClientHandshaker handshaker) {
		this.handshaker = handshaker;
	}

	public ChannelFuture handshakeFuture() {
		return handshakeFuture;
	}

	@Override
	public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
		handshakeFuture = ctx.newPromise();
	}

	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		handshaker.handshake(ctx.channel());
	}

	@Override
	public void channelInactive(ChannelHandlerContext ctx) throws Exception {
		// log("WebSocket Client disconnected!");
		ctx.nextInboundMessageBuffer().add("ChannelInactive");
	}

	@Override
	public void messageReceived(ChannelHandlerContext ctx, Object msg)
			throws Exception {

		Channel ch = ctx.channel();
		if (!handshaker.isHandshakeComplete()) {
			handshaker.finishHandshake(ch, (FullHttpResponse) msg);
			ctx.nextInboundMessageBuffer().add("WebSocket Client connected!");
			handshakeFuture.setSuccess();
			log("WebSocket handshake done!");
			return;
		}

		if (msg instanceof FullHttpResponse) {
			FullHttpResponse response = (FullHttpResponse) msg;
			throw new Exception("Unexpected FullHttpResponse (getStatus="
					+ response.getStatus() + ", content="
					+ response.content().toString(CharsetUtil.UTF_8) + ')');
		}

		// WebSocketFrame frame = (WebSocketFrame) msg;
		// if (frame instanceof TextWebSocketFrame) {
		// TextWebSocketFrame textFrame = (TextWebSocketFrame) frame;
		// log("WebSocket Client received message: "
		// + textFrame.text());
		// ctx.nextInboundMessageBuffer().add(frame);
		// } else if (frame instanceof PongWebSocketFrame) {
		// log("WebSocket Client received pong");
		// ctx.nextInboundMessageBuffer().add(frame);
		// } else if (frame instanceof CloseWebSocketFrame) {
		// log("WebSocket Client received closing");
		// ctx.nextInboundMessageBuffer().add(frame);
		// ch.close();
		// }

		WebSocketFrame frame = (WebSocketFrame) msg;
		if (frame instanceof TextWebSocketFrame) {
			TextWebSocketFrame textFrame = (TextWebSocketFrame) frame;
			ctx.nextInboundMessageBuffer().add(textFrame.text());
		} else if (frame instanceof PongWebSocketFrame) {
			ctx.nextInboundMessageBuffer().add("PongWebSocketFrame");
		} else if (frame instanceof CloseWebSocketFrame) {
			ctx.nextInboundMessageBuffer().add("CloseWebSocketFrame");
			ch.close();
		}

		return;
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
			throws Exception {
		cause.printStackTrace();

		if (!handshakeFuture.isDone()) {
			handshakeFuture.setFailure(cause);
		}

		ctx.close();
	}

	private static void log(String s) {
		System.out.println(s);
	}
}
