package org.ffi.lisa.core.websocket;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.http.DefaultHttpHeaders;
import io.netty.handler.codec.http.HttpClientCodec;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.handler.codec.http.websocketx.WebSocketClientHandshakerFactory;
import io.netty.handler.codec.http.websocketx.WebSocketVersion;

import java.io.IOException;
import java.net.URI;
import org.ffi.lisa.core.tcp.TCPDefaultInboundStringHandler;
import org.ffi.lisa.core.tcp.TCPDefaultOutboundStringHandler;

public class WSClient implements Runnable {
	private final URI uri;
	public String name;
	private String receiveHandlerClassName;
	private String sendHandlerClassName;

	public Channel channel;
	public Boolean started = false;

	static String dataPointID = "se.chalmers.pslab.deviceadaptor.webserver";

	public WSClient(URI uri, String name, String receiveHandlerClassName,
			String sendHandlerClassName) throws IOException {
		this.uri = uri;
		this.name = name;
		this.receiveHandlerClassName = receiveHandlerClassName;
		this.sendHandlerClassName = sendHandlerClassName;

		if (receiveHandlerClassName == null) {
			this.receiveHandlerClassName = TCPDefaultInboundStringHandler.class
					.getName();
		} else {
			this.receiveHandlerClassName = receiveHandlerClassName;
		}

		if (sendHandlerClassName == null) {
			this.sendHandlerClassName = TCPDefaultOutboundStringHandler.class
					.getName();
		} else {
			this.sendHandlerClassName = sendHandlerClassName;
		}
	}

	public void run() {
		EventLoopGroup group = new NioEventLoopGroup();
		try {
			Bootstrap b = new Bootstrap();
			String protocol = uri.getScheme();
			if (!"ws".equals(protocol)) {
				throw new IllegalArgumentException("Unsupported protocol: "
						+ protocol);
			}

			HttpHeaders customHeaders = new DefaultHttpHeaders();
			customHeaders.add("MyHeader", "MyValue");

			// Connect with V13 (RFC 6455 aka HyBi-17). You can change it to V08
			// or V00.
			// If you change it to V00, ping is not supported and remember to
			// change
			// HttpResponseDecoder to WebSocketHttpResponseDecoder in the
			// pipeline.
			final WSHandshakeHandler handshaker = new WSHandshakeHandler(
					WebSocketClientHandshakerFactory.newHandshaker(uri,
							WebSocketVersion.V13, null, false, customHeaders));

			b.group(group)
					.channel(NioSocketChannel.class)
					.handler(new ChannelInitializer<SocketChannel>() {
						@Override
						public void initChannel(SocketChannel ch)
								throws Exception {
							ChannelPipeline pipeline = ch.pipeline();
							pipeline.addLast("http-codec",
									new HttpClientCodec());
							pipeline.addLast("aggregator",
									new HttpObjectAggregator(65536));
							pipeline.addLast("handshaker", handshaker);
							pipeline.addLast(
									"receiveHandler",
									(ChannelHandler) Class.forName(
											receiveHandlerClassName)
											.newInstance());
							pipeline.addLast(
									"sendHandler",
									(ChannelHandler) Class.forName(
											sendHandlerClassName).newInstance());
						}
					}).option(ChannelOption.TCP_NODELAY, true)
					.option(ChannelOption.SO_KEEPALIVE, true);

			ChannelFuture future = b.connect(uri.getHost(), uri.getPort())
					.sync();
			handshaker.handshakeFuture().sync();
			channel = future.channel();
			started = true;
			log("WebSocket Client connected to: " + uri.toString());

			// TODO Probably move it to each instance of the WSclient.
			// Indeed, the identification message could vary depending on who
			// the client is communicating
			// Send the identification message
			sendIdentificationMessage();

			future.channel().closeFuture().sync();
		} catch (InterruptedException e) {
			log("Problem with Netty: " + e);
			// e.printStackTrace();
		} finally {
			group.shutdownGracefully();
		}
	}

	/**
	 * This function should be override(d) on the object instance. default
	 * message: "{name:" + name + ", type:JavaWSClient}
	 */
	void sendIdentificationMessage() {
		String outputString = null;
		outputString = "{\"messType\":\"REG\",\"id\":\"se.chalmers.pslab.deviceadaptor.webserver\",\"mess\":{name:"
				+ name + ", type:JavaWSClient}}";
		channel.write(new TextWebSocketFrame(outputString));
	}

	private static void log(String s) {
		System.out.println(s);
	}
}
