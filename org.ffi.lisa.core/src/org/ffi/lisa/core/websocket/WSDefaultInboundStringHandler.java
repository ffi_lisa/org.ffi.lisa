package org.ffi.lisa.core.websocket;

import org.ffi.lisa.core.Util;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundMessageHandlerAdapter;

public class WSDefaultInboundStringHandler extends ChannelInboundMessageHandlerAdapter<String> {
	@Override
	public void messageReceived(ChannelHandlerContext ctx, String msg) throws Exception {
		Util.log("WS - String received: " + msg);
	}

	// @Override
	// public void messageReceived(ChannelHandlerContext ctx, WebSocketFrame
	// frame)
	// throws Exception {
	// System.out.println("messageReceived - Second handler");
	//
	// Channel ch = ctx.channel();
	//
	// if (frame instanceof TextWebSocketFrame) {
	// TextWebSocketFrame textFrame = (TextWebSocketFrame) frame;
	// System.out
	// .println("WebSocket - text received: " + textFrame.text());
	// } else if (frame instanceof PongWebSocketFrame) {
	// System.out.println("WebSocket - pong received");
	// } else if (frame instanceof CloseWebSocketFrame) {
	// System.out.println("WebSocket - closing received");
	// ch.close();
	// }
	//
	// }
}
