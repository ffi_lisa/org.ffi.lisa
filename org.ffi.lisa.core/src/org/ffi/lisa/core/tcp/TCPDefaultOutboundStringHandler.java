package org.ffi.lisa.core.tcp;

import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelOutboundMessageHandlerAdapter;

@Sharable
public class TCPDefaultOutboundStringHandler extends ChannelOutboundMessageHandlerAdapter<String> {
	@Override
	public synchronized void flush(ChannelHandlerContext ctx, String msg) throws Exception {
		ctx.write(msg);
//		Util.log("DEBUG: String sent: " + msg);
	}
}
