package org.ffi.lisa.core.tcp;

import org.ffi.lisa.core.Util;

import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundMessageHandlerAdapter;

@Sharable
public class TCPDefaultInboundStringHandler extends ChannelInboundMessageHandlerAdapter<String> {

	@Override
	public synchronized void messageReceived(ChannelHandlerContext ctx, String msg) throws Exception {
		Util.log("DEBUG: String received: " + msg);
	}
}
