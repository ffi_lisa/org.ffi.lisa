package org.ffi.lisa.core.tcp;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;

import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.Delimiters;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.util.CharsetUtil;

import java.net.URI;

import org.ffi.lisa.core.Util;


public class TCPClient implements Runnable {
	private boolean running = false;
	private final URI uri;
	
	public String name;	
	public Object receiveHandler;
	public Object sendHandler;

	public Channel channel;	

	public TCPClient(
			URI uri,
			String name,
			Object receiveHandler,
			Object sendHandler) throws InstantiationException, IllegalAccessException {
		this.uri = uri;
		this.name = name;
		
		if (receiveHandler == null) {
			this.receiveHandler = TCPDefaultInboundStringHandler.class.newInstance();
		} else {
			this.receiveHandler = receiveHandler;
		}
		
		if (sendHandler == null) {
			this.sendHandler = TCPDefaultOutboundStringHandler.class.newInstance();
		} else {
			this.sendHandler = sendHandler;
		}
	}

	@Override
	public void run() {
		EventLoopGroup group = new NioEventLoopGroup();
		Bootstrap bootstrap = new Bootstrap();
		try {
			bootstrap
					.group(group)
					.channel(NioSocketChannel.class)
					.handler(new ChannelInitializer<SocketChannel>() {
						@Override
						public void initChannel(SocketChannel channel)
								throws Exception {
							ChannelPipeline pipeline = channel.pipeline();
							pipeline.addLast("frameDecoder",
									new DelimiterBasedFrameDecoder(65536,
											Delimiters.lineDelimiter()));
							pipeline.addLast("stringDecoder",
									new StringDecoder(CharsetUtil.UTF_8));
							pipeline.addLast("stringEncoder",
									new StringEncoder(CharsetUtil.UTF_8));
							pipeline.addLast("receiveHandler", (ChannelHandler) receiveHandler);
							pipeline.addLast("sendHandler", (ChannelHandler) sendHandler);
						}
					}).option(ChannelOption.TCP_NODELAY, true)
					.option(ChannelOption.SO_KEEPALIVE, true);

			ChannelFuture future = bootstrap.connect(uri.getHost(),
					uri.getPort()).sync();
			channel = future.channel();
			running = true;
			Util.log("INFO: Client started and connected to " + uri.getHost() + " on port " + uri.getPort());

			// TODO Probably move it to each instance of the TCPclient.
			// Indeed, the identification message could vary depending on who
			// the client is communicating
			// Send the identification message
			sendIdentificationMessage();

			future.channel().closeFuture().sync();
			Util.log("ERROR: Client " + uri.getHost() + ":" + uri.getPort() + " stopped");
		} catch (Exception e) {
			Util.log("ERROR: Problem with Netty: " + e);
			// e.printStackTrace();
		} finally {
			Util.log("DEBUG: finally");
			running = false;

			group.shutdownGracefully();
			// bootstrap.shutdown();
		}
	}

	/**
	 * This function should be override(d) on the object instance. default
	 * message: "{name:" + name + ", type:type:JavaTCPClient}
	 */
	private void sendIdentificationMessage() {
		channel.write(new TextWebSocketFrame("{name:" + name + ", type:JavaTCPClient}"));
	}

	public synchronized boolean isRunning() {
		return running;
	}

}
