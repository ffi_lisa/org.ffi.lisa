package org.ffi.lisa.core.tcp;


import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;

import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.Delimiters;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.util.CharsetUtil;

import java.net.InetSocketAddress;
import java.net.URI;

import org.ffi.lisa.core.Util;

public class TCPServer implements Runnable {
	private boolean running = false ;
	private final URI uri;
	
	public String name;	
	public Object receiveHandler;
	public Object sendHandler;	
	
	public Channel channel;
	public Channel childChannel;

	public TCPServer(URI uri, String name, Object receiveHandler,
			Object sendHandler) throws InstantiationException, IllegalAccessException {
		this.uri = uri;
		this.name = name;
		
		if (receiveHandler == null) {
			this.receiveHandler = TCPDefaultInboundStringHandler.class.newInstance();
		} else {
			this.receiveHandler = receiveHandler;
		}
		
		if (sendHandler == null) {
			this.sendHandler = TCPDefaultOutboundStringHandler.class.newInstance();
		} else {
			this.sendHandler = sendHandler;
		}
	}

	@Override
	public void run() {
		EventLoopGroup parentgroup = new NioEventLoopGroup();
		EventLoopGroup childgroup = new NioEventLoopGroup();
		ServerBootstrap serverbootstrap = new ServerBootstrap();
		try {
			serverbootstrap
					.group(parentgroup, childgroup)
					.channel(NioServerSocketChannel.class)
					.childHandler(new ChannelInitializer<SocketChannel>() {
						@Override
						public void initChannel(SocketChannel channel) {
							try {
								childChannel = channel;
								ChannelPipeline pipeline = channel.pipeline();
								pipeline.addLast("frameDecoder", new DelimiterBasedFrameDecoder(65536, Delimiters.lineDelimiter()));
								pipeline.addLast("stringDecoder", new StringDecoder(CharsetUtil.UTF_8));
								pipeline.addLast("stringEncoder", new StringEncoder(CharsetUtil.UTF_8));
								pipeline.addLast("receiveHandler", (ChannelHandler) receiveHandler);
								pipeline.addLast("sendHandler", (ChannelHandler) sendHandler);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}).childOption(ChannelOption.TCP_NODELAY, true)
					.childOption(ChannelOption.SO_KEEPALIVE, true);


			// If we want to bind to a specific IP address
			//ChannelFuture future = serverbootstrap.bind(uri.getHost(), uri.getPort()).sync();
			// If we want to bind to any address
			ChannelFuture future = serverbootstrap.bind(new InetSocketAddress(uri.getPort())).sync();			
			
			channel = future.channel();
			running = true;
			Util.log("INFO: Server started at " + uri.getHost() + " on port " + uri.getPort());

			future.channel().closeFuture().sync();
			Util.log("ERROR: Server " + uri.getHost() + ":" + uri.getPort() + " stopped");
		} catch (Exception e) {
			Util.log("ERROR: Problem with Netty: " + e);
			e.printStackTrace();
		} finally {
			Util.log("DEBUG: finally");
			running = false;
			
			parentgroup.shutdownGracefully();
			childgroup.shutdownGracefully();
		}
	}

	public synchronized boolean isRunning() {
		return running;
	}

}
