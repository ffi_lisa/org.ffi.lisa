package org.ffi.lisa.core.endpoint;

import java.io.Serializable;
import java.util.HashMap;

public class LISAMessageInfo implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private final HashMap<String,String> info;

	public LISAMessageInfo(HashMap<String,String> map) {
		this.info = map;
	}
	
	public HashMap<String,String> getInfos() {		
		return info;
	}

	public String getReceiver() {
		String ret = null;
		for (String key : info.keySet()){
			if (key.equalsIgnoreCase("Receiver")) {
				ret = info.get(key);
			}
		}
		return ret;
	}

	public String getReplyTo() {
		String ret = null;
		for (String key : info.keySet()){
			if (key.equalsIgnoreCase("ReplyTo")) {
				ret = info.get(key);
			}
		}
		return ret;
	}

	public String getRequestStatus() {
		String ret = null;
		for (String key : info.keySet()){
			if (key.equalsIgnoreCase("RequestStatus")) {
				ret = info.get(key);
			}
		}
		return ret;
	}

	public String getSendStatus() {
		String ret = null;
		for (String key : info.keySet()){
			if (key.equalsIgnoreCase("SendStatus")) {
				ret = info.get(key);
			}
		}
		return ret;
	}

	public String getRegisterStatus() {
		String ret = null;
		for (String key : info.keySet()){
			if (key.equalsIgnoreCase("RegisterStatus")) {
				ret = info.get(key);
			}
		}
		return ret;
	}

	public String getSender() {
		String ret = null;
		for (String key : info.keySet()){
			if (key.equalsIgnoreCase("Sender")) {
				ret = info.get(key);
			}
		}
		return ret;
	}
	
	public String getTopic() {
		String ret = null;
		for (String key : info.keySet()){
			if (key.equalsIgnoreCase("Topic")) {
				ret = info.get(key);
			}
		}
		return ret;
	}
	
}
