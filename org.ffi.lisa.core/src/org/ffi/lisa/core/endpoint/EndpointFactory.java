package org.ffi.lisa.core.endpoint;

import java.io.File;

public class EndpointFactory {
	public static EndpointExtended createEndpoint(File configFile, LISAMessageReceiver callback) {
		return new EndpointImpl(configFile, callback);
	}

	public static EndpointExtended createIDEndpoint(File configFile, LISAMessageReceiver callback) {
		return new IDEndpointImpl(configFile, callback);
	}
}
