package org.ffi.lisa.core.endpoint;

import org.ffi.lisa.core.Util;
import org.ffi.lisa.core.activemq.ActiveMQConsumer;
import org.ffi.lisa.core.activemq.ActiveMQProducer;
import org.ffi.lisa.core.lisamessage.LISAAliveMessage;
import org.ffi.lisa.core.lisamessage.LISAMessage;
import org.ffi.lisa.core.lisamessage.LISATypeMessage;

public class ActiveMQEndpoint implements Endpoint {
	private final ActiveMQConsumer consumer;
	private final ActiveMQProducer producer;

	private final String topic;

	private final String clientID;
	private final String clientName;
	
	private final boolean hasReadAccess;
	private final boolean hasWriteAccess;

	public ActiveMQEndpoint(
			String url,
			String user,
			String password,
			String clientID,
			String clientName,
			String topic,
			String selector,
			boolean durable,
			long messageTimeToLive,
			boolean hasReadAccess,
			boolean hasWriteAccess,
			boolean hasAdminAccess,
			LISAMessageReceiver callback) {
		this.topic = topic;
		this.clientID = clientID;
		this.clientName = clientName;
		this.hasReadAccess = hasReadAccess;
		this.hasWriteAccess = hasWriteAccess;

		if (hasReadAccess) {
			Util.log("DEBUG: Declaring and starting ActiveMQConsumer...");
			consumer = new ActiveMQConsumer(
					url,
					topic,
					user,
					password,
					clientName,
					clientID,
					durable,
					selector,
					hasAdminAccess,
					callback);
//			Util.log("DEBUG: ActiveMQConsumer has been declared!");
//			new Thread(consumer).start();
			Util.log("DEBUG: ActiveMQConsumer has been started!\n");
		} else {
			consumer = null;
		}

		if (hasWriteAccess) {
			Util.log("DEBUG: Declaring and starting ActiveMQProducer...");
			producer = new ActiveMQProducer(
					url,
					topic,
					user,
					password,
					clientName,
					clientID,
					messageTimeToLive,
					hasAdminAccess);
//			Util.log("ActiveMQProducer has been declared!");
//			new Thread(producer).start();
			Util.log("DEBUG: ActiveMQProducer has been started!\n");
		} else {
			producer = null;
		}
	}

	@Override
	public void send(LISAMessage message, LISAMessageInfo info) {
		if (producer != null) {
			producer.send(message, info);
//			Util.log("DEBUG: Sent LISAMessage on topic: " + topic);
		} else {
			Util.log("ERROR: Cannot send LISAMessage on topic: " + topic + ". \n\tActiveMQProducer is not defined for this topic");
		}
	}

	@Override
	public void queryLISAVariableTypes(LISATypeMessage typeMessage, LISAMessageInfo info) {
		if (producer != null) {
			producer.send(typeMessage, info);
//			Util.log("DEBUG: Queried LISAVariableTypes on topic: " + topic);
		} else {
			Util.log("ERROR: Cannot query LISAVariableTypes on topic: " + topic + ". \n\tActiveMQProducer is not defined for this topic");
		}
	}
	
	@Override
	public void registerLISAVariableTypes(LISATypeMessage typeMessage, LISAMessageInfo info) {
		if (producer != null) {
			producer.send(typeMessage, info);
//			Util.log("DEBUG: Registered LISAVariableTypes on topic: " + topic);
		} else {
			Util.log("ERROR: Cannot register LISAVariableTypes on topic: " + topic + ". \n\tActiveMQProducer is not defined for this topic");
		}
	}

	@Override
	public void alive(LISAAliveMessage aliveMessage, LISAMessageInfo info) {
		if (producer != null) {
			producer.send(aliveMessage, info);
			Util.log("DEBUG: Executed alive(LISAAliveMessage) on topic: " + topic);
		} else {
			Util.log("ERROR: Cannot execute alive(LISAAliveMessage) on topic: " + topic + ". \n\tActiveMQProducer is not defined for this topic");
		}
	}

	@Override
	public void keepAlive(LISAAliveMessage aliveMessage, LISAMessageInfo info) {
		if (producer != null) {
			producer.send(aliveMessage, info);
			Util.log("DEBUG: Executed keepAlive(LISAAliveMessage) on topic: " + topic);
		} else {
			Util.log("ERROR: Cannot execute keepAlive(LISAAliveMessage) on topic: " + topic + ". \n\tActiveMQProducer is not defined for this topic");
		}
	}

	@Override
	public String getClientID() {
		return clientID;
	}

	@Override
	public String getClientName() {
		return clientName;
	}

	@Override
	public synchronized boolean isRunning() {
		return (!hasReadAccess || producer.isRunning()) && (!hasWriteAccess || consumer.isRunning());
	}
}
