package org.ffi.lisa.core.endpoint;

import org.ffi.lisa.core.lisamessage.LISAVariableType;

public interface EndpointExtended extends Endpoint {
	public void registerLISAVariableType(LISAVariableType type);
	public void queryLISAVariableTypes(LISAVariableType type);
}
