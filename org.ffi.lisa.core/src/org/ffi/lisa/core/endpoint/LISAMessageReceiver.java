package org.ffi.lisa.core.endpoint;

import org.ffi.lisa.core.lisamessage.LISAAliveMessage;
import org.ffi.lisa.core.lisamessage.LISAMessage;
import org.ffi.lisa.core.lisamessage.LISATypeMessage;

public interface LISAMessageReceiver {
	public void receive(LISAMessage message, LISAMessageInfo info);
	public void receive(LISATypeMessage typeMessage, LISAMessageInfo info);
	public void receive(LISAAliveMessage aliveMessage, LISAMessageInfo info);
}
