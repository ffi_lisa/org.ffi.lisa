package org.ffi.lisa.core.endpoint;

import org.ffi.lisa.core.lisamessage.LISAAliveMessage;
import org.ffi.lisa.core.lisamessage.LISAMessage;
import org.ffi.lisa.core.lisamessage.LISATypeMessage;

public interface Endpoint {
	// Usual LISAMessage(s)
	public void send(LISAMessage message, LISAMessageInfo info); // TODO: Remove LISAMessageInfo?
	
	// Message(s) to the IDService
	public void registerLISAVariableTypes(LISATypeMessage typeMessage, LISAMessageInfo info); // TODO: Remove LISAMessageInfo?
	public void queryLISAVariableTypes(LISATypeMessage typeMessage, LISAMessageInfo info); // TODO: Remove LISAMessageInfo?
	
	public void alive(LISAAliveMessage aliveMessage, LISAMessageInfo info);
	public void keepAlive(LISAAliveMessage aliveMessage, LISAMessageInfo info);
	
	// Get endpoint parameters
	public String getClientID();
	public String getClientName();
	public boolean isRunning();
}
