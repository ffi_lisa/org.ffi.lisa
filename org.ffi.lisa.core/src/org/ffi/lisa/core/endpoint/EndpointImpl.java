package org.ffi.lisa.core.endpoint;

import java.io.File;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import org.ffi.lisa.core.Util;
import org.ffi.lisa.core.endpoint.EndpointConfig.Topic;
import org.ffi.lisa.core.lisamessage.LISAMessage;
import org.ffi.lisa.core.lisamessage.LISATypeMessage;
import org.ffi.lisa.core.lisamessage.LISAAliveMessage;
import org.ffi.lisa.core.lisamessage.LISAVariable;
import org.ffi.lisa.core.lisamessage.LISAVariableType;

import com.google.gson.Gson;

public class EndpointImpl implements EndpointExtended {
	// Variables as arguments
	private final LISAMessageReceiver parentCallback;

	// Final variables
	private final String idSserviceTopicPrefix = "IDSERVICE";
	private final String registerTopic = idSserviceTopicPrefix + "." + "Register";
	private final String requestTopic = idSserviceTopicPrefix + "." + "Request";
	private final String aliveTopic = idSserviceTopicPrefix + "." + "Alive";
	private final String keepAliveTopic = idSserviceTopicPrefix + "." + "KeepAlive";

	private final String clientID;
	private final String clientName;

	// Internal variables defined during initialization
	private HashMap<String, Endpoint> endpoints;

	private Endpoint registerEndpoint;
	private Endpoint requestEndpoint;
	private Endpoint aliveEndpoint;
	private Endpoint keepAliveEndpoint;

	public EndpointImpl(File configFile, LISAMessageReceiver callback) {
		this(configFile, callback, " AND (ReplyTo IS NULL)");
	}

	public EndpointImpl(File configFile, LISAMessageReceiver callback, String selectorSuffix) {
		this.endpoints = new HashMap<>();
		this.parentCallback = callback;

		Gson gson = new Gson();

		String content = Util.readFile(configFile);

		EndpointConfig config = gson.fromJson(content, EndpointConfig.class);
		String url = config.getURL();
		this.clientID = config.getClientID();
		this.clientName = config.getClientName();

		List<Topic> topics = config.getTopics();
		for (Topic topic : topics) {
			String type = topic.getType();
			if (type.equalsIgnoreCase("ActiveMQ")) {
				String topicText = topic.getTopic();
				boolean readAccess = topic.getReadAccess();
				boolean writeAccess = topic.getWriteAccess();
				boolean adminAccess = topic.getAdminAccess();
				if (topicText.toUpperCase().startsWith(idSserviceTopicPrefix)){
					registerEndpoint = new ActiveMQEndpoint(
							url,
							topic.getUsername(),
							topic.getPassword(),
							clientID + "|Register",
							clientName,
							registerTopic,
							"((Receiver IS NULL) OR (Receiver = '"+ clientID + "'))" + selectorSuffix,
							true,
							600000, // 600000 = 10 minutes TimeToLive
							readAccess,
							writeAccess,
							adminAccess,
							callback
							);

					requestEndpoint = new ActiveMQEndpoint(
							url,
							topic.getUsername(),
							topic.getPassword(),
							clientID + "|Request",
							clientName,
							requestTopic,
							"((Receiver IS NULL) OR (Receiver = '"+ clientID + "'))" + selectorSuffix,
							true,
							600000, // 600000 = 10 minutes TimeToLive
							readAccess,
							writeAccess,
							adminAccess,
							callback
							);

					aliveEndpoint = new ActiveMQEndpoint(
							url,
							topic.getUsername(),
							topic.getPassword(),
							clientID + "|Alive",
							clientName,
							aliveTopic,
							"(Receiver IS NULL) OR (Receiver = '"+ clientID + "')" + selectorSuffix,
							true,
							600000, // 600000 = 10 minutes TimeToLive
							readAccess,
							writeAccess,
							adminAccess,
							callback
							);

					keepAliveEndpoint = new ActiveMQEndpoint(
							url,
							topic.getUsername(),
							topic.getPassword(),
							clientID + "|KeepAlive",
							clientName,
							keepAliveTopic,
							"(Receiver IS NULL) OR (Receiver = '"+ clientID + "')" + selectorSuffix,
							true,
							600000, // 600000 = 10 minutes TimeToLive
							readAccess,
							writeAccess,
							adminAccess,
							callback
							);
					endpoints.put(registerTopic, registerEndpoint);
					endpoints.put(requestTopic, requestEndpoint);
					endpoints.put(aliveTopic, aliveEndpoint);
					endpoints.put(keepAliveTopic, keepAliveEndpoint);
				} else {
					endpoints.put(topicText, new ActiveMQEndpoint(
							url,
							topic.getUsername(),
							topic.getPassword(),
							topic.getClientID(),
							clientName,
							topicText,
							// topic.get("Selectors"); // TODO --> "((Receiver IS NULL) OR (Receiver = '"+ clientID + "')) AND (SelectorString)"
							"(Receiver IS NULL) OR (Receiver = '"+ clientID + "')",
							topic.getDurable(),
							topic.getMessageTTL(),
							readAccess,
							writeAccess,
							adminAccess,
							callback
							));
				}
			} else if (type.toLowerCase().startsWith("tcp")) {
				Util.log("ERROR: TCP not yet implemented.");
				System.exit(1);
			} else {
				Util.log("ERROR: Unhandled topic type.");
				System.exit(1);
			}
		}
	}
	
	@Override
	public void send(LISAMessage message, LISAMessageInfo info) {
		String topic = message.getTopic();
		Endpoint endpoint = endpoints.get(topic);		

		List<LISAVariable> vars_withoutId = new ArrayList<>();
		List<LISAVariable> vars_withId = new ArrayList<>();

		if (endpoint != null) {
			// Check if all LISAVariables contained in the message have a UUID
			for (LISAVariable var : message.getVariables()) {
				if (var.getId() == null) {
					vars_withoutId.add(var);
				} else {
					vars_withId.add(var);
				}
			}
			
			if (vars_withoutId.size() > 0) {
				Util.log("ERROR: send(LISAMessage) dispatch for topic " + topic + " failed.\n" +
						"\tLISAVariable without ID. Number:" + vars_withoutId.size());
				
				HashMap<String, String> map = new HashMap<>();
				map.put("Sender", endpoint.getClientID());
				map.put("Receiver", endpoint.getClientID());
				map.put("SendStatus", "ERROR");
				LISAMessageInfo updatedInfo = new LISAMessageInfo(map);
				
				parentCallback.receive(message, updatedInfo);
			} else {
				HashMap<String, String> map = new HashMap<>();
				map.put("Sender", endpoint.getClientID());
				map.put("Topic", topic);
				LISAMessageInfo updatedInfo = new LISAMessageInfo(map);
				
				endpoint.send(message, updatedInfo);
			}
		} else {
			Util.log("ERROR: send(LISAMessage) dispatch for topic " + topic + " failed.");
		}
	}

	@Override
	public void queryLISAVariableTypes(LISATypeMessage typeMessage, LISAMessageInfo info) {		
		LISAMessageInfo messageInfo;
		if (info == null) {
			HashMap<String, String> map = new HashMap<>();
			map.put("Sender", getClientID());
			map.put("ReplyTo", getClientID());
			messageInfo = new LISAMessageInfo(map);
		} else if (info.getSender() == null && info.getReplyTo() == null) {
			HashMap<String, String> map = new HashMap<>();
			map.putAll(info.getInfos());
			map.put("Sender", getClientID());
			map.put("ReplyTo", getClientID());
			messageInfo = new LISAMessageInfo(map);
		} else {
			messageInfo = info;
		}
		
		if (requestEndpoint != null) {
			requestEndpoint.queryLISAVariableTypes(typeMessage, messageInfo);
		} else {
			Util.log("ERROR: queryLISAVariableTypes(LISATypeMessage) dispatch for topic " + requestTopic + " failed.");
		}		
	}
	
	@Override
	public void queryLISAVariableTypes(LISAVariableType type) {
		ArrayList<LISAVariableType> types = new ArrayList<>();
		types.add(type);
		LISATypeMessage typeMessage = new LISATypeMessage(types);
		queryLISAVariableTypes(typeMessage, null);
	}
	
	@Override
	public void registerLISAVariableTypes(LISATypeMessage typeMessage, LISAMessageInfo info) {
		LISAMessageInfo messageInfo = info;
		if (info == null) {
			HashMap<String, String> map = new HashMap<>();
			map.put("Sender", getClientID());
			map.put("ReplyTo", getClientID());
			messageInfo = new LISAMessageInfo(map);
		}
		if (registerEndpoint != null) {
			registerEndpoint.registerLISAVariableTypes(typeMessage, messageInfo);
		} else {
			Util.log("ERROR: registerLISAVariableTypes(LISATypeMessage) dispatch for topic " + registerTopic + " failed.");
		}		
	}
	
	@Override
	public void registerLISAVariableType(LISAVariableType type) {
		registerLISAVariableTypes(new LISATypeMessage(Arrays.asList(new LISAVariableType[]{type})), null);
	}
	
	@Override
	public void alive(LISAAliveMessage aliveMessage, LISAMessageInfo info) {
		if (aliveEndpoint != null) {
			aliveEndpoint.alive(aliveMessage, info);
		} else {
			Util.log("ERROR: alive(LISAAliveMessage) dispatch for topic " + aliveTopic + " failed.");
		}
	}

	@Override
	public void keepAlive(LISAAliveMessage aliveMessage, LISAMessageInfo info) {
		if (keepAliveEndpoint != null) {
			keepAliveEndpoint.keepAlive(aliveMessage, info);
		} else {
			Util.log("ERROR: keepAlive(LISAAliveMessage) dispatch for topic " + keepAliveTopic + " failed.");
		}
	}

	@Override
	public String getClientID() {
		return clientID;
	}

	@Override
	public String getClientName() {
		return clientName;
	}

	@Override
	public boolean isRunning() {
		boolean running = true;
		for (Endpoint endpoint : endpoints.values()) {
			if (!endpoint.isRunning()) {
				running = false;
			}			
		}
		return running;
	}
}
