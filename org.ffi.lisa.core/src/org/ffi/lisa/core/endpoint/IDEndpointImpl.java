package org.ffi.lisa.core.endpoint;

import java.io.File;

import org.ffi.lisa.core.lisamessage.LISAMessage;

public class IDEndpointImpl extends EndpointImpl {
	public IDEndpointImpl(File configFile, LISAMessageReceiver callback) {
		super(configFile, callback, "");
	}

	@Override
	public void send(LISAMessage message, LISAMessageInfo info) {
		// This is never used here
	}
}
