package org.ffi.lisa.core.endpoint;

import java.util.HashMap;
import java.util.List;

public class EndpointConfig {
	private String URL;
	private String ClientID;
	private String ClientName;
	private List<Topic> Topics;

	EndpointConfig() {
	}

	protected EndpointConfig(
			String url,
			String clientID,
			String clientName,
			List<Topic> topics) {
		this.URL = url;
		this.ClientID = clientID;
		this.ClientName = clientName;
		this.Topics = topics;
	}

	public String getURL() {
		return URL;
	}

	public String getClientID() {
		return ClientID;
	}

	public String getClientName() {
		return ClientName;
	}

	public List<Topic> getTopics() {
		return Topics;
	}

	public void setURL(String uRL) {
		URL = uRL;
	}

	public void setClientID(String clientID) {
		ClientID = clientID;
	}

	public void setClientName(String clientName) {
		ClientName = clientName;
	}

	public void setTopics(List<Topic> topics) {
		Topics = topics;
	}

	public class Topic {
		private String Topic;
		private String Username;
		private String Password;
		private String ClientID;
		private Boolean ReadAccess;
		private Boolean WriteAccess;
		private Boolean AdminAccess;
		private Boolean Durable;
		private Integer MessageTTL;
		private String Type;
		private List<HashMap<String, Object>> Selectors;

		Topic() {
		}

		protected Topic(
				String topic,
				String username,
				String password,
				String clientID,
				Boolean readAccess,
				Boolean writeAccess,
				Boolean adminAccess,
				Boolean durable,
				Integer messageTTL,
				String type,
				List<HashMap<String, Object>> selectors) {
			this.Topic = topic;
			this.Username = username;
			this.Password = password;
			this.ClientID = clientID;
			this.ReadAccess = readAccess;
			this.WriteAccess = writeAccess;
			this.AdminAccess = adminAccess;
			this.Durable = durable;
			this.MessageTTL = messageTTL;
			this.Type = type;
			this.Selectors = selectors;
		}

		public String getTopic() {
			return Topic;
		}

		public String getUsername() {
			return Username;
		}

		public String getPassword() {
			return Password;
		}

		public String getClientID() {
			return ClientID;
		}

		public Boolean getReadAccess() {
			return ReadAccess;
		}

		public Boolean getWriteAccess() {
			return WriteAccess;
		}

		public Boolean getAdminAccess() {
			return AdminAccess;
		}

		public Boolean getDurable() {
			return Durable;
		}

		public Integer getMessageTTL() {
			return MessageTTL;
		}

		public String getType() {
			return Type;
		}

		public List<HashMap<String, Object>> getSelectors() {
			return Selectors;
		}

		public void setTopic(String topic) {
			Topic = topic;
		}

		public void setUsername(String username) {
			Username = username;
		}

		public void setPassword(String password) {
			Password = password;
		}

		public void setClientID(String clientID) {
			ClientID = clientID;
		}

		public void setReadAccess(Boolean readAccess) {
			ReadAccess = readAccess;
		}

		public void setWriteAccess(Boolean writeAccess) {
			WriteAccess = writeAccess;
		}

		public void setAdminAccess(Boolean adminAccess) {
			AdminAccess = adminAccess;
		}

		public void setDurable(Boolean durable) {
			Durable = durable;
		}

		public void setMessageTTL(Integer messageTTL) {
			MessageTTL = messageTTL;
		}

		public void setType(String type) {
			Type = type;
		}

		public void setSelectors(List<HashMap<String, Object>> selectors) {
			Selectors = selectors;
		}		
	}
}