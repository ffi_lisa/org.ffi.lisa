package org.ffi.lisa.core.lisamessage;

import java.io.Serializable;

import java.util.Set;
import java.util.UUID;

public final class LISAVariable implements Serializable {
	private static final long serialVersionUID = 1L;

	private final Object value;
	private final LISAVariableType type;
	private final String timestamp;
	
	@Deprecated
	public LISAVariable(Object value, UUID id, Set<LISAAttribute> attributes) {
		this(value, new LISAVariableType(id, attributes));
	}
	
	public LISAVariable(Object value, LISAVariableType type) { // TODO: Check if the UUID !=null, else return ERROR: Cannot create LISAVariable
		this.value = value;
		this.type = type;
		this.timestamp = null;
	}
	
	public LISAVariable(Object value, String timestamp, LISAVariableType type) { // TODO: Check if the UUID !=null, else return ERROR: Cannot create LISAVariable
		this.value = value;
		this.type = type;
		this.timestamp = timestamp;
	}

	public Object getVar() {
		return value;
	}

	public Integer getInteger() {
		return (Integer)getT(Integer.class);
	}

	public Boolean getBoolean() {
		return (Boolean)getT(Boolean.class);
	}

	public Double getDouble() {
		return (Double)getT(Double.class);
	}

	public String getString() {
		return (String)getT(String.class);
	}
	
	private Object getT(Class<?> c) {
		if (value.getClass().equals(c)) {
			return value;
		} else {
			return null;
		}
	}

	public String getTimestamp() {
		return timestamp;
	}
	
	public UUID getId() {
		return type.getId();
	}

	public LISAVariableType getType() {
		return type;
	}
	
	public Set<LISAAttribute> getAttributes() {
		return type.getAttributes();
	}
	
	public LISAAttribute getAttribute(LISAAttributeType attributeType) {
		return type.getAttributes(attributeType);
	}
	
	public String getName() {
		return type.getName();
	}
}
