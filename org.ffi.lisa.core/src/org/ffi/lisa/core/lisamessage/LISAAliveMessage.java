package org.ffi.lisa.core.lisamessage;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public final class LISAAliveMessage implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private final List<UUID> variableIds;
	
	public LISAAliveMessage(
			List<UUID> variableIds) {
		this.variableIds = new ArrayList<>();
		this.variableIds.addAll(variableIds);
	}
	
	public List<UUID> getVariableTypes() {
		List<UUID> ret = new ArrayList<>();
		ret.addAll(variableIds);
		return ret;
	}
}
