package org.ffi.lisa.core.lisamessage;

import java.io.Serializable;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.ffi.lisa.core.Util;

public final class LISAVariableType implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private final UUID id;
	private final HashSet<LISAAttribute> attributes;
	
	public LISAVariableType(UUID id, Set<LISAAttribute> attributes) {
		this.id = id;
		this.attributes = new HashSet<>();
		if (attributes != null) {
			HashSet<LISAAttributeType> types = new HashSet<>();
			for (LISAAttribute attribute : attributes) {
				if (types.contains(attribute.getType())) {
					Util.log("ERROR: One attribute is already defined for this type: " + attribute.getType().getName() + "|" + attribute.getType().getId()); 
				} else {
					types.add(attribute.getType());
					this.attributes.add(attribute);
				}
			}
		}
		if (this.attributes.size() == 0) {
			Util.log("ERROR: The set of attributes of a 'LISAVariableType' should not be empty.");
		}
	}
	
	public UUID getId() {
		return id;
	}

	public Set<LISAAttribute> getAttributes() {
		HashSet<LISAAttribute> ret = new HashSet<>();
		ret.addAll(attributes);
		return ret;
	}
	
	public LISAAttribute getAttributes(LISAAttributeType type) {
		LISAAttribute ret = null;
		for (LISAAttribute attribute : attributes) {
			if (attribute.getType().equals(type)) {
				ret = attribute;
			} 
		}
		return ret;
	}
	
	public String getName() {
		LISAAttribute attr = getAttributes(new LISAAttributeType("Name", UUID.fromString("34902ed8-1ea1-4e25-96e4-45b689793392")));
		if (attr != null) {
			return attr.getValue();
		} else {
			return null;
		}
	}
}
