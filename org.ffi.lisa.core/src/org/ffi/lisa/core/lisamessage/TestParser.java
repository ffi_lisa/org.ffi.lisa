package org.ffi.lisa.core.lisamessage;

import org.ffi.lisa.core.Util;
import org.ffi.lisa.core.lisamessage.adapted.AdaptedLISAAttribute;
import org.ffi.lisa.core.lisamessage.adapted.AdaptedLISAMessage;
import org.ffi.lisa.core.lisamessage.adapted.AdaptedLISASender;
import org.ffi.lisa.core.lisamessage.adapted.AdaptedLISAVariable;

import java.io.StringReader;
import java.io.StringWriter;

import java.util.HashSet;
import java.util.UUID;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class TestParser {
			
	public static void main(String[] args) throws JAXBException {
		Util.log("Step 0: Create the JAXBContext...");
		
		JAXBContext context = JAXBContext.newInstance(AdaptedLISAMessage.class);

	    Marshaller marshaller_FORMATTED_OUTPUT = context.createMarshaller();
	    marshaller_FORMATTED_OUTPUT.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	    marshaller_FORMATTED_OUTPUT.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
	    
	    Marshaller marshaller = context.createMarshaller();
	    marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, false);
	    marshaller_FORMATTED_OUTPUT.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");	    

	    Unmarshaller unmarshaller = context.createUnmarshaller();
	    
	    StringWriter stringWriter = new StringWriter();
		String outputString = stringWriter.toString();
	    
	    Util.log("Step 0: Done!");
	    
	    Util.log("\nStep 1: Create a LISAMessage object...");

	    LISAAttributeType unitType = new LISAAttributeType("STATION", UUID.randomUUID());
	    LISAAttributeType lineType = new LISAAttributeType("LINE", UUID.randomUUID());
	    LISAAttributeType areaType = new LISAAttributeType("AREA", UUID.randomUUID());
	    LISAAttributeType siteType = new LISAAttributeType("SITE", UUID.randomUUID());
	    LISAAttributeType enterpriseType = new LISAAttributeType("ENTERPRISE", UUID.randomUUID());
	    LISAAttributeType fooType = new LISAAttributeType("FOO", UUID.randomUUID());
	    LISAAttributeType barType = new LISAAttributeType("BAR", UUID.randomUUID());
	    
	    String topic = "LEVEL1.ControlRequest";
		
		// See ISO 8601, Third edition 2004-12-01. 	Data elements and interchange formats - Information interchange - Representation of dates and times
		String timestamp = "1234-05-06T07:08:09.123-0700";
//		String timestamp = "1234-05-06T07:08:09.123+0700";
//		String timestamp = "1234-05-06T07:08:09.123-07:00";
//		String timestamp = "1234-05-06T07:08:09.123-07:30";
//		String timestamp = "1234-05-06T07:08:09.123Z";
//		String timestamp = "1234-05-06T07:08:09.123456789Z";
//		String timestamp = "1234-05-06T07:08:09Z";
//		String timestamp = "1234-05-06T07.3Z";
		
		Util.log("\ttimestamp: " + timestamp);
		
		//LISAVariableBuilder builder = new LISAVariableBuilder(); // TODO
		List<LISAVariable> variables = new ArrayList<>();
		
		HashSet<LISAAttribute> attributes_1 = new HashSet<>();
		attributes_1.add(new LISAAttribute(unitType, "Station1"));
		attributes_1.add(new LISAAttribute(lineType, "Conveyor"));
		attributes_1.add(new LISAAttribute(areaType, "PSL"));
		attributes_1.add(new LISAAttribute(siteType, "Chalmers"));
		attributes_1.add(new LISAAttribute(enterpriseType, "LISA"));
		LISAVariableType varType_1 = new LISAVariableType(UUID.randomUUID(), attributes_1);
		variables.add(new LISAVariable(new Integer(5), varType_1));
		

		HashSet<LISAAttribute> attributes_2 = new HashSet<>();
		attributes_2.add(new LISAAttribute(unitType, "Station1"));
		attributes_2.add(new LISAAttribute(fooType, "foo"));
		LISAVariableType varType_2 = new LISAVariableType(UUID.randomUUID(), attributes_2);
		variables.add(new LISAVariable(new Boolean(false), varType_2));
		
		HashSet<LISAAttribute> attributes_3 = new HashSet<>();
		attributes_3.add(new LISAAttribute(barType, "bar"));
		attributes_3.add(new LISAAttribute(fooType, "foo"));
		LISAVariableType varType_3 = new LISAVariableType(UUID.randomUUID(), attributes_3);
		variables.add(new LISAVariable(new String("abc"), varType_3));		
		
		variables.add(new LISAVariable(new Double(3.14), varType_3));
		
		LISAMessage lisaMessage = new LISAMessage(topic, variables);
		
		Util.log("Step 1: Done!");
		
		
		
		
		Util.log("\nStep 2: Construct an AdaptedLISAMessage object from the previous LISAMessage object...");
		AdaptedLISAMessage adaptedLISAMessage = convert_LISAMessage_to_AdaptedLISAMessage(lisaMessage);
		Util.log("Step 2: Done!");
		
		
		
		
		Util.log("\nStep 3: Convert the previous AdaptedLISAMessage to a XML string...");
		

		marshaller_FORMATTED_OUTPUT.marshal(adaptedLISAMessage, stringWriter);
		outputString = stringWriter.toString();
		stringWriter.getBuffer().setLength(0);
	    Util.log("\tFormatted output:\n\n" + outputString);
	    Util.log("INFO: This could be used to override LISAMessage.toString()!\n");
	    
		marshaller.marshal(adaptedLISAMessage, stringWriter);
		outputString = stringWriter.toString();
		stringWriter.getBuffer().setLength(0);
	    Util.log("\tRaw output:\n\n" + outputString);
	    
	    Util.log("Step 3: Done!");
	    
	    
	    
	    
	    Util.log("\nStep 4: Construct a LISAMessage object from an XML string...");
	    StringReader tmp_reader = new StringReader(outputString);
	    
	    Util.log("\tStep 4.1: Construct an AdaptedLISAMessage object from an XML string...");
	    AdaptedLISAMessage adaptedLISAMessage_bis = null;
		try {
			adaptedLISAMessage_bis = (AdaptedLISAMessage) unmarshaller.unmarshal(tmp_reader);
		} catch (JAXBException e1) {
			Util.log("ERROR:" + e1.getMessage());
			e1.printStackTrace();
		}
				Util.log("\tStep 4.1: Done!");
		
		Util.log("\tStep 4.2: Construct a LISAMessage object from the previous AdaptedLISAMessage object...");
		LISAMessage lisaMessage_bis = convert_AdaptedLISAMessage_to_LISAMessage(adaptedLISAMessage_bis);
		Util.log("\tStep 4.2: Done!");
		
		Util.log("\tStep 4.3: Convert this LISAMessage object back to an AdaptedLISAMessage object...");
		AdaptedLISAMessage adaptedLISAMessage_ter = convert_LISAMessage_to_AdaptedLISAMessage(lisaMessage_bis);
		Util.log("\tStep 4.3: Done!");
		
		Util.log("\tStep 4.4: Convert the new AdaptedLISAMessage to a XML string...");	
		marshaller_FORMATTED_OUTPUT.marshal(adaptedLISAMessage_ter, stringWriter);
		outputString = stringWriter.toString();
		stringWriter.getBuffer().setLength(0);
	    Util.log("\n\t\tFormatted output:\n\n" + outputString);
	    Util.log("INFO: This could be used to override LISAMessage.toString()!\n");
	    
		marshaller.marshal(adaptedLISAMessage_ter, stringWriter);
		outputString = stringWriter.toString();
		stringWriter.getBuffer().setLength(0);
	    Util.log("Raw output bis:\n\n" + outputString);
	    Util.log("\tStep 4.4: Done!");
		
	    
	    
	    
	    
	    Util.log("\nTest hirondelle.date4j.DateTime...");
	    String date4jstr = hirondelle.date4j.DateTime.now(TimeZone.getDefault()).toString();
	    Util.log("date4j string result: " + date4jstr + ". This is not a correct ISO 8601 format!!!!");
		
		
		

	}

	private static AdaptedLISAMessage convert_LISAMessage_to_AdaptedLISAMessage(
			LISAMessage lisaMessage) {
		AdaptedLISAMessage adaptedLISAMessage = new AdaptedLISAMessage();
		adaptedLISAMessage.setTopic(lisaMessage.getTopic());

		AdaptedLISASender adaptedLISASender = new AdaptedLISASender();
		adaptedLISAMessage.setSender(adaptedLISASender);

		for (LISAVariable lisaVariable : lisaMessage.getVariables()) {
			AdaptedLISAVariable adaptedLISAVariable = new AdaptedLISAVariable();
			adaptedLISAVariable.setId(lisaVariable.getId().toString());

			if (lisaVariable.getBoolean() != null) {
				adaptedLISAVariable.setBoolean(lisaVariable.getBoolean());
			}
			if (lisaVariable.getDouble() != null) {
				adaptedLISAVariable.setDouble(lisaVariable.getDouble());
			}
			if (lisaVariable.getInteger() != null) {
				adaptedLISAVariable.setInt(lisaVariable.getInteger());
			}
			if (lisaVariable.getString() != null) {
				adaptedLISAVariable.setString(lisaVariable.getString());
			}

			for (LISAAttribute lisaAttribute : lisaVariable.getAttributes()) {
				AdaptedLISAAttribute adaptedLISAAttribute = new AdaptedLISAAttribute();
				
				adaptedLISAAttribute.setValue(lisaAttribute.getValue());
				adaptedLISAAttribute.setTypeId(lisaAttribute.getType().getId().toString());
				adaptedLISAAttribute.setTypeName(lisaAttribute.getType().getName());

				adaptedLISAVariable.getAttributes().add(adaptedLISAAttribute);
			}

			adaptedLISAMessage.getVariables().add(adaptedLISAVariable);
		}

		return adaptedLISAMessage;
	}
	
	private static LISAMessage convert_AdaptedLISAMessage_to_LISAMessage(
			AdaptedLISAMessage adaptedLisaMessage) {
		
		String lisaTopic = adaptedLisaMessage.getTopic();		
		
		ArrayList<LISAVariable> lisaVariables = new ArrayList<>();
		
	    for (AdaptedLISAVariable adaptedlisaVariable : adaptedLisaMessage.getVariables()){		    	
	    	Object lisaVariableVar = null;
	    	
			if (adaptedlisaVariable.isBoolean() != null) {
				lisaVariableVar = adaptedlisaVariable.isBoolean();
			}
			if (adaptedlisaVariable.getDouble() != null) {
				lisaVariableVar = adaptedlisaVariable.getDouble();
			}
			if (adaptedlisaVariable.getInt() != null) {
				lisaVariableVar = adaptedlisaVariable.getInt();
			}
			if (adaptedlisaVariable.getString() != null) {
				lisaVariableVar = adaptedlisaVariable.getString();
			}
	    	
			HashSet<LISAAttribute> lisaAttributes = new HashSet<>();
			
	    	for (AdaptedLISAAttribute adaptedlisaAttribute : adaptedlisaVariable.getAttributes()) {
	    		LISAAttributeType lisaAttributeType = new LISAAttributeType(adaptedlisaAttribute.getTypeName(), UUID.fromString(adaptedlisaAttribute.getTypeId()));
	    		LISAAttribute lisaAttribute = new LISAAttribute(lisaAttributeType, adaptedlisaAttribute.getValue());

	    		lisaAttributes.add(lisaAttribute);
	    	}
	    	
	    	LISAVariableType lisaVariableType = new LISAVariableType(UUID.fromString(adaptedlisaVariable.getId()), lisaAttributes);
	    	LISAVariable lisaVariable = new LISAVariable(lisaVariableVar, lisaVariableType);
	    	lisaVariables.add(lisaVariable);
	    }	
			
		LISAMessage lisaMessage = new LISAMessage(lisaTopic, lisaVariables);
		

		return lisaMessage;
	}
}
