package org.ffi.lisa.core.lisamessage;

import java.io.Serializable;

public final class LISAAttribute implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private final LISAAttributeType type;
	private final String value;
	
	public LISAAttribute(LISAAttributeType type, String value) {
		this.type = type;
		this.value = value;
	}
	
	public LISAAttributeType getType() {
		return type;
	}
	
	public String getValue() {
		return value;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() == this.getClass()) {
			LISAAttribute objcopy = (LISAAttribute) obj;
			if (this.value.equals(objcopy.getValue())
				&& this.type.equals(objcopy.getType()))
			{
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	@Override
	public int hashCode() {
		return type.hashCode() + value.hashCode();
	}
}
