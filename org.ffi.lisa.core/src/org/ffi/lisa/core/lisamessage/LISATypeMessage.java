package org.ffi.lisa.core.lisamessage;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

public final class LISATypeMessage implements Serializable {
	private static final long serialVersionUID = 1L;

	private final List<LISAVariableType> variableTypes;
	
	public LISATypeMessage(List<LISAVariableType> variableTypes) {
		this.variableTypes = new ArrayList<>();
		this.variableTypes.addAll(variableTypes);
	}
	
	public List<LISAVariableType> getVariableTypes() {
		List<LISAVariableType> ret = new ArrayList<>();
		ret.addAll(variableTypes);
		return ret;
	}
}
