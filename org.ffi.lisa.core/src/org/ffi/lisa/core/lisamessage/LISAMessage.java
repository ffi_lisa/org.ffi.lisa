package org.ffi.lisa.core.lisamessage;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

public final class LISAMessage implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private final String topic;
	
	private final List<LISAVariable> variables;
	
	public LISAMessage(
			String topic,
			List<LISAVariable> variables) {
		this.topic = topic;
		this.variables = new ArrayList<>();
		this.variables.addAll(variables);
	}
	
	public String getTopic() {
		return topic;
	}
	
	public List<LISAVariable> getVariables() {
		List<LISAVariable> ret = new ArrayList<>();
		ret.addAll(variables);
		return ret;
	}
	
	public List<LISAVariable> getIntegerVariables() {
		return getTVariables(Integer.class);
	}
	
	public List<LISAVariable> getBooleanVariables() {
		return getTVariables(Boolean.class);
	}
	
	public List<LISAVariable> getDoubleVariables() {
		return getTVariables(Double.class);
	}
	
	public List<LISAVariable> getStringVariables() {
		return getTVariables(String.class);
	}
	
	private List<LISAVariable> getTVariables(Class<?> c) {
		List<LISAVariable> ret = new ArrayList<>();
		for (LISAVariable variable : variables) {
			if (variable.getVar().getClass().equals(c)) {
				ret.add(variable);
			}
		}
		return ret;
	}
}
