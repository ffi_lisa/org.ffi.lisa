package org.ffi.lisa.core.lisamessage;

import java.io.Serializable;

import java.util.UUID;

public final class LISAAttributeType implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private final String name;
	private final UUID id;
	
	public LISAAttributeType(String name, UUID id) {
		this.name = name;
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public UUID getId() {
		return id;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() == this.getClass()) {
			LISAAttributeType objcopy = (LISAAttributeType) obj;
			if (this.name.equals(objcopy.getName())
				&& this.id.equals(objcopy.getId()))
			{
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	@Override
	public int hashCode() {
		return name.hashCode() + id.hashCode();
	}
}
