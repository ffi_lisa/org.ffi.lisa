package org.ffi.lisa.core.activemq;

import java.io.Serializable;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.ffi.lisa.core.Util;
import org.ffi.lisa.core.endpoint.LISAMessageInfo;
import org.ffi.lisa.core.lisamessage.LISAAliveMessage;
import org.ffi.lisa.core.lisamessage.LISAMessage;
import org.ffi.lisa.core.lisamessage.LISATypeMessage;

public class ActiveMQProducer implements ExceptionListener {
	// Variables as arguments
	private final String url;
	
	private final String user;
	private final String password;

	private final String clientID;
	private final String clientName;

	private final String topic;
	private final long messageTimeToLive;
	private final boolean hasAdminAccess;

	// Internal variables
	private boolean running;

	private Connection connection = null;
	private Session session = null;
	private MessageProducer producer = null;
	private Destination destination = null;

	public ActiveMQProducer(
			String url,
			String topic,
			String user,
			String password,
			String clientName,
			String clientID,
			long messageTimeToLive,
			boolean hasAdminAccess) {
		this.url = url;
		this.topic = topic;
		this.user = user;
		this.password = password;
		this.clientID = clientID;
		this.clientName = clientName;
		this.messageTimeToLive = messageTimeToLive;
		this.hasAdminAccess = hasAdminAccess;
		if (messageTimeToLive == 0) {
			Util.log("WARNING: MessageTimeToLive set to 0 is discouraged (TTL=0 means infinity, TTL=X>0 means X milliseconds).");
		}
		
		try {
			// Create the connection.
			ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(user, password, url);
			connection = connectionFactory.createConnection();
			connection.setExceptionListener(this);
			connection.start();

			// Create the session
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			destination = session.createTopic(topic); // Use ActiveMQ.Advisory.* topics

			// Create the producer
			producer = session.createProducer(destination);
			producer.setTimeToLive(messageTimeToLive);

			showParameters();
			running = true;
		} catch (JMSException e) {
			Util.log("ERROR: " + e.getMessage());
		} catch (Exception e) {
			Util.log("ERROR: [" + clientID + "] Caught: " + e);
			e.printStackTrace();
		}
	}

	public void showParameters() {
		Util.log("INFO: Connecting to URL: " + url + " (" + user + ":" + password + ")");
		Util.log("INFO: Publishing topic: " + topic);
		Util.log("INFO: Messages time to live " + ((messageTimeToLive == 0) ? "inf" : (messageTimeToLive + " ms")));
	}

	@Override
	public synchronized void onException(JMSException ex) {
		// TODO: use hasAdmin to filter errors
		Util.log("[" + clientID + "] JMS Exception occured." + ex);
		if (hasAdminAccess) {
			Util.log("TODO: Use hasAdmin to filter errors. hasAdmin: " + hasAdminAccess);
		} else {
			Util.log("TODO: Use hasAdmin to filter errors. hasAdmin: " + hasAdminAccess);
		}
		ex.printStackTrace();
	}

	public boolean isRunning() {
		return running;
	}

	public String getProducerName() {
		return clientName;
	}

	public Session getSession() {
		return session;
	}

	public MessageProducer getProducer() {
		return producer;
	}
	
	public void send(LISAMessage message, LISAMessageInfo info) {
		while (!isRunning()) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
			}
		}
		try {
			ObjectMessage objectMessage = this.getSession().createObjectMessage((Serializable)message);
			ObjectMessage updatedObjectMessage = setObjectMessageProperties(objectMessage,info);
			producer.send(updatedObjectMessage);
//			Util.log("\nINFO: Sent LISAMessage on topic: " + topic);
		} catch (JMSException e1) {
			e1.printStackTrace();
		} catch (Exception e2) {
			e2.printStackTrace();
		}
	}

	public void send(LISATypeMessage typeMessage, LISAMessageInfo info) {
		while (!isRunning()) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
			}
		}
		try {
			ObjectMessage objectMessage = this.getSession().createObjectMessage((Serializable)typeMessage);
			ObjectMessage updatedObjectMessage = setObjectMessageProperties(objectMessage,info);
			producer.send(updatedObjectMessage);
//			Util.log("\nINFO: Sent LISATypeMessage on topic: " + topic);
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

	public void send(LISAAliveMessage aliveMessage, LISAMessageInfo info) {
		while (!isRunning()) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
			}
		}
		try {
			ObjectMessage objectMessage = this.getSession().createObjectMessage((Serializable)aliveMessage);
			ObjectMessage updatedObjectMessage = setObjectMessageProperties(objectMessage,info);
			producer.send(updatedObjectMessage);
//			Util.log("\nINFO: Sent LISAAliveMessage on topic: " + topic);
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

	public ObjectMessage setObjectMessageProperties(ObjectMessage objectMessage, LISAMessageInfo info) throws JMSException {
		if (info != null) {
//			Util.log("DEBUG: lisaMsgInfo.getInfos(): " + info.getInfos());
			for (String key : info.getInfos().keySet()) {
				objectMessage.setStringProperty(key, info.getInfos().get(key));
			}
		} else {
			Util.log("WARNING: LISAMessageInfo was null");
		}
		
		if (objectMessage.getStringProperty("Sender") == null
				|| objectMessage.getStringProperty("Sender").equals("")) {
			Util.log("WARNING: No Sender was set in the LISAMessageInfo");
			objectMessage.setStringProperty("Sender", clientID);
		}

		return objectMessage;
	}
}
