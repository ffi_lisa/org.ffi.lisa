package org.ffi.lisa.core.activemq;

import org.apache.activemq.ActiveMQConnectionFactory;

import java.util.Enumeration;
import java.util.HashMap;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.Topic;

import org.ffi.lisa.core.Util;

import org.ffi.lisa.core.endpoint.LISAMessageInfo;
import org.ffi.lisa.core.endpoint.LISAMessageReceiver;

import org.ffi.lisa.core.lisamessage.LISAAliveMessage;
import org.ffi.lisa.core.lisamessage.LISAMessage;
import org.ffi.lisa.core.lisamessage.LISATypeMessage;


public class ActiveMQConsumer implements MessageListener, ExceptionListener {
	// Variables as arguments
	private final String url;
	
	private final String user;
	private final String password;
	
	private final String clientName;
	private final String clientID;

	private final String topic;
	private final boolean durable;
	private final String selector;
	private final boolean hasAdminAccess;
	
	private final LISAMessageReceiver consumerHandler;

	// Internal variables
	private boolean running;

	private Connection connection = null;
	private Session session = null;
	private MessageConsumer consumer = null;
	private Destination destination = null;
	
	public ActiveMQConsumer(
			String url,
			String topic,
			String user,
			String password,
			String clientName,
			String clientID,
			boolean durable,
			String selector,
			boolean hasAdminAccess,
			LISAMessageReceiver consumerHandler) {
		this.url = url;
		this.user = user;
		this.password = password;
		this.clientName = clientName;
		this.clientID = clientID;
		this.topic = topic;
		this.durable = durable;
		this.selector = selector;
		this.hasAdminAccess = hasAdminAccess;
		this.consumerHandler = consumerHandler;
		
		try {
			// Create the connection.
			ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(user, password, url);
			connection = connectionFactory.createConnection();
			if (clientID != null && clientID.length() > 0) {
				connection.setClientID(clientID);
			}
			connection.setExceptionListener(this);
			connection.start();
			
			// Create the session
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			destination = session.createTopic(topic); // Use ActiveMQ.Advisory.* topics

			// Create the consumer
			if (durable) {
				consumer = session.createDurableSubscriber((Topic)destination, clientName, selector, false);
			} else {
				consumer = session.createConsumer(destination, selector, false);
			}
			
			consumer.setMessageListener(this);
			
			showParameters();
			running = true;
		} catch (javax.jms.InvalidClientIDException e) {
			Util.log("ERROR: ClientID is not unique: " + e.getMessage());
		} catch (Exception e) {
			Util.log("[" + clientID + "] Caught: " + e);
			e.printStackTrace();
		}
	}

	public void showParameters() throws JMSException {
		Util.log("INFO: Connecting to URL: " + url + " (" + user + ":" + password + ")");
		Util.log("INFO: Consuming topic: " + topic);
		Util.log("INFO: Using a " + (durable ? "durable" : "non-durable") + " subscription");
		Util.log("INFO: Selector: " + selector);
	}
	
	public LISAMessageInfo ObjectMsgProperties_to_LISAMsgInfo(Message jmsMessage) throws JMSException {
		HashMap<String,String> infos = new HashMap<>();
		
		@SuppressWarnings("unchecked")
		Enumeration<String> properties = jmsMessage.getPropertyNames();
		while (properties.hasMoreElements()) {
			String propertyName = (String) properties.nextElement();
			Object property = jmsMessage.getObjectProperty(propertyName);
			if (property instanceof String) {
				infos.put(propertyName, (String) jmsMessage.getObjectProperty(propertyName));
			}			
		}
		
		boolean hasTopic = false;
		for (String key : infos.keySet()) {
			if (key.equalsIgnoreCase("Topic")) {
				hasTopic = true;
			}
		}
		if (!hasTopic) {
			infos.put("Topic", topic);
		}
		LISAMessageInfo lisaMsgInfo = new LISAMessageInfo(infos);
//		Util.log("DEBUG: lisaMsgInfo.getInfos(): " + lisaMsgInfo.getInfos());
		
		return lisaMsgInfo;
	}
	
	@Override
	public void onMessage(Message message) {
		try {
			if (message instanceof ObjectMessage) {
				Object object = ((ObjectMessage)message).getObject();
				if (object instanceof LISAMessage) {
//					Util.log("\nINFO: Received LISAMessage on topic: " + topic);
					LISAMessageInfo info = ObjectMsgProperties_to_LISAMsgInfo(message);
					consumerHandler.receive((LISAMessage)object, info);
				} else if (object instanceof LISATypeMessage) {
//					Util.log("\nINFO: Received LISATypeMessage on topic: " + topic);
					LISAMessageInfo info = ObjectMsgProperties_to_LISAMsgInfo(message);
					consumerHandler.receive((LISATypeMessage)object, info);
				} else if (object instanceof LISAAliveMessage) {
//					Util.log("\nINFO: Received LISAAliveMessage on topic: " + topic);
					LISAMessageInfo info = ObjectMsgProperties_to_LISAMsgInfo(message);
					consumerHandler.receive((LISAAliveMessage)object, info);
				} else if (object == null) {
					Util.log("\nINFO: Received 'null' on topic: " + topic + " (ignored)");
				} else {
					Util.log("\nINFO: Received " + object.getClass() + " on topic: " + topic + " (ignored)");
				}
			}
		} catch (JMSException e) {
			Util.log("[" + clientID + "] Caught: " + e);
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public synchronized void onException(JMSException ex) {
		// TODO: use hasAdmin to filter errors
		Util.log("[" + clientID + "] JMS Exception occured." + ex);
		if (hasAdminAccess) {
			Util.log("TODO: Use hasAdmin to filter errors. hasAdmin: " + hasAdminAccess);
		} else {
			Util.log("TODO: Use hasAdmin to filter errors. hasAdmin: " + hasAdminAccess);
		}
		ex.printStackTrace();
	}
	
	public synchronized boolean isRunning() {
		return running;
	}

	public String getClientName() {
		return clientName;
	}

	public String getClientID() {
		return clientID;
	}

	public Session getSession() {
		return session;
	}
	
	public String getTopic() {
		return topic;
	}

	public MessageConsumer getConsumer() {
		return consumer;
	}

	public LISAMessageReceiver getConsumerHandler() {
		return consumerHandler;
	}
}