package org.ffi.lisa.core;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

import io.netty.channel.Channel;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;

public class Util {
	/**
     * Send a string to a channel using TCP Socket.
     * Please make sure the channel you specified deals with TCP.
     * 
     * @param destChannel
     *     allowed object is
     *     {@link Channel }
     *     
     * @param message
     *     allowed object is
     *     {@link String }     
     */
	public static void sendTCPStringMessage(Channel destChannel, String message) {
		if (destChannel.isActive()) {
			// Append \n if it's not present, because of the frame delimiter
			if (message != null) {
				if (!message.endsWith("\n")) {
					message += System.lineSeparator();
				}
				destChannel.write(message);
			} else {
				log("WARN: Message was empty");
			}
		} else {
			log("ERROR: client Connection lost");
		}
	}
	
	/**
     * Send a string to a channel using WebSocket.
     * Please make sure the channel you specified deals with WebSocket.
     * 
     * @param destChannel
     *     allowed object is
     *     {@link Channel }
     *     
     * @param message
     *     allowed object is
     *     {@link String }     
     */
	public static void sendWSStringMessage(Channel destChannel, String message) {
		if (destChannel.isActive()) {
			// Append \n if it's not present, because of the frame delimiter
			if (message != null) {
				if (!message.endsWith("\n")) {
					message += System.lineSeparator();
				}
				destChannel.write(new TextWebSocketFrame(message));
			} else {
				log("WARN: Message was empty");
			}
		} else {
			log("ERROR: client Connection lost");
		}
	}
	
	public static String readFile(File file) {
		try {
			return new Scanner(file).useDelimiter("\\Z").next();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(1);
			return null;
		}
	}
	
	public void saveStringToFile(File file, String string) {
		BufferedWriter bw = null;

		try {
			// if file does not exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}

			bw = new BufferedWriter(
					new FileWriter(file.getAbsoluteFile(), true));
			bw.write(string);
			Util.log("INFO: File " + file.getName() + " saved.");
		} catch (IOException e) {
			Util.log("ERROR: IOException: Cannot write into the file" + file.getName());
			e.printStackTrace();
		} finally { // always close the file
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					Util.log("ERROR: IOException: Cannot close the file" + file.getName());
					e.printStackTrace();
				}
			}
		}
	}
	
	public final static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	public static void log(String message) {
		String sCaller = "";
		StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
		int frame = 2;
		while (frame < stackTrace.length) {
			StackTraceElement stackElement = stackTrace[frame];
			if (!(stackElement.getClassName().equals(Util.class.getName()) && stackElement.getMethodName().startsWith("write"))) {
				sCaller = stackElement.getFileName() + ":" + stackElement.getLineNumber();
				break;
			}
			frame++;
		}
		
		System.out.println(dateFormat.format(new Date()) + " (" + sCaller + ") " + message);
	}
}
