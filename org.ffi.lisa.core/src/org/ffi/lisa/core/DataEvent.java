package org.ffi.lisa.core;

import io.netty.channel.Channel;

public class DataEvent {
	private Channel channel;
	private String message;
	
	public DataEvent(Channel channel, String msg) {
		this.channel = channel;
		this.message = msg;
	}
	
	public String getMessage() {
		return this.message;
	}
	
	public Channel getChannel() {
		return this.channel;
	}
}
