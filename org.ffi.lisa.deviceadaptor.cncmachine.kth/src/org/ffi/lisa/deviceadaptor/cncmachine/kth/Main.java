package org.ffi.lisa.deviceadaptor.cncmachine.kth;


import java.net.Socket;

import java.net.*;
import java.io.*;

public class Main implements Runnable {

    private Socket connection;
    private String Timestamp;
    private int ID;

    private Translator translator;

    public static void main(String[] args) {
        Translator translator = new Translator();

        int port = 42200;
        int count = 0;
        try {
            ServerSocket myServer = new ServerSocket(port);
            System.out.println("Server initialized");

            while(true) {
                Socket connection = myServer.accept();
                System.out.print(":: Connection received from " + connection.getInetAddress().getHostName() + ":" + connection.getPort());
                Runnable runnable = new Main(connection, ++count);
                Thread thread = new Thread(runnable);
                thread.start();
                // System.out.println(Thread.currentThread() + "Thread started" + count);
            }

        } catch (Exception e) {}
    }

    Main(Socket s, int i) {
        this.connection = s;
        this.ID = i;
    }

    public void run() {
        try {

            BufferedInputStream is = new BufferedInputStream(connection.getInputStream());
            InputStreamReader isr = new InputStreamReader(is);
            int character;


            StringBuffer process = new StringBuffer();

            Timestamp = new java.util.Date().toString();
            System.out.print(" "+Timestamp + " ");

            while((character = isr.read()) != -1) {
                process.append((char)character);
            }

            XMLHandler xmlHandler = new XMLHandler(process.toString());
            xmlHandler.getXML();
            xmlHandler.setXML();
            TCPClient.forwardMessage(xmlHandler.getForwardMessage());
        }
        catch (Exception e) {
            System.out.println(e);
        }
        finally {
            try {
                connection.close();
            }
            catch (IOException e){}
        }
    }
}

