package org.ffi.lisa.deviceadaptor.cncmachine.kth;

//import org.ffi.lisa.core.esbmessage.EsbMessage;
//import org.ffi.lisa.core.esbmessage.EsbMessageObjectFactory;
//import org.ffi.lisa.core.esbmessage.IntElement;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.io.StringWriter;

public class XMLHandler {
    public String xmlMessage;
    public String forwardMessage;
    public MessageHandler message;

    public XMLHandler(String in) {
        this.xmlMessage = in;
        // System.out.println("> XML parsed: "+ in);
    }

    public String getXmlMessage() {
        return xmlMessage;
    }

    public void setXmlMessage(String in) {
        this.xmlMessage = in;
    }

    public void setForwardMessage(String in) {
        this.forwardMessage = in;
    }
    public String getForwardMessage() {
        return forwardMessage;
    }

    public void getXML() {
        try {
            StringReader reader = new StringReader(getXmlMessage());
            JAXBContext jaxbContext = JAXBContext.newInstance(MessageHandler.class);

            Unmarshaller um = jaxbContext.createUnmarshaller();
            message = (MessageHandler) um.unmarshal(reader);
            System.out.println(message.print());
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    public void setXML() {
        String dataPointId = "se.kth.xpres.cnc";
        String type = "EventNotification";

        EsbMessage forwardMessage = new EsbMessageObjectFactory().createEsbMessage(dataPointId);
        forwardMessage.getHeader().setType(type);
        forwardMessage.getHeader().setTimestamp(message.timestampForward);

        IntElement intElement = new EsbMessageObjectFactory().createIntElement();

        intElement.setName(message.event);
        intElement.setValue(0);

        forwardMessage.getBody().getElements().add(intElement);

        try {
            final StringWriter fm = new StringWriter();

            JAXBContext jaxbContext = JAXBContext.newInstance(EsbMessage.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, false);
            // jaxbMarshaller.marshal(forwardMessage, System.out);
            jaxbMarshaller.marshal(forwardMessage, fm);
            setForwardMessage(fm.toString());

        } catch (JAXBException e) {
            e.printStackTrace();
            System.out.println(e);
        }

    }
}
