package org.ffi.lisa.deviceadaptor.cncmachine.kth.model;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(namespace = "se.xpres.kth")
public class Config {

    // XmLElementWrapper generates a wrapper element around XML representation
    @XmlElementWrapper(name = "itemList")
    // XmlElement sets the name of the entities
    @XmlElement(name = "convertItem")
    private ArrayList<ConfigItem> itemList;
    private String name;
    private String location;

    /* public void setItemList(ArrayList<ConfigItem> in) {
        this.itemList = in;
    } */

    public ArrayList<ConfigItem> getItemList() {
        return itemList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}