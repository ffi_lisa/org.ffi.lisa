package org.ffi.lisa.deviceadaptor.cncmachine.kth;

import java.io.*;
import java.net.*;

public class TCPClient {
    private static String url = "130.237.56.123";
    private static int port = 52200;

    public static void forwardMessage(String XMLStream) {
        try{
            Socket socket = new Socket(url, port);
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader in =
                    new BufferedReader(
                            new InputStreamReader(socket.getInputStream()));
            out.println(XMLStream);
        }catch(IOException ioe){
            ioe.printStackTrace();
        }
    }
}