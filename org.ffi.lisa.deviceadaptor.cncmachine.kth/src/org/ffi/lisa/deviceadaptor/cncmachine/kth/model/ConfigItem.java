package org.ffi.lisa.deviceadaptor.cncmachine.kth.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "convertItem")
// If you want you can define the order in which the fields are written
// Optional
@XmlType(propOrder = { "generic", "foreign" })
public class ConfigItem {

    private String generic;
    private String foreign;

    // If you like the variable generic, e.g. "generic", you can easily change this
    // generic for your XML-Output:
    @XmlElement(name = "generic")
    public String getGeneric() {
        return generic;
    }

    public void setGeneric(String generic) {
        this.generic = generic;
    }

    public String getForeign() {
        return foreign;
    }

    public void setForeign(String foreign) {
        this.foreign = foreign;
    }

}