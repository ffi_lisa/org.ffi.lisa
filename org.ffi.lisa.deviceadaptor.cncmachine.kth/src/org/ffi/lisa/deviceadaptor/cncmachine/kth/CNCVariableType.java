package org.ffi.lisa.deviceadaptor.cncmachine.kth;

import java.util.UUID;

public class CNCVariableType {
	private CNCVariableValueType cncType;
	private CNCVariableValueType esbType;
	private String name;
	private UUID id;
	private boolean registered;

	public CNCVariableType(CNCVariableValueType cncType, CNCVariableValueType esbType, String name, UUID uuid) {
		this.cncType = cncType;
		this.esbType = esbType;
		this.name = name;
		this.id = uuid;
	}
	
	public CNCVariableType(CNCVariableValueType cncType, CNCVariableValueType esbType, String name, UUID uuid, boolean registered) {
		this.cncType = cncType;
		this.esbType = esbType;
		this.name = name;
		this.id = uuid;
		this.registered = registered;
	}

	public CNCVariableValueType getCNCType() {
		return this.cncType;
	}

	public CNCVariableValueType getESBType() {
		return this.esbType;
	}

	public String getName() {
		return this.name;
	}

	public UUID getId() {
		return id;
	}

	public boolean isRegistered() {
		return registered;
	}

	public void setRegistered(boolean registered) {
		this.registered = registered;
	}

	// set.contains calls internally the equals methods. So we have to override this method
	// set.add() calls internally the equals and hashCode methods. So we have to override these two methods
	@Override
	public boolean equals(Object obj) {
		// if the two objects are equal in reference, they are equal
		if (this == obj) {
			return true;
		} else if (obj instanceof CNCVariableType) {
			if (this.getId().equals(((CNCVariableType) obj).getId())){
				return true;
			} else {
				return false;
			}
		} else {
			return false;			
		}
	}
	
	// set.contains calls internally the equals methods. So we have to override this method
	// set.add() calls internally the equals and hashCode methods. So we have to override these two methods
	@Override
	public int hashCode(){
	    return Integer.parseInt(this.id.toString());
	}
}

enum CNCVariableValueType {
	List, Double, Float, Long, Int, Short, Byte, Boolean, String, NormalString, TokenString, Date, Datetime, Time, Duration, BigDecimal, BigInteger, Base16Binary, Base64Binary
}