package org.ffi.lisa.deviceadaptor.jggateway;

import org.ffi.lisa.core.Util;
import org.ffi.lisa.core.endpoint.EndpointExtended;
import org.ffi.lisa.core.endpoint.LISAMessageInfo;
import org.ffi.lisa.core.lisamessage.LISAAttribute;
import org.ffi.lisa.core.lisamessage.LISAAttributeType;
import org.ffi.lisa.core.lisamessage.LISAMessage;
import org.ffi.lisa.core.lisamessage.LISATypeMessage;
import org.ffi.lisa.core.lisamessage.LISAVariableType;
import org.ffi.lisa.core.lisamessage.LISAVariable;

import com.google.gson.Gson;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class JGTranslator {
	private EndpointExtended endpoint;
	
	private HashMap<String, LISAAttributeType> attributeTypes = new HashMap<>();
	
	private HashMap<LISAVariableType, JGVariable> pending = new HashMap<>();
	private boolean initComplete = false;
	
	private BiHashMap<UUID, String> jgNames = new BiHashMap<>();
	private HashMap<UUID, JGType> jgTypes = new HashMap<>();
	private HashMap<UUID, ESBType> esbTypes = new HashMap<>();
	private HashMap<UUID, LISAVariableType> varTypes = new HashMap<>();
	
	public JGTranslator(EndpointExtended endpoint) {
		this.endpoint = endpoint;
		
		addAttributeType(attributeTypes, "Enterprise", "ff0ed2bb-6355-4a98-98b5-ffb58f60d776");
		addAttributeType(attributeTypes, "Site", "47cb8652-7871-4698-bc9a-d6daab1b8659");
		addAttributeType(attributeTypes, "Area", "918063ad-67b0-474b-9a34-895938e2180b");
		addAttributeType(attributeTypes, "Line", "286fc9e1-9f25-42d5-be08-426f56cea74e");
		addAttributeType(attributeTypes, "Unit", "2cad9f1a-376e-4984-a530-42978562d2e2");
		addAttributeType(attributeTypes, "Name", "34902ed8-1ea1-4e25-96e4-45b689793392");
	}
	
	public void init() {
		configureVariables("data/variables.txt");
		initComplete = true;
	}
	
	private void addAttributeType(Map<String, LISAAttributeType> attributes, String name, String uuid) {
		LISAAttributeType type = new LISAAttributeType(name, UUID.fromString(uuid));
		attributes.put(name, type);
	}
	
	private void configureVariables(String filename) {
		String content = Util.readFile(new File(filename));
		@SuppressWarnings("unchecked")
		List<Map<String, Object>> conf = new Gson().fromJson(content, List.class);
		for (Map<String, Object> confObj : conf) {
			HashSet<LISAAttribute> attributes = new HashSet<>();
			String jgName = null;
			JGType jgType = null;
			ESBType esbType = null;
			boolean isUsed = false;
			
			for (Map.Entry<String, Object> entry : confObj.entrySet()) {				
				String key = entry.getKey();
				Object value = entry.getValue();
				if (key.equalsIgnoreCase("#IsUsed")) {					
					isUsed = (Boolean)value;
				} else if (key.equalsIgnoreCase("#JGName")) {
					jgName = (String)value;
					if (jgName.contains("|")) {
						Util.log("ERROR: Unsupported #JGName (must not contain '|') \"" + jgName + "\", ignored.");
						isUsed = false;
						break;
					}
				} else if (key.equalsIgnoreCase("#JGType")) {
					try {
						jgType = JGType.valueOf((String)value);
					} catch (IllegalArgumentException e) {
						Util.log("ERROR: Unsupported #JGType \"" + jgType + "\", ignored.");
						isUsed = false;
						break;
					}
				} else if (key.equalsIgnoreCase("#ESBType")) {
					try {
						esbType = ESBType.valueOf((String)value);
					} catch (IllegalArgumentException e) {
						Util.log("ERROR: Unsupported #ESBType \"" + esbType + "\", ignored.");
						isUsed = false;
						break;
					}
				} else {
					LISAAttributeType attributeType = attributeTypes.get(key);
					if (attributeType != null) {
						attributes.add(new LISAAttribute(attributeType, (String)value));
					} else {
						Util.log("ERROR: Unknown LISAAttributeType \"" + key + "\"");
					}
				}
			}
			
			if (isUsed) {
				if (jgName != null) {
					if (jgType != null) {
						if (esbType != null) {
							if (attributes.size() > 0) {
								LISAVariableType newType = new LISAVariableType(null, attributes);
								pending.put(newType, new JGVariable(jgName, jgType, esbType));
								endpoint.queryLISAVariableTypes(newType);
								Util.log("INFO: Querying LISAVariableType for #JGName \"" + jgName + "\".");
							} else {
								Util.log("ERROR: No attributes for \"" + jgName + "\", ignored.");
							}
						} else {
							Util.log("ERROR: No #ESBType for \"" + jgName + "\", ignored.");
						}
					} else {
						Util.log("ERROR: No #JGType for \"" + jgName + "\", ignored.");
					}
				} else {
					Util.log("ERROR: Missing #JGName, ignored. " + confObj);
				}
			}
		}
	}
	
	public void receive(LISATypeMessage typeMessage, LISAMessageInfo info) {
		if (info != null) {
			if (!(info.getSender().equals(endpoint.getClientID())) && !isSetup()) {
				if (typeMessage.getVariableTypes().size() == 1) {
					LISAVariableType msgVarType = typeMessage.getVariableTypes().get(0);
					boolean foundVarType = false;
					for (LISAVariableType varType : pending.keySet()) {
						if (msgVarType.getAttributes().containsAll(varType.getAttributes())) {
							foundVarType = true;
							JGVariable jgVar = pending.get(varType);
							UUID id = msgVarType.getId();
							if (id != null) {
								Util.log("INFO: LISAVariableType response (#JGName, UUID) = (" + jgVar.name + ", " + id + ")");
								if (!jgNames.put(id, jgVar.name)) {
									Util.log("ERROR: Ambiguous UUID \"" + id + "\" or #JGName \"" + jgVar.name + "\".");
								}
								jgTypes.put(id, jgVar.type);
								esbTypes.put(id, jgVar.esbType);
								varTypes.put(id, msgVarType);
							} else {
								Util.log("ERROR: Got LISAVariableType without ID, the requested variable does not exist.");
							}
							pending.remove(varType);
							break;
						}
					}
					if (!foundVarType) {
						Util.log("ERROR: Non-pending LISAVariableType.");
					}
				} else if (typeMessage.getVariableTypes().size() == 0) {
					Util.log("ERROR: No matching LISAVariableType.");
				} else if (typeMessage.getVariableTypes().size() > 1) {
					Util.log("ERROR: " + typeMessage.getVariableTypes().size() + " matching LISAVariableTypes.");
				}
			} else {
				Util.log("DEBUG: Local LISATypeMessage, ignored.");
			}
		} else {
			Util.log("ERROR: LISAMessageInfo is null, ignored.");
		}
	}
	
	public boolean isSetup() {
		return initComplete && pending.size() == 0;
	}
	
	public List<String> lisa2jgs(LISAMessage message) {
		List<String> ret = new ArrayList<>();
		if (message.getVariables().size() == 0) {
			Util.log("DEBUG: Message with empty body.");
		}
		for (LISAVariable var : message.getStringVariables()) {
			createJGMessage(ret, var.getId(), var.getString());
		}
		for (LISAVariable var : message.getDoubleVariables()) {
			createJGMessage(ret, var.getId(), var.getDouble().toString());
		}
		for (LISAVariable var : message.getIntegerVariables()) {
			createJGMessage(ret, var.getId(), var.getInteger().toString());
		}
		for (LISAVariable var : message.getBooleanVariables()) {
			createJGMessage(ret, var.getId(), var.getBoolean().booleanValue() ? "1" : "0");
		}
		return ret;
	}
	
	public void createJGMessage(List<String> list, UUID id, String value) {
		String jgName = jgNames.getForward(id);
		JGType jgType = jgTypes.get(id);
		if (jgName != null && jgType != null) { 
			switch (jgType) {
			case Bool:
				value = (value.equals("1") || value.equals("true")) ? "1" : "0";
				break;
			case Real:
				try {
					Double.parseDouble(value);
				} catch (NumberFormatException e) {
					Util.log("ERROR: Invalid Real value: " + value);
					return;
				}
				break;
			case Int:
				try {
					value = "" + (int)Double.parseDouble(value);
				} catch (NumberFormatException e) {
					Util.log("ERROR: Invalid Int value: " + value);
					return;
				}
				break;
			case String:
				break;
			default:
				Util.log("INTERNAL ERROR: Invalid #JGType \"" + jgType + "\" for id: " + id);
				break;
			}
			list.add(SocketProtocol.create(jgName, value));
		} else {
			Util.log("INFO: Message ignored. Unknown UUID: " + id);
		}
	}
	
	public LISAMessage jg2lisa(String message) {
		String name = SocketProtocol.getTag(message);
		String value = SocketProtocol.getValue(message);
		
		UUID id = jgNames.getBackward(name);
		if (id != null) {
			ESBType esbType = esbTypes.get(id);
			LISAVariableType varType = varTypes.get(id);
			String topic = "LEVEL1.ControlRequest"; // TODO: Should depend on variable name if sending both EventNotification and ControlRequest.
			if (endpoint.getClientName().equals("MockPlant")) {
				topic = "LEVEL0.EventNotification";
			}
			switch (esbType) {
			case Double:
				try {
					return createLISAMessage(topic, varType, new Double(value));
				} catch (NumberFormatException e) {
					Util.log("ERROR: Invalid Double value in message: " + message);
					return null;
				}
			case Boolean:
				return createLISAMessage(topic, varType, new Boolean(value.equals("1")));
			case String:
				return createLISAMessage(topic, varType, value);
			case Integer:
				try {
					return createLISAMessage(topic, varType, new Integer(value));
				} catch (NumberFormatException e) {
					Util.log("ERROR: Invalid Integer value in message: " + message);
					return null;
				}
			default:
				Util.log("INTERNAL ERROR: Unsupported type: " + esbType);
				return null;
			}
		} else {
			Util.log("ERROR: Unknown #JGName " + name);
			return null;
		}
	}
	
	public LISAMessage createLISAMessage(String topic, LISAVariableType varType, Object value) {
		List<LISAVariable> vars = new ArrayList<>();
		vars.add(new LISAVariable(value, varType));
		return new LISAMessage(topic, vars);
	}

	// Internal types
	public enum JGType {
		Bool,
		Real,
		Int,
		String
	}
	
	public enum ESBType {
		Boolean,
		Integer,
		Double,
		String
	}
	
	public class JGVariable {
		public final String name;
		public final JGType type;
		public final ESBType esbType;
		
		public JGVariable(String name, JGType type, ESBType esbType) {
			this.name = name;
			this.type = type;
			this.esbType = esbType;
		}
		
		@Override
		public boolean equals(Object obj) {
			if (obj instanceof JGVariable) {
				JGVariable other = (JGVariable)obj;
				return name.equals(other.name) && type.equals(other.type) && esbType.equals(other.esbType);
			} else {
				return false;
			}
		}
		
		@Override
		public int hashCode() {
			return name.hashCode() + type.hashCode() + esbType.hashCode();
		}
	}
}
