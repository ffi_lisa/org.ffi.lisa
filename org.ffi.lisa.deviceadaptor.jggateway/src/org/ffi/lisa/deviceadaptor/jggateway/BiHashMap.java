package org.ffi.lisa.deviceadaptor.jggateway;

import java.util.HashMap;

public class BiHashMap<K1, K2> {
	private HashMap<K1, K2> forward = new HashMap<>();
	private HashMap<K2, K1> backward = new HashMap<>();
	
	// Returns true if none of the keys were present in the BiHashMap before.
	// Otherwise returns false and the BiHashMap might be inconsistent.
	public synchronized boolean put(K1 key1, K2 key2) {
		boolean isNew = true;
		if (forward.put(key1, key2) != null) {
			isNew = false;
		}
		if (backward.put(key2, key1) != null) {
			isNew = false;
		}
		return isNew;
	}
	
	public synchronized K2 getForward(K1 key) {
		return forward.get(key);
	}
	
	public synchronized K1 getBackward(K2 key) {
		return backward.get(key);
	}
}
