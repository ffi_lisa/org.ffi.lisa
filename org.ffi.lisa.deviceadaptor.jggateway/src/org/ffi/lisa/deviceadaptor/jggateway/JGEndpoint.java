package org.ffi.lisa.deviceadaptor.jggateway;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundMessageHandlerAdapter;

import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.ffi.lisa.core.Util;
import org.ffi.lisa.core.endpoint.EndpointExtended;
import org.ffi.lisa.core.endpoint.EndpointFactory;
import org.ffi.lisa.core.endpoint.LISAMessageReceiver;
import org.ffi.lisa.core.endpoint.LISAMessageInfo;
import org.ffi.lisa.core.lisamessage.LISAAliveMessage;
import org.ffi.lisa.core.lisamessage.LISAMessage;
import org.ffi.lisa.core.lisamessage.LISATypeMessage;
import org.ffi.lisa.core.tcp.TCPServer;

@Sharable
public class JGEndpoint extends ChannelInboundMessageHandlerAdapter<String> implements LISAMessageReceiver, Runnable {
	private static final String defaultGatewayAddress = "http://localhost:9999";
	
	private String gatewayAddress;
	
	private JGTranslator translator;
	private EndpointExtended endpoint;
	private TCPServer jgServer;
	private int jgScanCycleTime = 10; // TODO: Fetch from JG
	private List<LISAMessage> messages = new ArrayList<>();
	
	public static void main(String[] args) throws Exception {
		try {
			JGEndpoint me = new JGEndpoint();
			EndpointExtended endpoint = EndpointFactory.createEndpoint(new File("data/topics.txt"), me);
			me.init(endpoint);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	public JGEndpoint() {
		this(defaultGatewayAddress);
	}
	
	public JGEndpoint(String gatewayAddress) {
		this.gatewayAddress = gatewayAddress;
	}
	
	public void init(EndpointExtended endpoint) throws Exception {
		this.endpoint = endpoint;
		if (endpoint.isRunning()) {
			Util.log("INFO: Endpoint setup successful.");
		} else {
			throw new Exception("FATAL: Endpoint setup failed.");
		}
		translator = new JGTranslator(endpoint);
		translator.init();
		while (!translator.isSetup()) {
			Thread.sleep(100);
		}
		
		jgServer = new TCPServer(new URI(gatewayAddress), "jgServer", this, null);
		
		new Thread(jgServer).start();
		Util.log("Waiting for jgServer to start...");
		while (!jgServer.isRunning()) {
			Thread.sleep(200);
		}
		Util.log("Done.");
		
		new Thread(this).start();
	}
	
	@Override
	public synchronized void messageReceived(ChannelHandlerContext ctx, String jgMessage) throws Exception {
		Util.log("DEBUG: Got message from JG: " + jgMessage);
		try {
			LISAMessage lisaMessage = translator.jg2lisa(jgMessage);
			if (lisaMessage != null) {
				endpoint.send(lisaMessage, null);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void receive(LISAMessage message, LISAMessageInfo info) {
//		Util.log("DEBUG: Got message from ESB: " + message);
		if (!info.getSender().equals(endpoint.getClientID())) {
			synchronized (messages) {
				messages.add(message);
				messages.notifyAll();
			}
		} else {
			Util.log("DEBUG: " + endpoint.getClientID() + " ignored own message.");
		}
	}
	
	@Override
	public void run() {
		try {
			while (!Thread.interrupted()) {
				// Wait for JG connection
				if (jgServer.childChannel == null) {
					Util.log("DEBUG: Waiting for JG(" + endpoint.getClientName() + ") to connect...");
					while (jgServer.childChannel == null) {
						Thread.sleep(1000);
					}
					Util.log("DEBUG: JG(" + endpoint.getClientName() + ") connected.");
				}
				
				// Consume message
				ConsumeStatus consumeStatus = ConsumeStatus.Ignored;
				synchronized (messages) {
					if (!messages.isEmpty()) {
						consumeStatus = consumeLISAMessage(messages.get(0));
						if (consumeStatus != ConsumeStatus.Error) {
							messages.remove(0);
						} else {
							Util.log("WARNING: Consume message failed. Will retry.");
						}
					}
				}
				
				// Give JG time to properly consume messages
				if (consumeStatus == ConsumeStatus.Sent) {
					Thread.sleep(2*jgScanCycleTime);
				}
				
				// Wait for messages
				synchronized (messages) {
					if (messages.isEmpty()) {
						messages.wait();
					}
				}
			}
		} catch (InterruptedException e) {
			// Requested to stop
		}
		Util.log("JGEndpoint terminated");
	}
	
	public enum ConsumeStatus {
		Sent,
		Ignored,
		Error
	}
	
	private ConsumeStatus consumeLISAMessage(LISAMessage message) {
//		Util.log("DEBUG: Consuming message: " + message);
		try {
			Channel channel = jgServer.childChannel;
			if (channel != null) {
				if (message.getTopic().equals("LEVEL0.EventNotification") || endpoint.getClientID().equals("MockPlant")) {
					ConsumeStatus ret = ConsumeStatus.Ignored;
					for (String jgMessage : translator.lisa2jgs(message)) {
						Util.sendTCPStringMessage(channel, jgMessage);
						Util.log("INFO: Sent to JG(" + endpoint.getClientName() + "): " + jgMessage);
						ret = ConsumeStatus.Sent;
					}
					return ret;
				} else {
					Util.log("DEBUG: " + endpoint.getClientName() + " ignored topic: " + message.getTopic());
					return ConsumeStatus.Ignored;
				}
			} else {
				Util.log("DEBUG: JG not connected. Message not consumed.");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ConsumeStatus.Error;
	}
	
	@Override
	public void receive(LISATypeMessage typeMessage, LISAMessageInfo info) {
//		Util.log("DEBUG: Got TypeMessage from ESB.");
		try {
			while (translator == null) {
				Util.log("DEBUG: Still constructing Translator. Stalling receive(LISATypeMessage)...");
				Thread.sleep(100);
			}
			translator.receive(typeMessage, info);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
	}
	
	@Override
	public void receive(LISAAliveMessage aliveMessage, LISAMessageInfo info) {
		Util.log("DEBUG: LISAAliveMessage received.");
	}
}
