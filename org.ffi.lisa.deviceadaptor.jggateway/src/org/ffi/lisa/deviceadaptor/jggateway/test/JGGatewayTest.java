package org.ffi.lisa.deviceadaptor.jggateway.test;

import org.ffi.lisa.deviceadaptor.jggateway.JGEndpoint;

public class JGGatewayTest {
	public static void main(String[] args) {
		try {
			MockESB esb = new MockESB();
			
			new MockIDService(esb);
			new MockPlant(esb);
			
			JGEndpoint gateway = new JGEndpoint();
			MockEndpoint endpoint = new MockEndpoint(esb, gateway, "JGGateway", "JGGateway");
			gateway.init(endpoint);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
}
