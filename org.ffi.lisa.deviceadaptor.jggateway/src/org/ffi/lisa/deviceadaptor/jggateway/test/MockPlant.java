package org.ffi.lisa.deviceadaptor.jggateway.test;

import org.ffi.lisa.core.endpoint.EndpointExtended;
import org.ffi.lisa.deviceadaptor.jggateway.JGEndpoint;

public class MockPlant {
	public MockPlant(MockESB esb) throws Exception {
		JGEndpoint jg = new JGEndpoint("http://localhost:8888");
		EndpointExtended esbEndpoint = new MockEndpoint(esb, jg, "MockPlant", "MockPlant");
		jg.init(esbEndpoint);
	}
}
