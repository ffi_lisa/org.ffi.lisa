package org.ffi.lisa.deviceadaptor.jggateway.test;

import java.util.Arrays;
import java.util.HashMap;

import org.ffi.lisa.core.endpoint.EndpointExtended;
import org.ffi.lisa.core.endpoint.LISAMessageInfo;
import org.ffi.lisa.core.endpoint.LISAMessageReceiver;
import org.ffi.lisa.core.lisamessage.LISAAliveMessage;
import org.ffi.lisa.core.lisamessage.LISAMessage;
import org.ffi.lisa.core.lisamessage.LISATypeMessage;
import org.ffi.lisa.core.lisamessage.LISAVariableType;

public class MockEndpoint implements EndpointExtended {
	private MockESB esb;
	private String clientID;
	private String clientName;
	
	public MockEndpoint(MockESB esb, LISAMessageReceiver callback, String clientID, String clientName) {
		this.esb = esb;
		esb.addReceiver(callback);
		this.clientID = clientID;
		this.clientName = clientName;
	}
	
	@Override
	public void send(LISAMessage message, LISAMessageInfo info) {
		if (info == null) {
			HashMap<String, String> map = new HashMap<>();
			map.put("Sender", getClientID());
			info = new LISAMessageInfo(map);
		}
		esb.receive(message, info);
	}
	
	@Override
	public void registerLISAVariableTypes(LISATypeMessage typeMessage, LISAMessageInfo info) {
		if (info == null) {
			HashMap<String, String> map = new HashMap<>();
			map.put("Sender", getClientID());
			map.put("ReplyTo", getClientID());
			info = new LISAMessageInfo(map);
		}
		esb.receive(typeMessage, info);
	}
	
	@Override
	public void registerLISAVariableType(LISAVariableType type) {
		registerLISAVariableTypes(new LISATypeMessage(Arrays.asList(new LISAVariableType[]{type})), null);
	}
	
	@Override
	public void queryLISAVariableTypes(LISATypeMessage typeMessage, LISAMessageInfo info) {
		registerLISAVariableTypes(typeMessage, info);
	}
	
	@Override
	public void queryLISAVariableTypes(LISAVariableType type) {
		registerLISAVariableType(type);
	}
	
	@Override
	public String getClientID() {
		return clientID;
	}
	
	@Override
	public String getClientName() {
		return clientName;
	}
	
	@Override
	public boolean isRunning() {
		return true;
	}
	
	// Not used
	@Override
	public void alive(LISAAliveMessage aliveMessage, LISAMessageInfo info) {
	}
	
	@Override
	public void keepAlive(LISAAliveMessage aliveMessage, LISAMessageInfo info) {
	}
}
