package org.ffi.lisa.deviceadaptor.jggateway.test;

import java.util.ArrayList;

import org.ffi.lisa.core.endpoint.LISAMessageInfo;
import org.ffi.lisa.core.endpoint.LISAMessageReceiver;
import org.ffi.lisa.core.lisamessage.LISAAliveMessage;
import org.ffi.lisa.core.lisamessage.LISAMessage;
import org.ffi.lisa.core.lisamessage.LISATypeMessage;

public class MockESB implements LISAMessageReceiver {
	private ArrayList<LISAMessageReceiver> receivers = new ArrayList<>();
	
	public MockESB() {
	}
	
	public synchronized void addReceiver(LISAMessageReceiver receiver) {
		receivers.add(receiver);
	}
	
	@Override
	public synchronized void receive(LISAMessage message, LISAMessageInfo info) {
		for (LISAMessageReceiver receiver : receivers) {
			receiver.receive(message, info);
		}
	}
	
	@Override
	public synchronized void receive(LISATypeMessage typeMessage, LISAMessageInfo info) {
		for (LISAMessageReceiver receiver : receivers) {
			receiver.receive(typeMessage, info);
		}
	}
	
	@Override
	public void receive(LISAAliveMessage aliveMessage, LISAMessageInfo info) {
	}
}
