package org.ffi.lisa.deviceadaptor.jggateway.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.UUID;

import org.ffi.lisa.core.endpoint.LISAMessageInfo;
import org.ffi.lisa.core.endpoint.LISAMessageReceiver;
import org.ffi.lisa.core.lisamessage.LISAAliveMessage;
import org.ffi.lisa.core.lisamessage.LISAMessage;
import org.ffi.lisa.core.lisamessage.LISATypeMessage;
import org.ffi.lisa.core.lisamessage.LISAVariableType;

public class MockIDService implements LISAMessageReceiver {
	private MockESB esb;
	private ArrayList<LISAVariableType> varTypes = new ArrayList<>();
	
	public MockIDService(MockESB esb) {
		this.esb = esb;
		esb.addReceiver(this);
	}
	
	@Override
	public void receive(LISATypeMessage typeMessage, LISAMessageInfo info) {
		if (!(info.getSender().equals(getClientID()))) {
			for (LISAVariableType requestedVarType : typeMessage.getVariableTypes()) {
				LISAVariableType ret = null;
				for (LISAVariableType storedVarType : varTypes) {
					if (storedVarType.getAttributes().containsAll(requestedVarType.getAttributes()) &&
					    requestedVarType.getAttributes().containsAll(storedVarType.getAttributes())) {
						ret = storedVarType;
						break;
					}
				}
				if (ret == null) {
					ret = new LISAVariableType(UUID.randomUUID(), requestedVarType.getAttributes());
					varTypes.add(ret);
				}
				
				HashMap<String, String> replyData = new HashMap<>();
				replyData.put("Sender", getClientID());
				replyData.put("Receiver", info.getReplyTo());
				replyData.put("RequestStatus", "OK");
				LISAMessageInfo replyInfo = new LISAMessageInfo(replyData);
				LISATypeMessage replyMessage = new LISATypeMessage(Arrays.asList(new LISAVariableType[]{ret}));
				esb.receive(replyMessage, replyInfo);
			}
		}
	}
	
	private String getClientID() {
		return "MockIDService";
	}
	
	// Not used
	@Override
	public void receive(LISAMessage message, LISAMessageInfo info) {
	}
	
	@Override
	public void receive(LISAAliveMessage aliveMessage, LISAMessageInfo info) {
	}
}
