package org.ffi.lisa.deviceadaptor.jggateway;

import java.util.StringTokenizer;

public class SocketProtocol {
	public static String getTag(String message) {
		StringTokenizer st = new StringTokenizer(message, "|");
		return st.nextToken();
	}

	public static String getValue(String message) {
		StringTokenizer st = new StringTokenizer(message,"|");
		st.nextToken(); // Skip the tag token
		if (st.hasMoreTokens()) {
			return st.nextToken();
		} else {
			// | is the final character in the message, i.e. an empty value.
			return "";
		}
	}

	public static String create(String tag, String value) {
		return tag + "|" + value;
	}
}
