package org.ffi.lisa.types;
import java.io.Serializable;

/**
 * The base class of all generic types.
 * <br><br>
 */
public abstract class GenericType implements Serializable {
    
    private static final int INDENT_SIZE = 3;
    
    protected static String indentString = "";
    protected static int indentLevel;
    
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    /**
     * Creates a printable string of the data type.
     */
    public abstract String toString();

    /**
     * Creates a type tagged readable string of the data type.
     */
    public abstract String toTaggedString();
    
    
    /**
     * Used by the toString() method to create the correct indent level.
     *
     */
    protected void incIndent() {
        indentLevel++;
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < indentLevel*INDENT_SIZE; i++) {
            buf.append(' ');
        }
        indentString = buf.toString();
    }
    
    /**
     * Used by the toString() method to create the correct indent level.
     *
     */
    protected void decIndent() {
        indentLevel--;
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < indentLevel*INDENT_SIZE; i++) {
            buf.append(' ');
        }
        indentString = buf.toString();
    }

    /**
     * Gets the type code of this SupportedDataType.
     * The type code is calculated as the Adler32 checksum of the fully qualified Java class name.
     * 
     * @return The type.
     */
//    public long getTypeCode() {
//        return TypeHelper.string2Adler32(this.getClass().getName());
//    }
    
    /**
     * Gets the type of this SupportedDataType as a string.
     * The type is calculated as the last part of the fully qualified Java class name.
     * 
     * @return
     */
    public String getTypeString() {
        return this.getClass().getName().substring(this.getClass().getName().lastIndexOf('.') + 1);
    }
}
