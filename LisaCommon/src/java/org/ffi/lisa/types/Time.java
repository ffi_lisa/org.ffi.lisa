package org.ffi.lisa.types;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Time data type.
 * 
 * Represents a time as the milliseconds since January 1, 1970, 00:00:00 GMT.
 * <br><br>
 * Assembled for the FFI LISA project 
 */

public class Time extends SimpleType {
	
    private long time;
    
	private SimpleDateFormat timestampFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");	
	
    /**
	 * The serialization version number
	 */
	private static final long serialVersionUID = 1L;

    /**
     * Initializes Time
     *  
     * @param value the milliseconds since January 1, 1970, 00:00:00 GMT.
     */
	public Time(long value) {
        this.time= value;
    }

	/**
	 * Initializes Time
	 * 
	 * @param date a java.util.Date object
	 */
    public Time(Date date) {
        this.time= date.getTime();
    }
    
    /**
	 * Initializes Time
     * 
     * @param timestamp a jav.sql.Timestamp object
     */
    public Time(Timestamp timestamp) {
        this.time= timestamp.getTime();
    }

    /**
     * Gets the Time value
     * 
     * @return the milliseconds since January 1, 1970, 00:00:00 GMT. 
     */
    public long getTime() {
        return this.time;
    }
    
	@Override
	public String toString() {
		return timestampFormat.format(new Date(this.time));		
	}

	@Override
	public String toTaggedString() {
        StringBuffer buf = new StringBuffer(indentString).append("<").append(this.getTypeString());
        if (this.getId() != null) {
        	buf.append(" id=\"").append(this.getId()).append("\"");
        }
        buf.append(">").append(timestampFormat.format(new Date(this.time))).append("</").append(this.getTypeString()).append(">");
        return buf.toString();
	}
}	
	