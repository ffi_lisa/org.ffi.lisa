package org.ffi.lisa.types;

import java.util.ArrayList;
import java.util.List;
/**
 * Holds an array of identical (generic) data types. <br>
 * <br>
 * Assembled for the FFI LISA project 
 * <br>
 */
public class Array extends GenericType{

    private List<GenericType> elements = new ArrayList<GenericType>();

    /**
	 * The serializable version number
	 */
	private static final long serialVersionUID = 1L;

    /**
     * Adds an element to the array.
     * 
     * @param element The element to add to the array.
     * @throws MixedTypesException if someone tries to add an element 
     * that is of different type than the other elements in the array.
     */
    public void add(GenericType element) throws TypeMismatchException{
        if (!isElementTypeAllowed(element)) {
            throw new TypeMismatchException("The submitted object is of a different type.", this, "TYPE_MISMATCH");
        }
        elements.add(element);
    }
    
    /**
     * Checks if the element type is allowed to add to the array.
     * The first time any type is OK
     * 
     * @param element 	The type to check.
     * @return true if the type is allowed to add to the array.
     */
    private boolean isElementTypeAllowed(GenericType element) {
        if (elements.size() == 0) {
            return true;
        }
        return elements.get(0).getClass().isInstance(element);
    }

    /**
     * Gets the elements of this array.
     * 
     * @return The elements of this array.
     */
    public List<GenericType> getElements() {
        return elements;
    }

    /**
     * Gets the element at the specified position.
     * 
     * @param index
     * @return The element at the specified position.
     */
    public GenericType get(int index) {
        return elements.get(index);
    }
    
    /**
     * Gets the current number of elements in the array.
     * 
     * @return The current number of elements in the array.
     */
    public int size() {
        return elements.size();
    }

    @Override
    public String toTaggedString() {
        StringBuffer buf = new StringBuffer(indentString).append("<").append(this.getTypeString());
        if (this.getId() != null) {
        	buf.append(" id=\"").append(this.getId()).append("\"");
        }
        buf.append(">\n");
        
        incIndent();
        for (GenericType element : elements) {
            buf.append(element.toTaggedString()).append("\n");
        }
        decIndent();
        
        buf.append(indentString).append("</").append(this.getTypeString()).append(">");
        return buf.toString();
    }

	@Override
	public String toString() {
        StringBuffer buf = new StringBuffer();
        for (GenericType element : elements) {
            buf.append(element.toString()).append("\n");
        }
        return buf.toString();
	}
}

