package org.ffi.lisa.types;

/**
 * UTF8String data type.
 * <br><br>
 * Assembled for the FFI LISA project 
 */

public class Utf8String extends SimpleType {

	private String value;
    
    /**
	 * The serialization version number
	 */
	private static final long serialVersionUID = 1L;
	
    public Utf8String(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
    
	@Override
    public String toTaggedString() {
        StringBuffer buf = new StringBuffer(indentString).append("<").append(this.getTypeString());
        if (this.getId() != null) {
        	buf.append(" id=\"").append(this.getId()).append("\"");
        }
        buf.append(">").append(value).append("</").append(this.getTypeString()).append(">");
        return buf.toString();
    }

	@Override
	public String toString() {
		return value;
	}
}
