package org.ffi.lisa.types;

import java.util.ArrayList;
import java.util.List;

/**
 * Holds a list of (generic) data types. 
 * The data types do not need to be of the same type.
 * <br>
 *
 */
public class Struct extends GenericType {

    List<GenericType> genericTypes = new ArrayList<GenericType>();

    /**
	 * The serializable version number
	 */
	private static final long serialVersionUID = 1L;
    
    /**
     * Adds a GenericType to the struct.
     * 
     * @param type to add
     */
    public void add(GenericType type) {
    	genericTypes.add(type);
    }

    /**
     * Gets the genericTypes of this struct.
     * 
     * @return The genericTypes of this struct.
     */
    public List<GenericType> getGenericTypes() {
        return genericTypes;
    }
    
    /**
     * Gets the genericType at the specified position.
     * 
     * @param index The position of the requested genericType.
     * @return The requested genericType.
     */
    public GenericType get(int index) {
        return genericTypes.get(index);
    }
    
    /**
     * Gets the genericType with the specified id.
     * 
     * @param id The id of the requested genericType.
     * @return The requested genericType, null if no one could be found.
     */
    public GenericType getById(String id) {
    	GenericType retVal = null;
        for (GenericType type : genericTypes) {
            if (id.equals(type.getId())) {
                retVal = type;
                break;
            }
        }
        return retVal;
    }
    
    /**
     * Gets the current number of genericTypes in the struct.
     * 
     * @return The current number of genericTypes in the struct.
     */
    public int size() {
        return genericTypes.size();
    }

    @Override
    public String toTaggedString() {
        StringBuffer buf = new StringBuffer(indentString).append("<").append(this.getTypeString());
        if (this.getId() != null) {
        	buf.append(" ID=\"").append(this.getId()).append("\"");
        }
        buf.append(">\n");
        incIndent();
        for (GenericType type : genericTypes) {
            buf.append(type.toTaggedString()).append("\n");
        }
        decIndent();
        buf.append(indentString).append("</").append(this.getTypeString()).append(">");
        return buf.toString();
    }

	@Override
	public String toString() {
        StringBuffer buf = new StringBuffer();
        for (GenericType type : genericTypes) {
            buf.append(type.toString()).append("\n");
        }
        return buf.toString();
	}
}
