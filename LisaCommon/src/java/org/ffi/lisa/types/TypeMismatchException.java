package org.ffi.lisa.types;

public class TypeMismatchException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Default constructor.
	 * Calls super with a default message
	 */
	public TypeMismatchException()	{
		super("No message provided");
	}
	
	public TypeMismatchException(String message){
		super(message);
	}
	
	public TypeMismatchException(String message, Object caller, String error){
		super(message);
	}
	
	public TypeMismatchException(String message, Object caller, String error, Throwable t){
		super(message, t);
	}
	
}
