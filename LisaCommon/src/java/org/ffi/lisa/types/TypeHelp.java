package org.ffi.lisa.types;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
/**
 * 
 * This class contains various helper methods for manipulation of types, both built 
 * in ones and our own
 * <P>
 * Written for the FFI LISA project 
 * <P>
 * 
 * Author: H�kan Pettersson,  February 2013 
 */
public class TypeHelp {

	private static final byte[] hexLower = new byte[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
	private static final byte[] hexUpper = new byte[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

	/**
	 * Converts a byte to a Hex string
	 * @param b
	 * @return
	 */
	public static String byte2UpperHexString(byte b) {
		return byte2HexString(b, hexUpper);
	}
	
	public static String byte2LowerHexString(byte b) {
		return byte2HexString(b, hexLower);
	}
	
	public static String byte2HexString(byte b, byte[] hexNybbleArray)
	{
		StringBuffer 	strBuf = new StringBuffer(); 
		ByteBuffer 		byteBuf=ByteBuffer.allocate(Short.SIZE/8);
		short 			value;

		byteBuf.put((byte)0);byteBuf.put(b); byteBuf.flip();
		value=byteBuf.getShort();
		
		return strBuf.append(hexNybbleArray[value&0xf0]).append(hexNybbleArray[value&0x0f]).toString();
	}
}
