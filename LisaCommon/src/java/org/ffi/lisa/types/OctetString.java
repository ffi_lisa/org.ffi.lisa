package org.ffi.lisa.types;

/**
 * OctetString data type.
 * 
 * Represents an array of bytes as a string
 * <br><br>
 * 
 * Assembled for the FFI LISA project 
 *
 */
public class OctetString extends SimpleType {

    private byte[] value;

	private static final long serialVersionUID = 1L;
    
    public OctetString(byte[] value) {
        this.value = value;
    }

    public byte[] getValue() {
        return value;
    }
    
    public int size() {
        return value.length;
    }
    
	@Override
    public String toTaggedString() {
        StringBuffer buf = new StringBuffer(indentString).append("<").append(this.getTypeString());
        if (this.getId() != null) {
        	buf.append(" id=\"").append(this.getId()).append("\"");
        }
        buf.append(">\n");
        
        for (int i = 0; i < value.length; i++) {
            buf.append(TypeHelp.byte2UpperHexString(value[i]));
        }
        
        buf.append("</").append(this.getTypeString()).append(">");
        return buf.toString();
    }

	@Override
	public String toString() {
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < value.length; i++) {
            buf.append(TypeHelp.byte2UpperHexString(value[i]));
        }
        return buf.toString();
	}
}
