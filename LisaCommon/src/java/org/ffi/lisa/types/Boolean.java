package org.ffi.lisa.types;

/**
 * Boolean data type.
 * <br><br>
 * Assembled for the FFI LISA project 
 */
public class Boolean extends SimpleType {
    
    boolean value = false;

	private static final long serialVersionUID = 1L;
	
	public static final Boolean TRUE = new Boolean(true); 
	public static final Boolean FALSE = new Boolean(false);

    public Boolean(boolean value) {
        this.value = value;
    }
    
    public boolean getValue() {
        return value;
    }

    public String toTaggedString() {
    	StringBuffer buf = new StringBuffer(indentString).append("<").append(this.getTypeString());
        if (this.getId() != null) {
        	buf.append(" id=\"").append(this.getId()).append("\"");
        }
        buf.append(">").append(value).append("</").append(this.getTypeString()).append(">");
        return buf.toString();
    }

	@Override
	public String toString() {
		Boolean b = new Boolean(value);
		return b.toString();
	}
}
