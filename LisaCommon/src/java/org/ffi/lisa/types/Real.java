package org.ffi.lisa.types;

/**
 * Real data type.
 * <br>
 * Represents a 64 bit floating point number.
 * <br><br>
 * Assembled for the FFI LISA project 
 */
public class Real extends SimpleType {

    private double value;
  
	private static final long serialVersionUID = 1L;
    
    public Real(double value) {
        this.value = value;
    }

    public double getValue() {
        return value;
    }
    
	@Override
    public String toTaggedString() {
        StringBuffer buf = new StringBuffer(indentString).append("<").append(this.getTypeString());
        if (this.getId() != null) {
        	buf.append(" id=\"").append(this.getId()).append("\"");
        }
        buf.append(">").append(value).append("</").append(this.getTypeString()).append(">");
        return buf.toString();
    }

	@Override
	public String toString() {
		return Double.toString(value);
	}
}
