package org.ffi.lisa.types;

/**
 * BitString data type.
 * <br><br>
 * Represents an array of bits as a string. The bits are backed by a byte array
 * <br><br>
 * 
 * Assembled for the FFI LISA project 
 *
 */
public class BitString extends SimpleType{

    private byte[]  value;
    private int		size=0;
    
	private static final byte[] byteMask = new byte[] {(byte) 128, 64, 32, 16, 8, 4, 2, 1 };     

	private static final long serialVersionUID = 1L;
    
	// Initialize an empty BitString to a specified size in bits
    public BitString(int size) throws IndexOutOfBoundsException 
    {
    	if(size<=0) {
            throw new IndexOutOfBoundsException("No size specified for " + getTypeString());
    	}
    	this.value = new byte[size%8==0 ? size/8 : size/8+1];	
    	this.size= size;	
    }
    
	// Initialize a BitString also indicating number of valid bits in the supplied byte array
    public BitString(byte[] value, int size) throws IndexOutOfBoundsException 
    {
    	if(size<=0 || (value.length*8 < size)) {
            throw new IndexOutOfBoundsException("No size specified for " + getTypeString());
    	}
    	this.value = value;	
    	this.size= size;	
    }

    // Returns the byte array backing this BitString
    public byte[] getArrayValue() {
        return value;
    }
    
    public int size() {
        return size;
    }

    public void setBit(int pos)  throws IndexOutOfBoundsException
    {
		value[pos/8] |= byteMask[pos%8]; 
    }
	
    public int getBit(int pos)  throws IndexOutOfBoundsException {
    	
		return ((value[pos/8]&byteMask[pos%8])!= 0 ? 1 :0 );
    }
	
	
	@Override
	public String toTaggedString() {
        StringBuffer buf = new StringBuffer(indentString).append("<").append(this.getTypeString());
        if (this.getId() != null) {
        	buf.append(" id=\"").append(this.getId()).append("\"");
        }
        buf.append(">\n");
        
        for (int i = 0; i < size; i++) {
            buf.append(getBit(i));
        }
        
        buf.append("</").append(this.getTypeString()).append(">");
        return buf.toString();
	}

	@Override
	public String toString() {
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < size; i++) {
            buf.append(getBit(i));
        }
        return buf.toString();
	}

}
