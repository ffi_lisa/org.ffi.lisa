package org.ffi.lisa.types;

/**
 * Integer data type.
 * 
 * Represents a 64 bit number.
 * <br><br>
 * Assembled for the FFI LISA project 
 */
public class Integer extends SimpleType {

    private long value;
    
    /**
	 * The serialization version number
	 */
	private static final long serialVersionUID = 1L;

    public Integer(long value) {
        this.value = value;
    }

    public long getValue() {
        return value;
    }

	@Override
    public String toTaggedString() {
        StringBuffer buf = new StringBuffer(indentString).append("<").append(this.getTypeString());
        if (this.getId() != null) {
        	buf.append(" id=\"").append(this.getId()).append("\"");
        }
        buf.append(">").append(value).append("</").append(this.getTypeString()).append(">");
        return buf.toString();
    }

	@Override
	public String toString() {
		return Long.toString(value);
	}
}
